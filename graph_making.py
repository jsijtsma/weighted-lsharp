import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib.patches as mpatches


def is_dijkstra(used_dijkstra):
    if used_dijkstra:
        return "Dijkstra"
    else:
        return "BFS"
    
def is_cheap(used_dijkstra, ratio):
    if ratio!=0:
        return "5050"
    elif used_dijkstra:
        return "Full"
    else:
        return "None"

def is_w(cheap_w, oracle):
    if cheap_w:
        return "CarthesianDijkstra"
    elif oracle == "wp":
        return "Wp"
    else:
        return "HADSINT"

def generate_graphs(experiment_result, variable, value, kind, out, exp_name):
    normalvalue = test_vars[0]


    ###pre-processing

    data = pd.read_csv(experiment_result, sep=",")[['name', 'seed', 'used_dijkstra', 'sort_style', 'infix_gen', 'oracle', 'cheap_w', 'ratio', value, 'num_states']]

    data = data[data['seed']!=2610]
    data['piw_style'] = data.apply(lambda x: is_cheap(x['used_dijkstra'], x['ratio']), axis=1)
    data['p_gen'] = data.apply(lambda x: is_dijkstra(x['used_dijkstra']), axis=1)
    data['w_style'] = data.apply(lambda x: is_w(x['cheap_w'], x['oracle']), axis=1)

    order = pd.read_csv("model_order.csv", sep=",")[['name','nr']].set_index('name').to_dict()['nr']

    expr_data = data[['name', variable, value]]
    expr_data = expr_data[expr_data['name'].isin(order.keys())]
    possible_values = expr_data[variable].unique()

    #sort models by nr of states              The commented code here was ran once, from then on the output of this was used to sort the models instead. This ensures consistency of order
    #states_data = data.groupby(['name']).mean('num_states')
    #numstates_dict = states_data['num_states'].to_dict()
    models = expr_data.name.unique()
    #models = sorted(models, key=lambda x:numstates_dict[x])
    #print(models)
#
    #with open("model_order.csv", 'w') as f:
    #    for i, m in enumerate(models):
    #        f.write(m + "," + str(i) +"\n")

    models = sorted(models, key=lambda x:order[x])
    

    ###prepare data
    boxplot_data = []  #data for boxplot
    mean_diff_data = [] #data will be used to calculate mean difference
    for pv in possible_values:
        data_plots = []
        mean_plots = []
        for m in models:
            data = expr_data[(expr_data[variable]==pv) & (expr_data['name']==m)][value]
            mean = data.mean()
            data_plots.append(data)
            mean_plots.append(mean)
        boxplot_data.append(data_plots)
        mean_diff_data.append(mean_plots)

    value_index = get_value_index(variable, possible_values)
    

    ###making boxplot of cost data
    #print(test_vars)
    #print(list(possible_values))
    #write_csv_boxplot(models, boxplot_data)
    for tv in test_vars:
        if tv==test_vars[0]:
            continue
        if "CARTHESIAN" in experiment_result:
            make_boxplot(models, boxplot_data, value_index, tv, normalvalue, variable, kind, out + tv + "_") # swapped normalvalue and changed value tv
            
        else:
            make_boxplot(models, boxplot_data, value_index, normalvalue, tv, variable, kind, out + tv + "_")
            


    ###getting information and making average cost comparison graphs
    diffdata = pd.DataFrame() 
    diffdata['name'] = models
    for tv in test_vars:
        if tv==test_vars[0]:
            continue
        diffs = []
        if "CARTHESIAN" in experiment_result:
            diffs = prepare_bardata(mean_diff_data, tv, normalvalue, value_index) #swapped normalvalue and changedvalue tv
        else:
            diffs = prepare_bardata(mean_diff_data, normalvalue, tv, value_index)
        

        make_barplot(models, diffs, tv, variable, kind,  out + tv + "_")
        diffdata[tv] = diffs
    write_csv_barplot(models, diffdata, exp_name)

def write_csv_barplot(models, bpdf, exp_name):
    #bpdf = pd.DataFrame({'name': models, 'average_difference' : diffs})
    bpdf.to_csv('graphs/csvs/' + exp_name + '_AVERAGES.csv')
    print('graphs/csvs/' + exp_name + '_AVERAGES.csv' + " was created.")

def write_csv_boxplot(models, boxplot_data):
    #print(len(models))
    #print(models)
    #print(len(boxplot_data))
    #print(boxplot_data)
    pass

def make_boxplot(models, costs, value_index, normalvalue, changedvalue, variable, kind, out):
    fig, ax = plt.subplots(ncols=1, nrows=1)
    fig.set_size_inches(12, 6)

    hypothesis= kind=="hyp"

    red =  dict(color=sort_colour[normalvalue])
    blue = dict(color=sort_colour[changedvalue])
    redthin =  dict(color=sort_colour[normalvalue], width=0)
    bluethin = dict(color=sort_colour[changedvalue], width=0)

    ax.boxplot(costs[value_index[normalvalue]], positions=range(0, len(costs[value_index[normalvalue]])*3, 3)    , widths=0, showfliers=False, boxprops=red, medianprops=red, flierprops=red, meanprops=red, whiskerprops=red, capprops=red, whis=(5, 95))
    ax.boxplot(costs[value_index[changedvalue]], positions=range(1, len(costs[value_index[changedvalue]])*3+1, 3), widths=0, showfliers=False, boxprops=blue, medianprops=blue, flierprops=blue, meanprops=blue, whiskerprops=blue, capprops=blue, whis=(5, 95))


    ax.boxplot(costs[value_index[normalvalue]], positions=range(0, len(costs[value_index[normalvalue]])*3, 3)    , widths=0.8, showfliers=False, showbox=False, medianprops=red, flierprops=red, meanprops=red, whiskerprops=red, capprops=red, whis=(5, 95))
    ax.boxplot(costs[value_index[changedvalue]], positions=range(1, len(costs[value_index[changedvalue]])*3+1, 3), widths=0.8, showfliers=False, showbox=False, medianprops=blue, flierprops=blue, meanprops=blue, whiskerprops=blue, capprops=blue, whis=(5, 95))
    #violinparts = ax.violinplot(costs[value_index[normalvalue]], positions=range(0, len(costs[value_index[normalvalue]])*3, 3), widths=1.8, bw_method=0.0, showmedians=True)#quantiles=[0.05, 0.95]
    #violinparts = ax.violinplot(costs[value_index[changedvalue]], positions=range(1, len(costs[value_index[changedvalue]])*3+1, 3), widths=1.8, bw_method=0.0, showmedians=True)

    predef_model_nr = ["","6","","","21","","","32","","","35","","","40","","","44","","","45",""]
    if kind=="def":
        ax.set_xticks(range(0, len(predef_model_nr)), predef_model_nr, ha='right')
    #zero = [0]*(len(models)+1)
    #ax.plot(zero, color='black')
    ax.set_yscale('log')
    ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=(kind=="def")) # labels along the bottom edge are off
    ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost',
        title='Cost of finding a counterexample using BFS or Dijkstra P-set')
    
    #ax.set_autoscale_on(False)
    #ax.set_ylim([0,100000000])

    blue = mpatches.Patch(color=sort_colour[normalvalue])
    red = mpatches.Patch(color=sort_colour[changedvalue])

    ax.yaxis.grid(color='gray')
    ax.legend([blue, red], [normalvalue, changedvalue], prop=medium_text)

    #ax.set_xticks(range(1,len(models)+1), range(0, len(models)), rotation=90, ha='right')
    
    ax.set_xlabel("", fontdict=medium_text)
    ax.set_ylabel("", fontdict=medium_text)
    fac = "finding a counterexample " if hypothesis else " learning a model"
    title = ""

    "Costs of " + fac + "with " + normalvalue + " and " + changedvalue
    
    ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost')
    title='Costs of normal L# vs 20 pre-generated sequences sorted by ' + changedvalue
    
    
    if variable=="infix_gen":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost')
        title='Costs of' + fac + ' with ' + changedvalue + ' vs Random I vs Uniform Random I'
        
    if variable=="p_gen":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost')
        title='Costs of' + fac + ' with ' + changedvalue + ' P compared to BFS P'

    if variable=="piw_style":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost')
        title='Costs of' + fac + ' with PIW set to ' + changedvalue + ' compared to None'
    
    if variable=="w_style":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Cost')
        title='Costs of' + fac + ' with Cartesian-Dijkstra W vs ' + changedvalue + " W"
    plt.title(title, fontdict=medium_text)

    plt.savefig(out + "BOXPLOT.png")
    print(out + "BOXPLOT.png" + " was created.")
    #plt.show()


def get_value_index(variable, possible_values):
    value_index = {}
    for pv in possible_values:
        value_index[str(pv)] = possible_values.tolist().index(pv)
    return value_index


def prepare_bardata(mean_diff_data, normalvalue, changedvalue, value_index):
    normal_data = mean_diff_data[value_index[normalvalue]]
    changed_data = mean_diff_data[value_index[changedvalue]]

    diffs = []# data for average difference plot    
    for i, m in enumerate(normal_data):
        difference = normal_data[i]-changed_data[i] #old-new
        fraction = difference/normal_data[i]  # diff / old
        percentage = fraction * 100
        diffs.append(fraction)
        extra = " " if (fraction>=0) else ""
        #print(extra + "%.2f" % percentage + "%\t" + model_names[i] + ":new: " + str(changed_data[i]) + ", old:" + str(normal_data[i]))
    return diffs


def make_barplot(models, diffs, changedvalue, variable, kind, out):
    fig, ax = plt.subplots(ncols=1, nrows=1)
    fig.set_size_inches(12, 6)

    hypothesis = kind == "hyp"

    #zero = [0]*(len(diffs)+2)
    #ax.plot(zero, color='black')
    plt.axhline(y=0, color='black')

    predef_model_nr = ["6","21","32","35","40","44","45"]

    toplim = 0.3
    botlim = -0.5

    if variable == "p_gen":
        toplim = 0.15
        botlim = -0.15
    if variable == "infix_gen":
        toplim = 1
        botlim = -6
    if variable == "w_style" and not hypothesis:
        toplim = 0.5
        botlim = -1
    if variable=="piw_style":
        toplim = 1
        botlim = -6

    bl = ax.bar(range(0, len(models)), diffs, color=sort_colour[changedvalue])
    br = ax.bar_label(bl, [('{:,.2%}'.format(x) if (x<(botlim+0.07) or x>(toplim-0.07) ) else "") for x in diffs], label_type='center', rotation=(90 if kind!="def" else 0)) # write result on bar if the bar is larger than the plot
    
    for i, label in enumerate(br):
        if i%2==0:
            label.set(y=0.7)

    if kind!="def":
        ax.bar_label(bl, [(str(x) if x%1==0 else "") for x in range(len(models))], fontsize=8)
    else:
        ax.bar_label(bl, predef_model_nr, fontsize=8)

    ax.set_autoscale_on(False)
    ax.set_ylim([botlim,toplim])

    #yticks  = [x/20 for x in range((int(botlim*20)), (int(toplim*20))+1, 1)]
    ax.set_yticks(ax.get_yticks()) #avoid UserWarning: FixedFormatter should only be used together with FixedLocator

    #ax.set_xticks(range(0,len(models)), models, rotation=90)
    if kind=="def":
        ax.set_xticks(range(0, len(diffs)), predef_model_nr, rotation=0)
    ax.set_yticklabels(['{:,.2%}'.format(x) for x in ax.get_yticks()])
    ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    ax.set_axisbelow(True)
    ax.yaxis.grid(color='gray')

    ax.set_xlabel("", fontdict=medium_text)
    ax.set_ylabel("", fontdict=medium_text)

    
    fac = " finding counterexample, " if hypothesis else ""
    title = ""

    ax.set(xlabel='Models (ordered by number of states)', ylabel='Average Cost Saved')
    title='Average cost saved,' + fac + ' sorted by ' + changedvalue #'Average testing phase cost saved compared to normal L# with 20 pre-generated sequences sorted by '
    
    
    if variable=="infix_gen":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Average Cost Saved')
        title='Average cost saved,' + fac + ' with ' + changedvalue + ' Random Walk Compared to Uniform Random Walk.'
        
    if variable=="p_gen":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Average Cost Saved')
        title='Average cost saved,' + fac + ' with ' + changedvalue + ' P vs BFS P'

    if variable=="piw_style":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Average Cost Saved')
        title='Average cost saved,' + fac + ' Measures: ' + changedvalue + ', vs None'
    
    if variable=="w_style":
        ax.set(xlabel='Models (ordered by number of states)', ylabel='Average Cost Lost')
        title='Average cost saved,' + fac + 'Cartesian-Dijkstra W vs ' + changedvalue + ' W'
        
    
    plt.title(title, fontdict=medium_text)
    
    plt.savefig(out + "AVERAGE.png")
    print(out + "AVERAGE.png" + " was created.")
    #plt.show()

sort_colour = {'OccurenceReversed':     'tab:purple', 
               'LengthPerCost':         'tab:cyan', 
               'LengthReversed':        'tab:pink',
               'OccurencePerCost':      'lightgray', 
               'Occurence':             'green', 
               'Length':                'orangered', 
               'CostReversed':          'tab:pink',
               'Presence':              'black', 
               'Cost':                  'tab:blue', 
               'CostPerOccurence':      'tab:olive', 
               'CostPerLength':         'tab:orange',
               'Random':                'black',
               'Weighted':              'lightcoral',
               'CarthesianDijkstra':    'orangered',
               'HADSINT':               'tab:green', 
               'Wp':                    'tab:blue', 
               'None':                  'black', 
               '5050':                  'tab:blue', 
               'Full':                  'orangered', 
               'Dijkstra':              'tab:orange', 
               'BFS':                   'black', 
               }

test_vars = ['Presence',
            'OccurenceReversed',
            'LengthPerCost', 
            'LengthReversed',
            'OccurencePerCost', 
            'Occurence', 
            'Length', 
            'CostReversed',
            'Cost', 
            'CostPerOccurence', 
            'CostPerLength']


big_text = {'weight' : 'normal', 'size' : 22}
medium_text = {'weight' : 'normal', 'size' : 16}
small_text = {'weight' : 'normal', 'size' : 10}
tiny_text = {'weight' : 'normal', 'size' : 8}

if __name__ == "__main__":
    
    experiment_name = sys.argv[1].strip()
    variable = ""
    value = "test_cost"

    stop=False

    directory = "expr_results/" + experiment_name + "_results.csv"
    hypo_directory = "expr_results/" + experiment_name + "_hypothesis_results.csv"
    predef_directory = "expr_results/" + experiment_name + "_predefweights_results.csv"

    if experiment_name=="SORTvsNORMAL":
        variable = "sort_style"
    elif experiment_name=="BFSvsDIJKSTRA":
        variable = "p_gen"
        test_vars = ['BFS', 'Dijkstra'] 
    elif experiment_name=="CHEAPPIWvsNORMAL":
        variable = "piw_style"
        test_vars =  ['None','5050', 'Full']
    elif experiment_name=="CARTHESIANDIJKSTRAvsHADSINTvsWP":
        variable = "w_style"
        test_vars =  ['CarthesianDijkstra','HADSINT', 'Wp']
    elif experiment_name=="RANDOMvsWEIGHTED":
        variable = "infix_gen"
        test_vars = ["Random", "Weighted"] 
    else:
        print("No valid experiment found, try again")
        stop=True
        
    if not stop:
        generate_graphs(directory, variable, value, "non",                       "graphs/" + experiment_name + "_", experiment_name)
        generate_graphs(hypo_directory, variable, value, "hyp",                   "graphs/hypothesis/" + experiment_name + "_hypothesis_", experiment_name + "_hypothesis")
        generate_graphs(predef_directory, variable, value, "def",               "graphs/predef/" + experiment_name + "_predef_", experiment_name + "_predef")

