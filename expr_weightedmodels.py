#from progress.bar import Bar
from concurrent.futures import ProcessPoolExecutor
import os
import datetime
import re
import subprocess
from time import sleep
import random




# model_names = ["hyp.0.obf"]

seeds = [
    81,
    100,
    158,
    216,
    245,
    359,
    366,
    470,
    560,
    578,
    580,
    597,
    661,
    689,
    692,
    783,
    818,
    879,
    930,
    968,
    995,
    1004,
    1005,
    1190,
    1205,
    1257,
    1320,
    1534,
    1541,
    1596,
    1607,
    1665,
    1836,
    1989,
    2015,
    2143,
    2147,
    2199,
    2221,
    2263,
    2283,
    2365,
    2370,
    2408,
    2495,
    2528,
    2554,
    2558,
    2561,
    2588,
    2610,
    # 2619,
    # 2679,
    # 2773,
    # 2816,
    # 2950,
    # 2966,
    # 2969,
    # 2983,
    # 3044,
    # 3101,
    # 3131,
    # 3147,
    # 3169,
    # 3209,
    # 3211,
    # 3213,
    # 3235,
    # 3265,
    # 3350,
    # 3383,
    # 3415,
    # 3444,
    # 3496,
    # 3528,
    # 3588,
    # 3658,
    # 3743,
    # 3769,
    # 3806,
    # 3809,
    # 3900,
    # 3980,
    # 4094,
    # 4179,
    # 4358,
    # 4370,
    # 4447,
    # 4467,
    # 4535,
    # 4550,
    # 4588,
    # 4632,
    # 4646,
    # 4689,
    # 4782,
    # 4845,
    # 4948,
    # 4950,
    # 4973,
]

def get_lsharp_command(name, oracle, seed, es, rnd_len, dijkstra, infix_gen, piw_listsize, piw_sortstyle, budget, ratio, pre, cw, weight_file, extra):
    # when adding new parameters to test, remember to not only add them to the generated command here, but also to add the parameter to the output result file name in some form so you dont overwrite what you actually want to test. 
    # additionaly, add it to the header string in analysis.py 's combine_resultfiles()
    org_dir = "experiment_models/"
    model = org_dir + name + ".dot"
    # log = org_dir + name + ".traces"
    returnlist = [
        "target/release/lsharp",
        "-e",
        oracle,
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-k",
        str(es),
        "-l",
        str(rnd_len),
        "-x",
        str(seed),
        "--eq-mode",
        "infinite",
        "-i",
        infix_gen,
        # "--traces",
        # log,
        "-m",
        model,
        "-v",
        "1",
        "--out",
        "csvs/results" + name + str(seed) + oracle + dijkstra + "_" + infix_gen + "_n" + str(piw_listsize) + piw_sortstyle + "pt" + str(pre) + cw + "ratio_" + str(ratio) + "budg" + str(budget) + extra + ".csv",
        #"--compress-tree",
        #"-q",
        "-w",
        weight_file,
        "-n",
        str(piw_listsize),
        "-s",
        piw_sortstyle,
        "-b",
        str(pre),
        "-r",
        str(ratio),
        "-u",
        str(budget)
    ]
    if len(dijkstra)>0 :
        returnlist.append("-d")
    if len(cw)>0:
        returnlist.append("-c")
    return returnlist



def run_expr(cmd):

    pr = subprocess.Popen(cmd)
    if pr.wait() != 0:
        Exception("Experiment failed!")
        exit(-1)
    return

def input_list(file):
    with open(file) as f:
        actions = []
        for line in f:
            if "->" in line:
                regex = r".*\"(.*)/.*"
                action = re.findall(regex, line)
                if action:
                    if action[0] not in actions:
                        actions.append(action[0])
        return actions
        
def output_list(file):
    with open(file) as f:
        outputs = []
        for line in f:
            if "->" in line:
                regex = r".*/(.*)\".*"
                output = re.findall(regex, line)
                if output:
                    if output[0] not in outputs:
                        outputs.append(output[0])
        return outputs
        
    
def generate_random_weight_file(actions, filename, expensive_cost):
    leng = (len(actions)//2)+1
    ub = leng
    lb = 2 if leng>2 else 1
    expensive_actions = random.sample(actions, random.randrange(lb, ub))

    #print(len(actions), len(expensive_actions))
    with open(filename, 'w' ) as f:
        for a in expensive_actions:
            f.write(a + ":" + str(expensive_cost) + "\n")

def current_moment_string():
    return datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S_%f')

model_names = [
    #"exampleweight",
    #"example",
    #"BitVise",
    #"DropBear",
    #"GnuTLS_3.3.8_client_full",   // heavy
    "NSS_3.17.4_client_full",
    #"ASML_3_3_3_3", 
    #"OpenSSH_3_3_3",              // heavy sometimes
    #"10_learnresult_MasterCard_fix",
    #"TCP_Linux_Client",
    #"TCP_Linux_Server",
    #"Rabo_learnresult_MAESTRO_fix",
    #"Rabo_learnresult_SecureCode_Aut_fix",
    #"GnuTLS_3.3.8_client_regular",
    #"GnuTLS_3.3.8_server_regular",
]

exp_models = [
    "1_learnresult_MasterCard_fix",
    "4_learnresult_MAESTRO_fix",
    "4_learnresult_PIN_fix",
    "4_learnresult_SecureCode_Aut_fix",
    "10_learnresult_MasterCard_fix",
    "ASML_3_3_3_3",
    "ASN_learnresult_MAESTRO_fix",
    "ASN_learnresult_SecureCode_Aut_fix",
    #"BitVise",
    "DropBear",
    "GnuTLS_3.3.8_client_full",
    "GnuTLS_3.3.8_client_regular",
    "GnuTLS_3.3.8_server_full",
    "GnuTLS_3.3.8_server_regular",
    "GnuTLS_3.3.12_client_full",
    "GnuTLS_3.3.12_client_regular",
    "GnuTLS_3.3.12_server_full",
    "GnuTLS_3.3.12_server_regular",
    "learnresult_fix",
    "miTLS_0.1.3_server_regular",
    #"model1",
    #"model3",
    #"model4",
    "NSS_3.17.4_client_full",
    "NSS_3.17.4_client_regular",
    "NSS_3.17.4_server_regular",
    "OpenSSH",
    #"OpenSSH_2_2_2",
    #"OpenSSH_3_3_3",
    "OpenSSL_1.0.1g_client_regular",
    "OpenSSL_1.0.1g_server_regular",
    "OpenSSL_1.0.1j_client_regular",
    "OpenSSL_1.0.1j_server_regular",
    "OpenSSL_1.0.1l_client_regular",
    "OpenSSL_1.0.1l_server_regular",
    "OpenSSL_1.0.2_client_full",
    "OpenSSL_1.0.2_client_regular",
    "OpenSSL_1.0.2_server_regular",
    "Rabo_learnresult_MAESTRO_fix",
    "Rabo_learnresult_SecureCode_Aut_fix",
    "RSA_BSAFE_C_4.0.4_server_regular",
    "RSA_BSAFE_Java_6.1.1_server_regular",
    "TCP_FreeBSD_Client",
    "TCP_FreeBSD_Server",
    "TCP_Linux_Client",
    "TCP_Linux_Server",
    "TCP_Windows8_Client",
    "TCP_Windows8_Server",
    #"TestRing",
    "Volksbank_learnresult_MAESTRO_fix"
]

# change these lists below to determine the kind of experiment you want to run 

# oracles = ["hads-int", "iads"]
oracles = ["hads-int"]#, "hsi", "w", "wp"]
# oracles = ["iads"]
# oracles = ["soucha-spy", "soucha-spyh"]
# oracles = ["soucha-spy"]

sds = [34]#seeds[0]#[145, 809, 4295, 55701, 1098, 3109, 345, 6, 9933, 251]#, 1098]
all_results = []
cmds = []
es = [3]#[1, 2]
rnd_lens = [2]#[2, 3]
dijk = [""]#["", "-d"]#,"-d"]#["", "-d"]
infixers = ['random']#['random', 'weighted']#, 'weighted']#["random", "weighted"]#, "inverted"]
cheap_ws = [""]#["", "-c"]
ratios = [0]#[2, 5, 100]
budgets = [0]
piw_listsizes = [1]#[1, 2, 4, 8, 12, 16, 20, 24, 32, 64]

randomweights = False   # When True, the randomly generated weightfiles will contain the strings from the ws list,
                        # When False, the filename of the results will contain the the ppart after the second slash.

ws = ["weights/custom/weightsNeutral.txt"]#[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]#[ "weights/custom/IdealOpenSSH.txt"]#,"weights/custom/weightsneutral.txt"
pre_input = [0]#[0, 10, 50, 100]
sort_styles = ["cost"]#, "cost-reversed", "length", "cost-per-length", "length-per-cost"]#, "length"]
expensive_costs = [10]

#the order of these fors does actually matter in some cases. For example, it might be needed to create a single random weight file that is used with different parameters.



for k in es:
    for oracle in oracles:
        for rnd_len in rnd_lens:
            for model in exp_models:
                for exp_cost in expensive_costs:
            
                    for w in ws:
                        model_file = "experiment_models/" + model + ".dot"
                        weightfile = w

                        if randomweights:
                            weightfile = "weights/genned/" + model + oracle + current_moment_string() + str(exp_cost)  + ".txt"
                            actions = input_list(model_file)
                            generate_random_weight_file(actions, weightfile, exp_cost)
                            extra = "w_" + str(w)
                        else:
                            extra = "w" + w.split('/')[2]

                        for ratio in ratios:
                            for budget in budgets:
                                for cw in cheap_ws:
                                    for s in sort_styles:
                                        for pre in pre_input:
                                            for seed in sds: # was seed in seeds
                                                for d in dijk:
                                                    for i in infixers:
                                                        for n in piw_listsizes:
                                                            #if ((d == "-d") & (i == "weighted") & (cw == "-c")) | ((d == "") & (i == "random") & (cw == "")):
                                                            cmd = get_lsharp_command(model, oracle, seed, k, rnd_len, d, i, n, s, budget, ratio, pre, cw, weightfile, extra) #use w instead of weight_file if you dont want random weights
                                                            cmds.append(cmd)















# if __name__ == "__main__":
#     with Bar("Running experiments", max=len(cmds), suffix="%(percent)d%%") as bar:
#         for cmd in cmds:
#             run_expr(cmd)
#             bar.next()


if __name__ == "__main__":
    with ProcessPoolExecutor(max_workers=1) as executor:
        executor.map(run_expr, cmds)
        