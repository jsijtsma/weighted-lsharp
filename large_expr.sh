#!/bin/bash
python3 expr_run.py BFSvsDIJKSTRA large_models.txt $1 $2 10 non
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP large_models.txt $1 $2 10 non
python3 expr_run.py CHEAPPIWvsNORMAL large_models.txt $1 $2 10 non
python3 expr_run.py RANDOMvsWEIGHTED large_models.txt $1 $2 10 non
python3 expr_run.py SORTvsNORMAL large_models.txt $1 $2 10 non
python3 expr_run.py BFSvsDIJKSTRA large_models.txt $1 $2 10 hypothesis
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP large_models.txt $1 $2 10 hypothesis
python3 expr_run.py CHEAPPIWvsNORMAL large_models.txt $1 $2 10 hypothesis
python3 expr_run.py RANDOMvsWEIGHTED large_models.txt $1 $2 10 hypothesis
python3 expr_run.py SORTvsNORMAL large_models.txt $1 $2 10 hypothesis
python3 combine_results.py BFSvsDIJKSTRA
python3 combine_results.py CARTHESIANDIJKSTRAvsHADSINTvsWP
python3 combine_results.py CHEAPPIWvsNORMAL
python3 combine_results.py RANDOMvsWEIGHTED
python3 combine_results.py SORTvsNORMAL