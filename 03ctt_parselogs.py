#!/usr/bin/python3

import ast
import glob
import os
import pathlib
import re
import sys
from typing import Any

import numpy as np
import pandas as pd

from expr_models import get_lsharp_command, run_expr

# REGEX to separate log file lines generated as set in logback.xml file in the cttlearning Java project

# Let the example of log file line: "2022-01-20 13:18:12 INFO Benchmarking |ClosingStrategy: CloseFirst"
# Below, each REGEX part is presented with comments indicating the matching parts of the example above.
P_LOG = re.compile(
    # r"([0-9]+-[0-9]+-[0-9]+\s*"  # Log timestamp date in format yyyy-MM-dd (e.g., 2022-01-20 )
    # r"[0-9]+:[0-9]+:[0-9]+)\s*"  # Log timestamp time in format HH:mm:ss   (e.g., 13:18:12 )
    # r"(?P<lvl>[\w]+)\s*"  # Outputs the level of the logging event (e.g., INFO)
    # r"(?P<clz>\w+)\s*"  # Outputs origin of the logging event. (e.g., Benchmarking class)
    r".*\|\s*(?P<key>[^:]+):\s*"  # cttlearning log format "Message key:"  (e.g., "ClosingStrategy:")
    r"(?P<val>.+)"  # cttlearning log format "Message value" (e.g., "CloseFirst")
)

# More information on the logback layout parameters used in:
# - cttlearning's logback.xml file: https://github.com/damascenodiego/cttlearn/blob/5fad701d1fffc1397aa49d7eb5fec56e8442f400/src/cttlearning/src/main/resources/logback.xml#L20
# - Logback official website:       https://logback.qos.ch/manual/layouts.html#conversionWord

(_, results_path, out_csv_name) = sys.argv


# results_path = "./log_comp_mem/"

logs_dict = {}
stats_overall = {
    # "ClosingStrategy": [],
    # "ObservationTableCEXHandler": [],
    "SUL name": [],
    "Seed": [],
    "Cache": [],
    "EquivalenceOracle": [],
    "Method": [],
    "Rounds": [],
    "MQ [Resets]": [],
    "MQ [Symbols]": [],
    "EQ [Resets]": [],
    "EQ [Symbols]": [],
    # "Learning [ms]": [],
    # "Searching for counterexample [ms]": [],
    "Qsize": [],
    "Isize": [],
    "Equivalent": [],
    "Info": [],
}

overall_keys = [
    "SUL name",
    "Seed",
    "Cache",
    "EquivalenceOracle",
    "Method",
    "Rounds",
    "MQ [Resets]",
    "MQ [Symbols]",
    "EQ [Resets]",
    "EQ [Symbols]",
    "Qsize",
    "Isize",
    "OTreeSize",
    "Equivalent",
    "OTreeSize",
    "TreeType",
    # "Info",
]


def is_some(x: Any) -> bool:
    return x is not None


logfiles = pathlib.Path(results_path).expanduser().glob("*.*")


def dict_from_logpath(log):
    log_dict = dict()
    with open(cttl_log) as cttl_f:
        matches = map(P_LOG.match, cttl_f.readlines())
        for m in matches:
            if m == None:
                continue
            key, val = m.groups()
            key = key.strip()
            val = val.strip()
            if key == None or val == None:
                exit(-1)
            if key not in overall_keys:
                continue
            log_dict[key] = val
    errored = False
    for x in overall_keys:
        if x not in log_dict:
            errored = True
            # print("Logfile could not be read! : ")
            # print(log)
            # print(f"Key: {x}")
    if errored:
        print("Logfile could not be read! : ")
        model = log_dict["SUL name"][:-4]
        eo = log_dict["EquivalenceOracle"]
        seed = log_dict["Seed"]
        print(log)
        print(f"Model:  {model}, EO: {eo}, Seed: {seed}")
        cmd = get_lsharp_command(model, "hads-int" if "H" in eo else "iads", seed)
        os.remove(log)
        run_expr(cmd)
    return log_dict


stats_overall = list()
for cttl_log in logfiles:
    log_dict = dict_from_logpath(cttl_log)
    stats_overall.append(log_dict)

df_overall = pd.DataFrame(stats_overall)
df_overall.to_csv(os.path.join(out_csv_name), index=False)


# for cttl_log in logfiles:
#     try:
#         with open(cttl_log) as cttl_f:
#             tmp_stats = {k: None for k in stats_overall.keys()}
#             tmp_iter = []
#             counter_EQStats = 0
#             line = "to_start_iteration"
#             matches = map(P_LOG.match, cttl_f.readlines())
#             # lines = filter(is_some, matches)
#             for m in matches:
#                 # m = P_LOG.match(line)
#                 if m is None:
#                     continue

#                 m_dict = m.groupdict()
#                 if len(m_dict) == 0:
#                     continue
#                 m_dict["key"] = m_dict["key"]  # .strip()
#                 m_dict["val"] = m_dict["val"]  # .strip()
#                 assert m_dict["val"] != None
#                 if m_dict["key"] in stats_overall.keys():
#                     tmp_stats[m_dict["key"]] = m_dict["val"]  # type: ignore
#                 if m_dict["key"] == "EQStats":
#                     try:
#                         d_stats = ast.literal_eval(m_dict["val"])
#                         tmp_iter.append(d_stats)
#                     finally:
#                         pass
#             for k, v in stats_overall.items():
#                 v.append(tmp_stats[k])

#             for d_stats in tmp_iter:
#                 for k, v in tmp_stats.items():
#                     if not k in stats_iter.keys():
#                         continue
#                     d_stats[k] = v
#                 # for k, v in d_stats.items():
#                 #     stats_iter[k].append(v)
#             # print(f"{stats_overall}")
#             # x = input("Press Enter\n")
#             # if x == "q":
#             #     exit(-1)
#             # print(f"{stats_iter}")
#     except:
#         print(f"Could not read file: {cttl_log}")

# df_overall = pd.DataFrame.from_dict(stats_overall)
# df_overall.to_csv(os.path.join("df_overall.csv"), index=False)

# # df_iter = pd.DataFrame.from_dict(stats_iter)
# # df_iter.to_csv(os.path.join("df_iter.csv"), index=False)
