#![warn(clippy::pedantic)]

//! # L# Learning Library
//!
//! This library implements the L# learning algorithm
//! as mentioned in the paper "``A New Approach for Active Automata Learning Based on Apartness``"
//!  at TACAS 2022, [link](<https://rdcu.be/cO36l>).
//! Source code is available [here](<https://gitlab.science.ru.nl/sws/lsharp>).
//! Docs are available on [GitLab](<https://sws.pages.science.ru.nl/lsharp/lsharp_ru/index.html>).

pub mod ads;
pub mod definitions;
pub mod learner;
pub mod oracles;
pub mod sul;
pub mod util;
