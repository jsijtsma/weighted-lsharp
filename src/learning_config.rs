//! Learning configuration options and `LearnResult` struct.
//!
//! Contains enums for the equivalence oracle, rules two and three,
//! and lastly a struct for storing the learning results.

use lsharp_ru::oracles::equivalence::{InfixGenerator, generic::CandidateListSortMethod};
use num_format::{Locale, ToFormattedString};
use std::fmt;

/// Equivalence oracle.
#[derive(PartialEq, Eq, Clone, Debug, clap::ValueEnum)]
pub enum EqOracle {
    /// Internal H-ADS oracle (*default*).
    HadsInt,
    /// [`Hybrid-ADS`](lsharp_ru::oracles::equivalence::hads::Oracle) (*broken, do not use!*).
    HadsTree,
    /// Simple shortest separating sequence (can be used iff the FSM is known.)
    Internal,
    /// Broken, do not use!
    SepSeq,
    /// FSMLib HSI implementation, see [`SouchaOracle`](lsharp_ru::oracles::equivalence::soucha::Oracle).
    SouchaH,
    /// FSMLib HSI implementation, see[`SouchaOracle`](lsharp_ru::oracles::equivalence::soucha::Oracle).
    SouchaHSI,
    /// FSMLib HSI implementation, see [`SouchaOracle`](lsharp_ru::oracles::equivalence::soucha::Oracle).
    SouchaSPY,
    /// FSMLib HSI implementation, see [`SouchaOracle`](lsharp_ru::oracles::equivalence::soucha::Oracle).
    SouchaSPYH,
    /// IADS implementation, see [`IadsOracle`](lsharp_ru::oracles::equivalence::hads::Oracle).
    Iads,
    /// W-Method.
    W,
    /// W-Method.
    Wp,
    /// Basic separating sequences.
    Separating,
    Hsi,
    /// Logged version of H-ADS.
    LoggedHads,
    /// Logged version of I-ADS.
    LoggedIads,
    /// External W-method from StateChum.
    ExtW,
    /// Chained equivalence oracles.
    Chained,
}
impl fmt::Display for EqOracle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

/// Stores the metrics of the learning process.
pub struct LearnResult {
    pub rounds: usize,
    pub success: bool,
    pub pre_inputs: usize,
    pub pre_resets: usize,
    pub pre_cost: usize,
    pub learn_inputs: usize,
    pub learn_resets: usize,
    pub learn_cost: usize,
    pub test_inputs: usize,
    pub test_resets: usize,
    pub test_cost: usize,
    pub eq_oracle: EqOracle,
    pub num_states: usize,
    pub num_inputs: usize,
    pub ads_score: f32,
    pub cost: usize,
    pub dijkstra: bool,
    pub infix_gen: InfixGenerator,
    pub piw_listsize: usize,
    pub pre_input: usize,
    pub piw_sortstyle: CandidateListSortMethod,
    pub cheap_w: bool,
    pub budget: usize,
    pub ratio: usize,
}

impl LearnResult {
    #[must_use]
    pub fn to_csv_entry(&self) -> String {
        format!(
            "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
            self.success,
            self.rounds,
            self.num_states,
            self.num_inputs,
            self.pre_inputs,
            self.pre_resets,
            self.pre_cost,
            self.learn_inputs,
            self.learn_resets,
            self.learn_cost,
            self.test_inputs,
            self.test_resets,
            self.test_cost,
            self.ads_score,
            self.cost,
            self.dijkstra,
            self.infix_gen,
            self.piw_listsize,
            self.piw_sortstyle,
            self.cheap_w,
            self.budget,
            self.ratio,
        )
    }
}

/// Only used when printing to stdout.
impl fmt::Display for LearnResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Number of rounds : {}", self.rounds).unwrap();
        writeln!(f, "Model learned : {}", self.success).unwrap();
        writeln!(
            f,
            "Pre Inputs / Resets / Cost: {} / {} / {}",
            self.pre_inputs.to_formatted_string(&Locale::en),
            self.pre_resets.to_formatted_string(&Locale::en),
            self.pre_cost.to_formatted_string(&Locale::en)
        ).unwrap();
        writeln!(
            f,
            "Learning Inputs / Resets / Cost: {} / {} / {}",
            self.learn_inputs.to_formatted_string(&Locale::en),
            self.learn_resets.to_formatted_string(&Locale::en),
            self.learn_cost.to_formatted_string(&Locale::en)
        )
        .unwrap();
        writeln!(
            f,
            "Testing Inputs / Resets / Cost: {} / {} / {}",
            self.test_inputs.to_formatted_string(&Locale::en),
            self.test_resets.to_formatted_string(&Locale::en),
            self.test_cost.to_formatted_string(&Locale::en)
        ).unwrap();
        writeln!(
            f,
            "Total Weighted Cost : {}",
            self.cost.to_formatted_string(&Locale::en),
        )
    }
}
