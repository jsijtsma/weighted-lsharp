use log::LevelFilter;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
};
use lsharp_ru::{
    definitions::mealy::{InputSymbol, Mealy, OutputSymbol},
    util::{parsers::{logs::read as read_logs, machine::read_mealy_from_file}, weighted_input::weights::{weights_from_file, read_weight_from_file}},
};
use std::{collections::HashMap, fs, io::Write, path::Path};

use clap::Parser;

mod cli;
mod learning;
mod learning_config;

use learning::{learn_fsm, OptionsBuilder};

use crate::learning::find_counterexample;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Create hypothesis and log directories
    ensure_dir_exists("./hypothesis")?;
    // Create the log directory manually before execution.
    // ensure_dir_exists("./log")?;
    let log_file_name = format!(
        "log/{}.log",
        chrono::Utc::now()
            .to_string()
            .replace(".", "_")
            .replace(":", "_")
            .replace(" ", "_")
    );
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {M} | {m}{n}",
        )))
        .build(log_file_name.clone())?;

    let args = cli::Cli::parse();

    let rule2_mode = args.rule2();
    let rule3_mode = args.rule3();
    let oracle_choice = args.eq_oracle();
    let start = chrono::Utc::now();

    let path_name = args.model();
    let hypothesis_name = args.hypothesis();
    let weights_path = args.weights();
    let dijkstra = *args.dijkstra();
    let pre_input = *args.pre_input();
    let use_ly_ads = false;
    let extra_states = *args.extra_states();

    let rnd_infix_len = *args.expected_random_length();

    let seed = *args.seed();

    let infix_style = *args.eq_mode();
    let infix_gen = *args.infix_gen();

    let cheap_w = *args.cheap_w();
    let budget = *args.budget();
    let ratio = *args.ratio();

    let piw_listsize = *args.piw_listsize();
    let piw_sortstyle = *args.sort_style();

    let logging_level = *args.verbosity();
    let logging_level = match logging_level {
        0 => LevelFilter::Off,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let log_path = args.traces();
    let file = args.out();
    let is_quiet = *args.quiet();
    let compress_tree = *args.compress_tree();
    let find_ce_isolated:bool = hypothesis_name.len()>0;

    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(Root::builder().appender("logfile").build(logging_level))?;

    log4rs::init_config(config)?;
    log::info!("ClosingStrategy : ADS");
    log::info!("ObservationTableCEXHandler: N/A");
    log::info!("Method : L# ({:?}, {:?})", rule2_mode, rule3_mode);
    log::info!("Seed : {seed}");
    log::info!("EquivalenceOracle : ({oracle_choice}, {extra_states}, {rnd_infix_len})");
    log::info!("CTT : {oracle_choice}");
    log::info!("Extra States : {extra_states}");
    log::info!("Random Infix Length : {rnd_infix_len}");
    log::info!("Infix Generator : {infix_gen}");
    log::info!("Cache : Y");
    let mut result_vec = Vec::new();
    writeln!(
        &mut result_vec,
        "name,learned,rounds,num_states,num_inputs,pre_inputs,pre_resets,pre_cost,learn_inputs,learn_resets,learn_cost,test_inputs,test_resets,test_cost,ads_score,cost,used_dijkstra,infix_gen,piw_listsize,sort_style,cheap_w,budget,ratio,pre_input,weightfile,seed,learning_algorithm"
    ).expect("Could not write to the results vector!");
    let mut f =
        std::fs::File::create(file).unwrap_or_else(|file| panic!("Could not create file {file}"));
    f.write_all(&result_vec)
        .unwrap_or_else(|file| panic!("Write exception when writing to file {file}"));
    result_vec.clear();
    let exec_options = OptionsBuilder::default()
        .rule2_mode(rule2_mode.clone())
        .rule3_mode(rule3_mode.clone())
        .oracle_choice(oracle_choice.clone())
        .use_ly_ads(use_ly_ads)
        .seed(seed)
        .quiet(is_quiet)
        .comp_tree(compress_tree)
        .extra_states(extra_states)
        .infix_style(infix_style)
        .expected_rnd_length(rnd_infix_len)
        .dijkstra(dijkstra)
        .infix_gen(infix_gen)
        .piw_listsize(piw_listsize)
        .piw_sortstyle(piw_sortstyle)
        //.weightfile_name(weights)
        .pre_input(pre_input)
        .cheap_w(cheap_w)
        .budget(budget)
        .ratio(ratio)
        .build()
        .unwrap();
    if Path::new(path_name).is_file() {
        let file_name = Path::new(path_name);
        log::info!("SUL dir: {}", file_name.display());
        log::info!(
            "SUL name: {}",
            file_name
                .file_name()
                .expect("Safe")
                .to_owned()
                .to_string_lossy()
        );
        let mut hypothesis_file = Path::new(path_name); // temp filename

        println!("Currently doing: {:?}", file);

        if find_ce_isolated{                            // this is where we load the hypothesis if we want to find a single counterexample
            if !exec_options.quiet {
                println!("Finding counterexample for: {}", hypothesis_name.as_str());
            }
            if Path::new(hypothesis_name).is_file(){
                hypothesis_file = Path::new(hypothesis_name);  
            }
            else {
                if !exec_options.quiet {
                    println!("Couldn't find {} file", hypothesis_file.to_str().expect("Safe"));
                }
            }
        }



        if !exec_options.quiet {
            println!("Learning model: {}", file_name.to_str().expect("Safe"));
        }
        let (fsm, input_map, output_map) = prep_fsm_from_file(file_name.to_str().expect("Safe"))
            .unwrap_or_else(|_| {
                panic!(
                    "Error reading from DOT file for {}",
                    file_name.to_str().expect("Safe"),
                )
            });
        log::info!("Input Map{:?}", input_map);
        
        let (hypothesis, hyp_input_map, hyp_output_map) = prep_fsm_from_file(hypothesis_file.to_str().expect("Safe"))
        .unwrap_or_else(|_| {
            panic!(
                "Error reading from DOT file for {}",
                file_name.to_str().expect("Safe"),
            )
        });

        println!("INPUTMAP: {:?}, OUTPUTMAP: {:?}", input_map, output_map);
        //println!("INPUTMAP:  correct: {:?}, from hypothesis: {:?}", input_map, hyp_input_map);
        //println!("OUTPUTMAP:  correct: {:?}, from hypothesis: {:?}", output_map, hyp_output_map);


        let fsm_logs = log_path
            .as_ref()
            .and_then(|log_p| read_logs(log_p, &input_map, &output_map).ok());
        {
            let options = &exec_options;
            let file_name = file_name;
            let result_vec: &mut Vec<u8> = &mut result_vec;
            let f: &mut fs::File = &mut f;

            let weight_map = weights_from_file(&input_map, weights_path);
            let reset_cost = read_weight_from_file("RESET".to_string(), weights_path);

            let learn_results;
            if find_ce_isolated {           // this is where we split between normal L# functionality and the counterexample finding experiment
                learn_results = find_counterexample(&fsm, &hypothesis, &input_map, &output_map, options, fsm_logs.clone(), &weight_map, reset_cost);
            } else {
                learn_results = learn_fsm(&fsm, &input_map, &output_map, options, fsm_logs.clone(), &weight_map, reset_cost);
            }
            if !exec_options.quiet {
                println!("Learning finished!\n{learn_results}");
            }
            writeln!(
                result_vec,
                "{},{},{},{},{},lsharp_{:#?}_{:#?}",
                file_name.file_name().unwrap().to_str().unwrap(),
                learn_results.to_csv_entry(),
                pre_input.clone(),
                weights_path.clone(),
                seed,
                options.rule2_mode.clone(),
                options.rule3_mode.clone(),
            )
            .expect("Could not write to results file!");
            f.write_all(result_vec)
                .unwrap_or_else(|file| panic!("Write exception when writing to file {file}"));
            result_vec.clear();
        };
    } else {
        unreachable!("How can the path be neither a file nor a directory?");
    }
    let stop = chrono::Utc::now();
    let dur = stop - start;
    if !exec_options.quiet {
        println!("Time taken: {}s", dur.num_seconds());
    }
    Ok(())
}

fn ensure_dir_exists(dir_name: &str) -> std::io::Result<()> {
    // Do not remove any of these lines
    // Remove the dir (returns an error if it doesn't exist)
    // and ignore the error-case.
    let dir = Path::new(dir_name);
    // let dir = dir.canonicalize()?;
    if dir.exists() {
        fs::remove_dir_all(dir_name)?;
    }
    // Create an empty dir.
    fs::create_dir_all(dir)
}

type ParseResultFSM = Result<
    (
        Mealy,
        HashMap<String, InputSymbol>,
        HashMap<String, OutputSymbol>,
    ),
    Box<dyn std::error::Error>,
>;

fn prep_fsm_from_file(file_name: &str) -> ParseResultFSM {
    ensure_dir_exists("./hypothesis")?;
    log::info!("Read file from {:?}", file_name);
    Ok(read_mealy_from_file(file_name))
}
