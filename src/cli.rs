use clap::Parser;
use lsharp_ru::{
    learner::l_sharp::{Rule2, Rule3},
    oracles::equivalence::{InfixStyle, InfixGenerator, generic::CandidateListSortMethod},
};

use crate::learning_config::EqOracle;

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(long, value_enum, default_value_t)]
    rule2: Rule2,
    #[arg(long, value_enum, default_value_t)]
    rule3: Rule3,
    #[arg(short = 'e', long)]
    eq_oracle: EqOracle,
    #[arg(short = 'm', long)]
    model: String,
    #[arg(short = 'w', long)]
    weights: String,
    #[arg(short = 'd', default_value_t=false)]              // This determines how to generate the prefix. "-d" to use dijkstra, ignore for BFS
    dijkstra: bool,
    #[arg(short = 'c', default_value_t=false)]              // This determines how to generate the characterisation set W. "-c" creates the cheapest possible W, diregarding the other cli setting
    cheap_w: bool,
    //#[arg(short = 'p', default_value_t=false)]
    //experiment: bool,
    #[arg(short = 'x', long, default_value_t = 42)]
    seed: u64,
    #[arg(long)]
    eq_mode: InfixStyle,
    #[arg(short = 'i', long, default_value = "random")]      //  This determines how we generate the random infixes during 'infite' mode. Either "-i weighted" or "-i random"
    infix_gen: InfixGenerator,                                                 
    #[arg(short = 'k', long)]
    extra_states: usize,
    #[arg(short = 'l', long)]
    expected_random_length: usize,
    #[arg(short = 'n', long, default_value_t = 1)]
    piw_listsize: usize,                                        // This determines how many potential counterexamples are generated before being sorted and chosen to test. n<=1 means original (non-weighted) behaviour
    #[arg(short = 's', long, default_value = "cost")]
    sort_style: CandidateListSortMethod,                        // This determines the way the potential counterexamples are sorted when n>1. 
    #[arg(long)]
    traces: Option<String>,
    #[arg(short = 'b', default_value_t = 0)]                    // this determines the cost cap of the pre inputs that can be done before learning. Does not include one extra reset
    pre_input: usize,
    #[arg(short = 'u', default_value_t = 999999)]                    // this determines the max budget for one test learning loop, 0 = infinite
    budget: usize,
    #[arg(short = 'r', default_value_t = 0)]                    // this determines how often potential optimizations are dropped in favor of the original algorithm  0 = 100% new cheap, 100 = 100% original l#
    ratio: usize,
    #[arg(short = 'h', long, default_value = "")]             // this is the filepath to a hypothesis. When supplied, the program should find only a single counterexample and exit.
    hypothesis: String,
    #[arg(short = 'o', long)]
    out: String,
    #[arg(short = 'v')]
    /// Set verbosity level for logs.
    ///
    /// 0 => disabled, 1 => Info, 2 => Debug, 3 => Trace.
    verbosity: usize,
    #[arg(short = 'q', default_value_t)]
    /// Quiet mode (false by default).
    /// Do not print anything to stdio.
    quiet: bool,
    #[arg(long, default_value_t)]
    /// Compress the tree. Currently broken, do not use.
    compress_tree: bool,
}

// const  EO_HELP: &str = "This option sets the equivalence oracle to use. All options with the prefix `soucha` use FSMLIB, while `hads` uses hybrid-ads from Joshua Moerman. We also have a local implementation of H-ADS, called `hads_int`.";

// const ES_HELP: &str =
//     "Number of extra states you assume in the testing process. You do not need to add 1 to it.";

// const EXPECTED_INFIX_HELP : &str =
// "Expected length of the random infix. We will generate random infix sequences using a geometric distribution, where the probability of ending the sequence will be 1 divided by the argument provided.";
