use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};
use crate::learner::obs_tree::ObservationTree;

/// Score is (num of states that generate an output) * (num of states that do not)
/// divided by the total number of states in the original block.
#[must_use]
#[inline]
pub(super) fn _compute_score(u: f64, u_i_o: f64, u_i: f64, child_score: f64) -> f64 {
    (u_i_o * (u_i - u_i_o + child_score)) / u
}

/// Non-normalised score function for redundancy-free ADS computation.
#[must_use]
#[inline]
pub(super) fn compute_reg_score(u_i_o: usize, u_i: usize, child_score: usize) -> usize {
    u_i_o * (u_i - u_i_o) + child_score
}

/// Partition a block of states using an input and  
/// return a map of `OutputSymbol` -> block of states.
#[must_use]
#[inline]
pub(super) fn partition_on_output<Tree>(
    obs_tree: &Tree,
    block: &[Tree::S],
    input: InputSymbol,
) -> FxHashMap<OutputSymbol, Vec<Tree::S>>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol>,
{
    block
        .iter()
        .filter_map(|s| obs_tree.get_out_succ(s.clone(), input))
        .fold(FxHashMap::default(), |mut acc, (o, d)| {
            acc.entry(o).or_insert_with(Vec::new).push(d);
            acc
        })
}
