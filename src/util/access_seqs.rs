use std::collections::VecDeque;

use rand::{rngs::StdRng, seq::SliceRandom, SeedableRng};
use rand_distr::{Distribution, Uniform};
use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, Mealy, State};
use crate::definitions::FiniteStateMachine;

use super::weighted_input::weights::{dijkstra};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InputSelection {
    Normal,
    Randomised(u64),
}

impl InputSelection {
    #[must_use]
    pub fn rand_with_seed(seed: u64) -> Self {
        Self::Randomised(seed)
    }

    #[must_use]
    pub fn normal() -> Self {
        Self::Normal
    }

    fn is_random(&self) -> bool {
        !matches!(self, InputSelection::Normal)
    }
}

impl Default for InputSelection {
    fn default() -> Self {
        Self::Randomised(0)
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum SearchStrategy {
    Bfs,
    Dfs,
    Dijkstra,
    #[default]
    Buggy,
}

impl SearchStrategy {
    fn insert_elem<T>(self, work_list: &mut VecDeque<T>, elem: T) {
        match self {
            SearchStrategy::Dfs => work_list.push_front(elem),
            SearchStrategy::Bfs | SearchStrategy::Buggy => work_list.push_back(elem),
            SearchStrategy::Dijkstra => work_list.push_front(elem)
        }
    }

    fn next_elem<T>(self, work_list: &mut VecDeque<T>, rng: &mut StdRng) -> Option<T> {
        match self {
            SearchStrategy::Dfs | SearchStrategy::Bfs => work_list.pop_front(),
            SearchStrategy::Buggy | SearchStrategy::Dijkstra => {
                if work_list.is_empty() {
                    return None;
                }
                let uni_dist = Uniform::new(0, work_list.len());
                let idx = uni_dist.sample(rng);
                work_list.remove(idx)
            }
        }
    }
}

#[must_use]
pub fn access_sequences(
    fsm: &Mealy,
    input_sel: InputSelection,
    ss: SearchStrategy,
    weights: &FxHashMap<InputSymbol, usize>,
) -> FxHashMap<State, Vec<InputSymbol>> {
    //println!("weight map in acc_seqs: {:?}", weights);

    if matches!(ss, SearchStrategy::Dijkstra) {
        let acc_seqs = dijkstra(fsm, State::new(0), weights);
        //println!("Doing Dijkstra");
        return acc_seqs
    }
    //println!("Not Doing Dijkstra");
    let mut ret = FxHashMap::default();
    let mut inputs = fsm.input_alphabet();
    let mut rng = {
        let x = match input_sel {
            InputSelection::Normal => 0,
            InputSelection::Randomised(x) => x,
        };
        StdRng::seed_from_u64(x)
    };
    let mut work_list = VecDeque::from([State::new(0)]);
    ret.insert(State::new(0), vec![]);

    while let Some(c_state) = ss.next_elem(&mut work_list, &mut rng) {
        // Get list of inputs.
        if input_sel.is_random() {
            inputs.shuffle(&mut rng);
        }
        for i in &inputs {
            let dest = fsm.step_from(c_state, *i).0;
            if ret.contains_key(&dest) {
                continue;
            }
            ss.insert_elem(&mut work_list, dest);
            let dest_acc = {
                let mut x = ret.get(&c_state).expect("Safe").clone();
                x.push(*i);
                x
            };
            ret.insert(dest, dest_acc);
        }
    }

    ret
}



#[cfg(test)]
mod tests {

    use rstest::rstest;
    use rustc_hash::FxHashSet;

    use crate::definitions::FiniteStateMachine;
    use crate::util::parsers::machine::read_mealy_from_file;
    use crate::util::weighted_input::weights::weights_from_inputmap;

    use super::{access_sequences, InputSelection, SearchStrategy};

    #[rstest]
    #[case("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/hypothesis_14.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    /// Check if all the number of access sequences found is equal to
    /// the number of states of an FSM.
    fn all_states_found(#[case] file_name: &str) {
        let (fsm, input_map, _) = read_mealy_from_file(file_name);
        let weights = weights_from_inputmap(input_map);
        let access_states: FxHashSet<_> =
            access_sequences(&fsm, InputSelection::Randomised(1), SearchStrategy::Buggy, &weights)
                .into_keys()
                .collect();
        let fsm_states: FxHashSet<_> = fsm.states().into_iter().collect();
        assert_eq!(fsm_states, access_states);
    }
}
