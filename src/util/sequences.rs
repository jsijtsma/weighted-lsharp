use std::{collections::BTreeMap, iter::Flatten, vec::IntoIter};

use itertools::Itertools;
use rand::{rngs::StdRng, seq::SliceRandom, SeedableRng};
use rand_distr::{Distribution, Geometric, Uniform, WeightedIndex};
use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, State};

use super::toolbox::inputs_iterator;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone)]
pub struct LogExpSequences {
    seed: u64,
    mega_trace: Vec<InputSymbol>,
    beginning_indices: Vec<usize>,
    min_inputs: usize,
    exp_length: usize,
}

impl LogExpSequences {
    #[must_use]
    pub fn from_traces(
        traces: &Vec<Vec<InputSymbol>>,
        seed: u64,
        min_inputs: usize,
        exp_length: usize,
    ) -> Self {
        let mut beginning_indices = Vec::with_capacity(traces.len());
        let mut mega_trace = Vec::new();
        let mut curr_idx = 0;
        for trace in traces {
            beginning_indices.push(curr_idx);
            mega_trace.extend(trace);
            curr_idx = mega_trace.len();
        }
        Self {
            seed,
            mega_trace,
            beginning_indices,
            min_inputs,
            exp_length,
        }
    }
}

impl IntoIterator for LogExpSequences {
    type Item = Vec<InputSymbol>;

    type IntoIter = LogExpSeqIterator;

    fn into_iter(self) -> Self::IntoIter {
        #[allow(clippy::cast_precision_loss)]
        let prob = 1f64 / (self.exp_length as f64);
        let geo_ditr = Geometric::new(prob).expect("Cannot set 0 as expected random length.");
        LogExpSeqIterator {
            rng: SeedableRng::seed_from_u64(self.seed),
            point_selector: Uniform::new(0, self.mega_trace.len()),
            mega_trace: self.mega_trace,
            beginning_indices: self.beginning_indices,
            min_inputs: self.min_inputs,
            geo_ditr,
        }
    }
}

pub struct LogExpSeqIterator {
    rng: StdRng,
    point_selector: Uniform<usize>,
    mega_trace: Vec<InputSymbol>,
    beginning_indices: Vec<usize>,
    min_inputs: usize,
    geo_ditr: Geometric,
}

impl Iterator for LogExpSeqIterator {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        // point in the mega-trace.
        let point = self.point_selector.sample(&mut self.rng);
        // If next_trace_at is none, we have the last sequence in the mega-trace, and we simply get the max elements.
        let next_trace_at = self.beginning_indices.iter().find(|idx| **idx > point); // the next trace begins here.
        let exp_len_sample = self.geo_ditr.sample(&mut self.rng);
        // the number of elements we have selected.
        let selected_length = Ord::max(self.min_inputs, exp_len_sample.try_into().unwrap());

        let selection_range = if let Some(&next_trace_at) = next_trace_at {
            let end = point + selected_length;
            let end = Ord::min(end, next_trace_at);
            point..end
        } else {
            point..(self.mega_trace.len())
        };
        #[allow(clippy::redundant_closure_for_method_calls)]
        let next = self.mega_trace.get(selection_range).map(|x| x.to_vec());
        next
    }
}

/// Iterator which randomly selects access sequences from the given map.
///
/// ## Warning
/// We randomly *select* an access sequence, not *generate* one.
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone)]
pub struct RandomAccessSequences<'a> {
    rng: StdRng,
    acc_map: &'a BTreeMap<State, Vec<InputSymbol>>,
    states: Vec<State>,
}

impl<'a> RandomAccessSequences<'a> {
    #[must_use]
    pub fn new(map: &'a BTreeMap<State, Vec<InputSymbol>>, seed: u64) -> Self {
        #[allow(clippy::cast_possible_truncation)]
        let states = map.keys().copied().collect();
        Self {
            rng: SeedableRng::seed_from_u64(seed),
            acc_map: map,
            states,
        }
    }
}

impl<'a> Iterator for RandomAccessSequences<'a> {
    type Item = &'a Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        let s = self.states.choose(&mut self.rng)?;
        self.acc_map.get(s)
    }
}

/// Generates random vectors of symbols.
pub struct RandomInfixGenerator {
    rng: StdRng,
    min_inputs: usize,
    num_inputs: usize,
    expected_inputs: usize,
}

impl RandomInfixGenerator {
    /// Construct a generator with `seed`, `num_inputs` number of inputs,
    /// `min_inputs` minimum length, and `expected_inputs` being the expected
    /// length of the random portion.
    #[must_use]
    pub fn new(seed: u64, num_inputs: usize, min_inputs: usize, expected_inputs: usize) -> Self {
        Self {
            min_inputs,
            num_inputs,
            expected_inputs,
            rng: SeedableRng::seed_from_u64(seed),
        }
    }
}

impl IntoIterator for RandomInfixGenerator {
    type Item = Vec<InputSymbol>;

    type IntoIter = RandomInfixIterator;

    fn into_iter(self) -> Self::IntoIter {
        let uni = Uniform::new(0, self.expected_inputs);
        #[allow(clippy::cast_possible_truncation)]
        let num_inputs = self.num_inputs as u16;
        RandomInfixIterator {
            rng: self.rng,
            uni,
            k_min: self.min_inputs,
            inputs_dist: Uniform::new(0u16, num_inputs),
        }
    }
}

#[derive(Clone)]
pub struct RandomInfixIterator {
    rng: StdRng,
    uni: Uniform<usize>,
    k_min: usize,
    inputs_dist: Uniform<u16>,
}

impl Iterator for RandomInfixIterator {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut lookahead = 0;
        for i in self.uni.sample_iter(&mut self.rng) {
            if i == 0 && lookahead > self.k_min {
                break;
            }
            lookahead += 1;
        }
        let input_seq: Vec<_> = self
            .inputs_dist
            .map(InputSymbol::from)
            .sample_iter(&mut self.rng)
            .take(lookahead)
            .collect();
        Some(input_seq)
    }
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Order {
    ASC,
    DESC,
}

/// Generate fixed infixes. We do not need to add 1.
pub struct FixedInfixGenerator<T> {
    input_alphabet: Vec<T>,
    range: Vec<usize>,
}

impl<T: Clone> FixedInfixGenerator<T> {
    #[must_use]
    /// Generate fixed infixes for the given input alphabet up until `length + 1`.
    pub fn new(input_alphabet: Vec<T>, length: usize, order: Order) -> Self {
        let mut x = (0..=length + 1).into_iter().collect_vec();
        Self {
            input_alphabet,
            range: if order == Order::ASC {
                x
            } else {
                x.reverse();
                x
            },
        }
    }

    #[must_use]
    pub fn generate(self) -> FixedInfixIterator {
        FixedInfixIterator::new(self.range, self.input_alphabet.len())
    }
}

/// Implementation copied from [here](https://stackoverflow.com/a/71420578).
/// All rights as written in the link.
///
#[derive(Debug, Clone)]
pub struct PermutationsReplacementIter<I> {
    items: Vec<I>,
    permutation: Vec<usize>,
    group_len: usize,
    finished: bool,
}

impl<I: Copy> PermutationsReplacementIter<I> {
    fn increment_permutation(&mut self) -> bool {
        let mut idx = 0;

        loop {
            if idx >= self.permutation.len() {
                return true;
            }

            self.permutation[idx] += 1;

            if self.permutation[idx] >= self.items.len() {
                self.permutation[idx] = 0;
                idx += 1;
            } else {
                return false;
            }
        }
    }

    fn build_vec(&self) -> Vec<I> {
        let mut vec = Vec::with_capacity(self.group_len);

        for idx in &self.permutation {
            vec.push(self.items[*idx]);
        }

        vec
    }
}

impl<I: Copy> Iterator for PermutationsReplacementIter<I> {
    type Item = Vec<I>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None;
        }

        let item = self.build_vec();

        if self.increment_permutation() {
            self.finished = true;
        }

        Some(item)
    }
}

pub trait ToPermutationsWithReplacement {
    type Iter;
    fn permutations_with_replacement(self, group_len: usize) -> Self::Iter;
}

impl<I: Iterator> ToPermutationsWithReplacement for I {
    type Iter = PermutationsReplacementIter<<I as Iterator>::Item>;

    fn permutations_with_replacement(self, group_len: usize) -> Self::Iter {
        let items = self.collect::<Vec<_>>();
        PermutationsReplacementIter {
            permutation: vec![0; group_len],
            group_len,
            finished: group_len == 0 || items.is_empty(),
            items,
        }
    }
}

#[derive(Clone, Debug)]
pub struct FixedInfixIterator {
    fl_inner: Flatten<IntoIter<PermutationsReplacementIter<InputSymbol>>>,
}

impl FixedInfixIterator {
    #[must_use]
    pub fn new(range: Vec<usize>, input_size: usize) -> Self {
        let vec_inner = range
            .into_iter()
            .map(|r| inputs_iterator(input_size).permutations_with_replacement(r))
            .collect_vec();
        let fl_inner = vec_inner.into_iter().flatten();
        Self { fl_inner }
    }
}

impl Iterator for FixedInfixIterator {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        self.fl_inner.next()
    }
}


//Generates random vectors of symbols with likelihood based on cost.
pub struct WeightedInfixGenerator {
    rng: StdRng,
    min_inputs: usize, 
    num_inputs: usize,
    expected_inputs: usize,
    weight_map: FxHashMap<InputSymbol, usize>,
    inverted: bool,
}

impl WeightedInfixGenerator {
    #[must_use]
    pub fn new(seed: u64, weight_map: FxHashMap<InputSymbol, usize>, num_inputs: usize, min_inputs:usize, expected_inputs:usize, inverted:bool) -> Self {
        Self {
            min_inputs,
            num_inputs,
            expected_inputs,
            rng: SeedableRng::seed_from_u64(seed),
            weight_map,
            inverted,

        }
    }
}

impl IntoIterator for WeightedInfixGenerator {
    type Item = Vec<InputSymbol>;

    type IntoIter = WeightedInfixIterator;

    fn into_iter(self) -> Self::IntoIter {
        let uni = Uniform::new(0, self.expected_inputs);
        let all_weights: Vec<usize> = self.weight_map.iter().map(|a| *a.1).collect();       // extract all weights from the weightmap
        let max_weight: usize = *all_weights.iter().max().unwrap();                         // pick the highest weight
        
        // if inverted is true, then all_weights is used as is, making high weight InputSymbols more likely to be chosen
        // if false, then the values from all_weights are inverted by subtracting that weight from the max weight (+1 to prevent a weight from 0 to exist)
        // "inverted" functions this way because outside of this file, you would not expect 'inverted' to mean the opposite
        WeightedInfixIterator {
            rng: self.rng,
            uni,
            k_min: self.min_inputs,
            weights: if self.inverted {all_weights} else {all_weights.iter().map(|a| max_weight+1-a).collect()},
            symbols: self.weight_map.iter().map(|a| *a.0).collect(),

        }
    }
}

#[derive(Clone)]
pub struct WeightedInfixIterator {
    rng: StdRng,
    uni: Uniform<usize>,
    k_min: usize,
    weights: Vec<usize>,
    symbols: Vec<InputSymbol>,
}

impl Iterator for WeightedInfixIterator {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        // randomly choose the length of the infix, taken from the RandomInfixGenerator
        let mut lookahead = 0;
        for i in self.uni.sample_iter(&mut self.rng) {
            if i == 0 && lookahead > self.k_min {
                break;
            }
            lookahead += 1;
        }

        // the weighted distribution according to the weights
        let distr = WeightedIndex::new(self.weights.clone()).unwrap();
        
        // take the random amount of inputsymbols from the weighted distribution
        // technically takes their indexes, and then maps them to the symbols.
        let input_seq: Vec<InputSymbol> = distr
            .sample_iter(&mut self.rng)
            .take(lookahead)
            .map(|a| self.symbols[a])
            .collect();
        Some(input_seq)
    }
}