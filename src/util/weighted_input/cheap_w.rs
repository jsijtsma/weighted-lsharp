use std::collections::{HashMap, BTreeMap, BTreeSet};

use petgraph::adj::NodeIndex;
use petgraph::graph::Graph;
use petgraph::algo::astar;
use petgraph::Directed;
use rustc_hash::FxHashMap;
use crate::definitions::FiniteStateMachine;
use crate::definitions::mealy::{InputSymbol, Mealy, State};

pub struct Temp {
    state_to_node: HashMap<DoubleState, NodeIndex>,
    graph: Graph<DoubleState, usize, Directed>,
    goal_node: NodeIndex,
    rem_list: Vec<DoubleState>,
}

#[derive(Hash, Eq, PartialEq, Debug, Copy, Clone)]
pub struct DoubleState {
    s1: State,
    s2: State,
    a: InputSymbol,
}

impl DoubleState {
    fn new(s1: State, s2: State, a:InputSymbol) -> DoubleState {
        DoubleState{s1, s2, a}
    }
}

impl Temp {
    pub fn double_graph(
        fsm: &Mealy,
        weight_map: &FxHashMap<InputSymbol, usize>,
    ) -> Self {
        let mut g: Graph<DoubleState, usize, Directed> = Graph::new();
        let mut state_lookup: HashMap<DoubleState, u32> = HashMap::new();
        let mut to_be_removed: Vec<DoubleState> = Vec::new();

        for i in fsm.states() {
            for j in fsm.states() {
                for a in fsm.input_alphabet() {
                    let dn = DoubleState::new(i, j, a);
                    let node = g.add_node(dn).index().try_into().unwrap();
                    state_lookup.insert(dn, node);
                
                }
            }
        }

        let firststate = fsm.states()[0];
        //for a in fsm.input_alphabet() {
        //    let dn = DoubleState::new(firststate, firststate, a);
        //    let node = g.add_node(dn).index().try_into().unwrap();  // add intermediary goal nodes, that save which action was needed to get to the goal.
        //    state_lookup.insert(dn, node);
            //if a == fsm.input_alphabet()[0] {
            //    endnode = node;
            //}
        //}
        let endnode: NodeIndex = state_lookup[&DoubleState::new(firststate, firststate, fsm.input_alphabet()[0])].into();
        //: HashMap<(State, State), NodeIndex> 

        for i in fsm.states() {
            for j in fsm.states() {
                if i == j {
                    continue;
                }
                for a in fsm.input_alphabet() {
                    let (target1, output1) = fsm.step_from(i, a);
                    let (target2, output2) = fsm.step_from(j, a);
                    let cost = weight_map[&a];

                    for b in fsm.input_alphabet() {
                        let u = DoubleState::new(i, j, b);
                        if output1 != output2 {
                            let end = DoubleState::new(firststate, firststate, a);
                            g.add_edge(state_lookup[&u].into(),state_lookup[&end].into(),cost);
                        }
                        else if target1 != target2 {
                            let t = DoubleState::new(target1, target2, a);
                            //println!("{:?} goes to {:?}", u, t);
                            g.add_edge(state_lookup[&u].into(),state_lookup[&t].into(),cost);
                        }
                    }
                }
            }
        }

        for a in fsm.input_alphabet() {
            if a != fsm.input_alphabet()[0] {
                let almost_endnode = DoubleState::new(firststate, firststate, a);
                g.add_edge(state_lookup[&almost_endnode].into(),endnode.into(),0);
                to_be_removed.push(almost_endnode);
            }    
        }    
        Self { state_to_node: state_lookup, graph: g , goal_node: endnode, rem_list: to_be_removed}
    }

    pub fn dist_seq_between(&mut self, 
        fsm: &Mealy,
        s: State, 
        t: State, 
        
        ) ->  Vec<InputSymbol> {
        let start = DoubleState::new(s, t, fsm.input_alphabet()[0]);
        let paths = astar(&self.graph, self.state_to_node[&start].into(), |n| n==self.goal_node.into(), |x| -> usize { *x.weight()}, |_| 0).unwrap();
        let path = paths.1.iter().map(|x| self.graph.node_weight(*x).unwrap());
        let mut result: Vec<_> = path.collect();
        

        for e in self.rem_list.iter() {
            let p = result.iter().position(|x| *x == e);
            if p!= None {
                let p_goal = result.iter().position(|x| self.goal_node == self.state_to_node[&x]);
                result.remove(p_goal.unwrap());} //remove the extra goal node if it wasnt needed to find the solution
            }

        result.remove(0); //remove the first node with arbitrary input

        result.iter().map(|x| x.a).collect()
        }



    pub fn all_shortest_paths(&mut self, fsm:&Mealy) -> BTreeMap<State,BTreeSet<Vec<InputSymbol>>> {
        //let mut paths: Vec<Vec<InputSymbol>> = Vec::new();
        let mut dist_seq: BTreeMap<State, BTreeSet<Vec<InputSymbol>>> = BTreeMap::new();
        for s in fsm.states() {
            let mut seq: BTreeSet<Vec<InputSymbol>> = BTreeSet::new();
            for t in fsm.states() {
                if s != t {
                    let res = self.dist_seq_between(fsm, s, t);
                    //paths.push(res);
                    seq.insert(res);
                }
            }
            dist_seq.insert(s, seq);
        }
        dist_seq
    }
}

pub fn all_cheapest_paths(fsm: &Mealy, cost_map: &FxHashMap<InputSymbol, usize>,) -> BTreeMap<State,BTreeSet<Vec<InputSymbol>>> {
    let mut temp = Temp::double_graph(fsm, cost_map);
    temp.all_shortest_paths(fsm)
}

pub fn without_prefixes(original_list:Vec<Vec<InputSymbol>>) -> Vec<Vec<InputSymbol>>{
    let mut new_list = Vec::new();
    for fix in &original_list {
		let mut is_prefix = false;
		for other in &original_list {
			if other.len() > fix.len() {
				for i in 0..fix.len() {
					if fix[i]== other[i] {
                        if i == fix.len()-1 {
                                is_prefix = true;
                                break;
                        }
                        continue;
                    } else {
                        break;
					}
				}
			}
		}
		if !is_prefix {new_list.push(fix.clone());}
	}
	new_list
}


#[cfg(test)]
mod tests {

	use rstest::rstest;

    use crate::definitions::FiniteStateMachine;
	use crate::definitions::mealy::{InputSymbol, Mealy, State};
	use crate::util::parsers::machine::{read_mealy_from_file, read_mealy_from_file_using_maps};
	use crate::util::weighted_input::weights::weights_from_file;

	use super::{all_cheapest_paths, Temp};
    #[rstest]
    #[case("tests/src_models/TestRing.dot")]
    fn w_test(#[case] file_name: &str) {
        let (_, input_map, output_map) = read_mealy_from_file(file_name);
		let fsm = read_mealy_from_file_using_maps(file_name, &input_map, &output_map).expect("Safe");
        let weights = weights_from_file(&input_map, &"weights/custom/TestRingWeights.txt".to_string());
        //let access_seqs = dijkstra(&fsm, State::new(0), &weights);
		//print_seq(access_seqs.clone(), &weights);
        let mut temp = Temp::double_graph(&fsm, &weights);
        let states = fsm.states();

        let acp = <crate::oracles::equivalence::incomplete::hads::HADSMethod as crate::definitions::characterisation::Characterisation>::characterisation_map(&fsm);//(&fsm, &weights);
        println!("{:?}",acp);
        let mut best_path = Vec::from([InputSymbol::new(0)]);
        assert_eq!(temp.dist_seq_between(&fsm, states[0], states[1]), best_path);

        //best_path = Vec::from([InputSymbol::new(0), InputSymbol::new(0)]);
        //assert_eq!(temp.dist_seq_between(&fsm, states[0], states[5]), best_path);
        
        //best_path = Vec::from([InputSymbol::new(0), InputSymbol::new(0), InputSymbol::new(0), InputSymbol::new(0)]);
        //assert_eq!(temp.dist_seq_between(&fsm, states[2], states[5]), best_path);
        
        //assert_eq!(c, 0);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(1)], &weights), 1);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(2)], &weights), 2);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(3)], &weights), 3);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(4)], &weights), 4);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(5)], &weights), 5);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(6)], &weights), 4);
		//assert_eq!(sequence_cost(&access_seqs[&State::new(7)], &weights), 6);
	}
    
}