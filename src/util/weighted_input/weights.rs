use std::cell::RefCell;
use std::fs::File;
use std::io::{ self, BufRead};
use std::collections::HashMap;
use std::iter::zip;
use std::rc::Rc;
use rand::rngs::StdRng;
use rand_distr::{WeightedIndex, Distribution};
use rustc_hash::FxHashMap;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

use crate::definitions::FiniteStateMachine;
use crate::definitions::mealy::{InputSymbol, Mealy, State, OutputSymbol};

use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle;
//use crate::sul::{SystemUnderLearning, SinkWrapper, Simulator};
use crate::util::parsers::machine::{read_mealy_from_file, read_mealy_from_file_using_maps};



#[derive(Copy, Clone, Eq, PartialEq)]
struct StateDistance {
state: State,
distance: usize,
}

impl Ord for StateDistance {
    fn cmp(&self, other: &Self) -> Ordering {
        //other.distance.cmp(&self.distance)
		self.distance.cmp(&other.distance)
    }
}

impl PartialOrd for StateDistance {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

	// public function for pre_input. First creates a list of input sequences, then inputs these sequences 
	// and finds their corresponding output sequences. Returns them combined.
	pub fn pre_input<T>(
		oracle: Rc<RefCell<Oracle<T>>>,
		inputs: Vec<InputSymbol>,
		weights: &FxHashMap<InputSymbol, usize>,
		rng: StdRng,
		budget: usize,
	) -> Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> 
	where T: ObservationTree<InputSymbol, OutputSymbol>, T: Send, T: Sync, {
		let input_seq_list: Vec<Vec<InputSymbol>> = pre_input_seq_list(inputs, weights, rng, budget);
		let output_seq_list: Vec<Vec<OutputSymbol>> = input_pre_inputs(oracle, &input_seq_list);
		combine_input_output(input_seq_list, output_seq_list)
	}

	// create a list of input sequences for the pre_input
	fn pre_input_seq_list(
		_inputs: Vec<InputSymbol>,
		weight_map: &FxHashMap<InputSymbol, usize>,
		mut rng: StdRng,
		budget: usize,
	) -> Vec<Vec<InputSymbol>>{

		let pre_weights: Vec<usize> = weight_map.iter().map(|a| *a.1).collect();
		let max_weight: usize = *pre_weights.iter().max().unwrap();

		// similar to the weighted random I, we inverse the weights so we can choose the cheap ones more often.
		let symbols: Vec<InputSymbol> 	= weight_map.iter().map(|a| *a.0).collect();
		let weights: Vec<usize> 		= pre_weights.iter().map(|a| max_weight+1-a).collect();

		let distr = WeightedIndex::new(weights.clone()).unwrap();
		let mut cost_so_far: usize = 0;

		let mut input_seqs: Vec<Vec<InputSymbol>> = Vec::new();
		let mut input_seq: Vec<InputSymbol> = Vec::new();
		loop{
			if cost_so_far>budget {break;}		// if the budget is exceeded, stop the process
			loop{								// this loops picks inputsymbols according to the distribution until the RESET 'input' is picked, then it
				let i = distr.sample(&mut rng);

				if cost_so_far>budget {break;}  // if the last input would have exceeded the budget, then stop adding this new input symbol

				let input_symbol: InputSymbol = symbols[i];
				cost_so_far += weight_map[&input_symbol];
				if input_symbol == InputSymbol::new(u16::MAX) {
					break;
				}
				input_seq.push(input_symbol)
			}
			input_seqs.push(input_seq);			// adds the created input sequence to the list and clears it
			input_seq = Vec::new();
		}
		//println!("pre input sequences: {:?}.", input_seqs);
		input_seqs
	}

	// get corresponding output sequences for the input sequences
	fn input_pre_inputs<T>(
		oracle: Rc<RefCell<Oracle<T>>>,
		input_seq_list: &Vec<Vec<InputSymbol>>, 
	) -> Vec<Vec<OutputSymbol>>  
	where T: ObservationTree<InputSymbol, OutputSymbol>, T: Send, T: Sync, {
		let mut outputs: Vec<Vec<OutputSymbol>> = Vec::new();
		for inputs in input_seq_list {
			//let mut output_seq: Vec<OutputSymbol> = Vec::new();
			//println!("pre-inputting: {:?}", inputs);
			let o = RefCell::borrow_mut(&oracle).output_query(inputs);

			outputs.push(o);

		}
		outputs
	}

	fn combine_input_output(
		input_seq_list:Vec<Vec<InputSymbol>>, 
		output_seq_list:Vec<Vec<OutputSymbol>>
	) -> Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>{
		let m = zip(input_seq_list, output_seq_list);
		m.collect()
	}

	// return the weight of a single input from the file "weight.txt" 
	pub fn read_weight(target: String) -> usize {
		let filenamae = "weights.txt".to_string();
		let file = File::open(filenamae).unwrap();
		let lines = io::BufReader::new(file).lines();
		for line in lines {
			let (v, k) = read_pair(line.unwrap());
			if k == target {
				return v;
			}
		}
		1		// if not found, just return 1
	}

	// return the weight of a single input from the any file
	pub fn read_weight_from_file(target: String, filenamae:&String) -> usize {
		let file = File::open(filenamae).unwrap();
		let lines = io::BufReader::new(file).lines();
		for line in lines {
			let (v, k) = read_pair(line.unwrap());
			if k == target {
				return v;
			}
		}
		1		// if not found, just return 1
	}

	// return the cost of one input sequence, based on the weights in weights.txt. Mostly kep for debugging
	pub fn sequence_weight(seq: Vec<InputSymbol>) -> usize {
		let mut count:usize = 0;
		for i in seq {
			count += read_weight(i.to_string());
		}
		count
	}

	// return the cost of an input sequence based on a FxHashMap costmap
	pub fn sequence_cost(seq: &Vec<InputSymbol>, costs:&FxHashMap<InputSymbol, usize>) -> usize {
		let mut count:usize = 0;
		for i in seq {
			count += costs[&i];
		}
		count
	}

	// return the cost of an input sequence based on a HashMap costmap
	pub fn sequence_cost_hashmap(seq: &Vec<InputSymbol>, costs:&HashMap<InputSymbol, usize>) -> usize {
		let mut count:usize = 0;
		for i in seq {
			count += costs[&i];
		}
		count
	}

	// from a single line, isolate the value and the string of the inputsymbol
	fn read_pair(line: String) -> (usize, String) {
		let mut bits = line.split(":");
		let key = bits.next().unwrap();
		let number = bits.next();
		let value = number.unwrap().parse::<usize>().unwrap();
		
		(value, key.to_string())
	}

	fn split_pair(line: &String) -> (usize, String) {
		let mut bits = line.split(":");
		let key = bits.next().unwrap();
		let number = bits.next();
		let value = number.unwrap().parse::<usize>().unwrap();
		
		(value, key.to_string())
	}

	// print a sequence in a human readable format from the 
	fn print_seq(m: FxHashMap<State, Vec<InputSymbol>>, costs:&FxHashMap<InputSymbol, usize>) {
		let n = m.len();
		println!("Accsize: {}", n);
		for (k, v) in m {
			let c = sequence_cost(&v, costs);
			let mut acc_seq: String = ":".to_string();
			for s in v {
				let h:String = s.to_string();
				acc_seq.push_str(&(h + ":"));
			}
			println!("State: {}, Access: {}, Cost: {}", k, acc_seq, c);
		}
	}
	

	// find shortest path to the source state. Please forgive any ugly code,  this was the first real function I implemented in rust.
	pub fn dijkstra(fsm: &Mealy, source: State, costs:&FxHashMap<InputSymbol, usize>)  -> FxHashMap<State, Vec<InputSymbol>> {
		let inputs = fsm.input_alphabet();
		let mut distances: FxHashMap<State, usize> = fsm.states().clone().into_iter().map(|a| (a, usize::MAX)).collect();
		*distances.entry(source).or_insert(0) = 0;
		let mut paths = FxHashMap::default();
		paths.insert(source, vec![]);
	
		let mut queue = BinaryHeap::new();
		queue.push(StateDistance {state:source, distance:0});
	
		while let Some(StateDistance {state, distance}) = queue.pop() {

			if distance > *distances.get(&state).unwrap() {continue;}

			for i in &inputs {
				let dest = fsm.step_from(state, *i).0;
				let dest_dist = distance + costs[i];
				let old_dist = distances[&dest];

				if dest_dist < old_dist {
					//new shortest path found
					//adding to queue
					queue.push(StateDistance {state:dest, distance:dest_dist});
					//extracting new shortest path
					let dest_acc = {
						let mut x = paths.get(&state).expect("Safe").clone();
						x.push(*i);
						x
					};
					//update paths hashmap
					*paths.entry(dest).or_insert(vec![]) = dest_acc;
					//update distances hashmap
					*distances.entry(dest).or_insert(0) = dest_dist;


				}
				
			}
		}
		paths
	}


	// looks for the name of the inputsymbol in the lines and returns its given weight
	fn find_weight(target:&String, lines:&Vec<String>) -> usize {
		for line in lines {
			let (v, k) = split_pair(&line);
			if k == target.to_string() {
				return v;
			}
		}
		1
	}

	// read all weights from a file and return a map from all inputsymbols to their weights
	pub fn weights_from_file(input_map:&HashMap<String, InputSymbol>, weights_file:&String) -> FxHashMap<InputSymbol, usize>{
		let file = File::open(weights_file).unwrap();
		let reader = io::BufReader::new(file);
		let lines:Vec<String> = reader.lines().collect::<Result<_,_>>().unwrap();

		let cost_map:FxHashMap<InputSymbol, usize> = input_map.into_iter().map(|(k, v)| ( *v , find_weight(k, &lines))).collect();
		cost_map
	}

	// read all weights from an inputmap and return a map from all inputsymbols to their weights
	pub fn weights_from_inputmap(input_map:HashMap<String, InputSymbol>) -> FxHashMap<InputSymbol, usize> {
		let cost_map:FxHashMap<InputSymbol, usize> = input_map.into_iter().map(|(k, v)| ( v , read_weight(k))).collect();
		cost_map
	}


	pub fn test_dijkstra() {
		let (_sul, input_map, output_map) = read_mealy_from_file("weightedmodel2.dot");
		println!("{:?}", input_map);
        let fsm = read_mealy_from_file_using_maps("weightedmodel2.dot", &input_map, &output_map).expect("Safe");
		let cost_map:FxHashMap<InputSymbol, usize> = input_map.into_iter().map(|(k, v)| ( v , read_weight(k))).collect();
		let gg = dijkstra(&fsm, State::new(0), &cost_map);
		print_seq(gg, &cost_map);
	}



#[cfg(test)]
mod tests {

	use rstest::rstest;

	//use rustc_hash::FxHashMap;

	use crate::definitions::mealy::{State};
	use crate::util::parsers::machine::{read_mealy_from_file, read_mealy_from_file_using_maps};
	use crate::util::weighted_input::weights::weights_from_file;

	use super::{dijkstra, print_seq, sequence_cost};
    #[rstest]
    #[case("tests/src_models/weightedmodel3.dot")]
    fn all_distances_correct(#[case] file_name: &str) {
        let (_, input_map, output_map) = read_mealy_from_file(file_name);
		let fsm = read_mealy_from_file_using_maps(file_name, &input_map, &output_map).expect("Safe");
        let weights = weights_from_file(&input_map, &"weights/custom/weights.txt".to_string());
        let access_seqs = dijkstra(&fsm, State::new(0), &weights);
		print_seq(access_seqs.clone(), &weights);

		let c = sequence_cost(&access_seqs[&State::new(0)], &weights);


        assert_eq!(c, 0);
		assert_eq!(sequence_cost(&access_seqs[&State::new(1)], &weights), 1);
		assert_eq!(sequence_cost(&access_seqs[&State::new(2)], &weights), 2);
		assert_eq!(sequence_cost(&access_seqs[&State::new(3)], &weights), 3);
		assert_eq!(sequence_cost(&access_seqs[&State::new(4)], &weights), 4);
		assert_eq!(sequence_cost(&access_seqs[&State::new(5)], &weights), 5);
		assert_eq!(sequence_cost(&access_seqs[&State::new(6)], &weights), 4);
		assert_eq!(sequence_cost(&access_seqs[&State::new(7)], &weights), 6);
	}
    
}