//! This module contains all the oracles we use during learning.
//!
//! Specifically, we have two types of oracles:
//! 1. Output Oracles, and
//! 2. Equivalence oracles.

/// Collection of equivalence oracles.
pub mod equivalence;
/// Output oracles module.
pub mod membership;
