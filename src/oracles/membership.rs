use datasize::DataSize;
use itertools::Itertools;
use rand::{prelude::SliceRandom, rngs::StdRng, SeedableRng};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

use crate::ads::tree::heu_threaded::Ads as OtADS;
use crate::ads::{AdaptiveDistinguishingSequence, AdsStatus};
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::definitions::FiniteStateMachine;
use crate::learner::apartness::{acc_states_are_apart, compute_witness, states_are_apart};
use crate::learner::l_sharp::{Rule2, Rule3};
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::equivalence::CounterExample;
use crate::sul::SystemUnderLearning;
use crate::util::toolbox;

#[derive(DataSize)]
pub struct Oracle<'a, T> {
    #[data_size(skip)]
    sul: &'a mut dyn SystemUnderLearning,
    obs_tree: T,
    #[data_size(skip)]
    rule2: Rule2,
    #[data_size(skip)]
    rule3: Rule3,
    #[data_size(skip)]
    /// Sink state, if assigned.
    sink_state: Option<Vec<InputSymbol>>,
    #[data_size(skip)]
    /// Sink output.
    pub sink_output: OutputSymbol,
    #[data_size(skip)]
    /// Rng
    rng: StdRng,
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(
        sul: &'a mut dyn SystemUnderLearning,
        rule2: Rule2,
        rule3: Rule3,
        sink_output: OutputSymbol,
        seed: u64,
        obs_tree: T,
    ) -> Self {
        Self {
            obs_tree,
            // obs_tree: T::new(sul.input_map().len()),
            sul,
            rule2,
            rule3,
            sink_state: None,
            sink_output,
            rng: SeedableRng::seed_from_u64(seed),
        }
    }

    pub fn get_counts(&mut self) -> (usize, usize) {
        self.sul.get_counts()
    }

    pub fn get_cost(&mut self) -> usize {
        self.sul.get_cost()
    }

    /// Immutably borrow the underlying observation tree.
    pub fn borrow_tree(&self) -> &T {
        &self.obs_tree
    }

    /// Mutably borrow the underlying observation tree.
    pub fn borrow_mut_tree(&mut self) -> &mut T {
        &mut self.obs_tree
    }

    /// Insert extra symbols in the observation tree for sink states.
    /// We won't then ever need to do queries where the sink-state is the source.
    fn make_sink(&mut self, s: &T::S) {
        for i in toolbox::inputs_iterator(self.obs_tree.input_size()) {
            self.obs_tree
                .insert_observation(Some(s.clone()), &[i], &[self.sink_output]);
        }
    }
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    /// Identifies state `fs` amongst the basis candidates to just one basis candidate.
    /// # Panics
    /// If norm did not increase.
    pub fn identify_frontier(
        &mut self,
        fs_acc: &[InputSymbol],
        candidates: &mut Vec<Vec<InputSymbol>>,
    ) {
        {
            let fs = self
                .obs_tree
                .get_succ(T::S::default(), fs_acc)
                .expect("Safe");
            candidates.retain(|b| {
                let bs = self.obs_tree.get_succ(T::S::default(), b).expect("Safe");
                !states_are_apart(&self.obs_tree, fs.clone(), bs)
            });
        }
        let org_cand_len = candidates.len();
        // candidates.retain(|&b| !states_are_apart(&self.obs_tree, fs, b));
        if org_cand_len < 2 {
            return;
        }
        let mut prefix = fs_acc.to_vec();
        // let mut candidates = org_candidates;
        let (input_seq, output_seq) = match self.rule3 {
            Rule3::Ads => {
                if let [q1_acc, q2_acc] = &candidates[..] {
                    let q1 = self
                        .obs_tree
                        .get_succ(T::S::default(), q1_acc)
                        .expect("Safe");
                    let q2 = self
                        .obs_tree
                        .get_succ(T::S::default(), q2_acc)
                        .expect("Safe");

                    let wit = compute_witness(&self.obs_tree, q1, q2).expect("Safe");
                    let mut input_seq = prefix.clone();

                    let fs_apart_from =
                        |q_acc| acc_states_are_apart(&self.obs_tree, &prefix, q_acc);
                    assert!(
                        !(fs_apart_from(q1_acc) || fs_apart_from(q2_acc)),
                        "Query will not increase norm!"
                    );
                    input_seq.extend(wit);
                    let output_seq = self.output_query(&input_seq);

                    (input_seq, output_seq)
                } else {
                    let candss = candidates
                        .iter()
                        .map(|acc| self.obs_tree.get_succ(T::S::default(), acc).expect("Safe"))
                        .collect_vec();
                    let suffix = &mut OtADS::new(&self.obs_tree, &candss, Some(self.sink_output));
                    self.adaptive_output_query(&mut prefix, None, suffix)
                }
            }
            Rule3::SepSeq => {
                let mut wit = candidates
                    .choose_multiple(&mut self.rng, 2)
                    .map(|acc| self.obs_tree.get_succ(T::S::default(), acc).expect("Safe"))
                    .collect_tuple()
                    .and_then(|(b1, b2)| compute_witness(&self.obs_tree, b1, b2))
                    .expect("Safe");
                let mut input_seq = prefix;
                input_seq.append(&mut wit);
                let output_seq = self.output_query(&input_seq);
                (input_seq, output_seq)
            }
        };
        self.obs_tree
            .insert_observation(None, &input_seq, &output_seq);
        candidates.retain(|b| !acc_states_are_apart(&self.obs_tree, fs_acc, b));
        assert!(candidates.len() != org_cand_len, "Did not increase norm!");
    }

    /// Explores the frontier for all basis states and returns a vector of the frontier states
    /// paired with their corresponding basis candidates.
    #[must_use]
    pub fn explore_frontier(
        &mut self,
        basis: &[Vec<InputSymbol>],
    ) -> Vec<(Vec<InputSymbol>, Vec<Vec<InputSymbol>>)> {
        let inputs = toolbox::inputs_iterator(self.obs_tree.input_size());

        #[allow(clippy::needless_collect)]
        let to_explore: Vec<_> = basis
            .iter()
            .cartesian_product(inputs)
            .filter(|(a_seq, i)| {
                let bs = self
                    .obs_tree
                    .get_succ(T::S::default(), a_seq)
                    .expect("Safe");
                self.obs_tree.get_succ(bs, &[*i]).is_none()
            })
            .collect();

        #[allow(clippy::needless_collect)]
        let ret: Vec<_> = to_explore
            .into_iter()
            .inspect(|(q, i)| log::debug!("{:?} has {:?} undefined", q, i))
            .map(|(q_acc, i)| self._explore_frontier(q_acc, i, basis))
            .collect();
        ret
    }

    /// Explore the frontier for a single (state, input) pair.
    fn _explore_frontier(
        &mut self,
        acc_q: &[InputSymbol],
        i: InputSymbol,
        basis: &[Vec<InputSymbol>],
    ) -> (Vec<InputSymbol>, Vec<Vec<InputSymbol>>) {
        let mut access_q = acc_q.to_vec();
        let q = self
            .obs_tree
            .get_succ(T::S::default(), acc_q)
            .expect("Safe");
        let bss: Vec<_> = basis
            .iter()
            .map(|b_acc| {
                self.obs_tree
                    .get_succ(T::S::default(), b_acc)
                    .expect("Safe")
            })
            .collect();
        let (input_seq, output_seq) = match self.rule2 {
            Rule2::Ads => {
                log::debug!("Constructing ADS for {:?} at {:?}", access_q, i);
                let suffix = &mut OtADS::new(&self.obs_tree, &bss, Some(self.sink_output));
                self.adaptive_output_query(&mut access_q, Some(i), suffix)
            }
            Rule2::Nothing => {
                let mut prefix = access_q;
                prefix.push(i);
                let o_seq = self.output_query(&prefix);
                (prefix, o_seq)
            }
            Rule2::SepSeq => {
                let mut wit = {
                    if basis.len() < 2 {
                        Vec::new()
                    } else {
                        basis
                            .choose_multiple(&mut self.rng, 2)
                            .map(|b| self.obs_tree.get_succ(T::S::default(), b).expect("Safe"))
                            .collect_tuple()
                            .and_then(|(b1, b2)| compute_witness(&self.obs_tree, b1, b2))
                            .expect("Safe")
                    }
                };
                let mut input_seq = access_q;
                input_seq.push(i);
                input_seq.append(&mut wit);
                let output_seq = self.output_query(&input_seq);
                (input_seq, output_seq)
            }
        };
        self.obs_tree
            .insert_observation(None, &input_seq, &output_seq);
        let fs = self.obs_tree.get_succ(q, &[i]).expect("Safe");
        log::debug!("Added frontier {:?}.", fs);
        let bs_not_sep = basis
            .par_iter()
            .filter(|&b| {
                let bs = self.obs_tree.get_succ(T::S::default(), b).expect("Safe");
                !states_are_apart(&self.obs_tree, fs.clone(), bs)
            })
            .cloned()
            .collect();
        let fs = toolbox::concat_slices(&[acc_q, &[i]]);
        (fs, bs_not_sep)
    }

    /// Make a non-adaptive output query.
    pub fn output_query(&mut self, input_seq: &[InputSymbol]) -> Vec<OutputSymbol> {
        if let Some(x) = self.obs_tree.get_observation(None, input_seq) {
            return x;
        }
        self.sul.reset();
        let out_seq: Vec<_> = input_seq.iter().map(|&i| self.sul.step(&[i])[0]).collect();
        if self.sink_state.is_none() && out_seq.last().copied() == Some(self.sink_output) {
            self.sink_state = Some(input_seq.to_vec());
        }
        self.add_observation(input_seq, &out_seq);
        out_seq
    }

    /// Make an adaptive output query of the form (prefix -> infix -> ADS).
    fn adaptive_output_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        prefix: &mut Vec<InputSymbol>,
        infix: Option<InputSymbol>,
        suffix: &mut ADS,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        if let Some(i) = infix {
            prefix.push(i);
        }
        self._adaptive_output_query(prefix, suffix)
    }

    /// Make an adaptive query, where the infix has been moved into the prefix.
    fn _adaptive_output_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        prefix: &[InputSymbol],
        suffix: &mut ADS,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        let tree_reply = self
            .obs_tree
            .get_succ(T::S::default(), prefix)
            .and_then(|curr_state| self.answer_ads_from_tree(suffix, curr_state));
        suffix.reset_to_root();
        if tree_reply.is_some() {
            unreachable!("ADS is not increasing the norm, we already knew this information.");
        }
        self.sul.reset();
        let prefix_out = self.sul.step(prefix);
        // If the last output was a sink output, then we know that the current state of the
        // SUL is a sink state.
        if prefix_out.last().copied() == Some(self.sink_output) {
            let sink = self.add_observation(prefix, &prefix_out);
            if self.sink_state.is_none() {
                self.sink_state = Some(prefix.to_vec());
            }
            self.make_sink(&sink);
            return (prefix.to_vec(), prefix_out.to_vec());
        }
        let (mut suffix_inputs, suffix_outputs) = self.sul_adaptive_query(suffix);
        let mut input_seq = prefix.to_vec();
        input_seq.append(&mut suffix_inputs);
        let output_seq = toolbox::concat_slices(&[&prefix_out, &suffix_outputs]);
        self.add_observation(&input_seq, &output_seq);
        (input_seq.clone(), output_seq)
    }

    // Assuming the prefix has been sent to the SUL, perform the adaptive query.
    fn sul_adaptive_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        ads: &mut ADS,
    ) -> (Vec<InputSymbol>, Box<[OutputSymbol]>) {
        let mut inputs_sent = Vec::new();
        let mut outputs_received = Vec::new();
        let mut last_output = None;
        while let Ok(next_input) = ads.next_input(last_output) {
            log::debug!("Next input: {:?}", next_input);
            inputs_sent.push(next_input);
            let o = self.sul.step(&[next_input])[0];
            last_output = Some(o);
            outputs_received.push(o);
        }
        log::debug!("Next input undefined.");
        (inputs_sent, outputs_received.into())
    }

    pub fn ads_equiv_test(
        &mut self,
        ads: &mut super::equivalence::incomplete::impl_ads::AdsTree,
        prefix: &[InputSymbol],
        fsm: &Mealy,
    ) -> CounterExample {
        // For sinks.
        {
            let mut curr = T::S::default();
            for i in prefix {
                if let Some((o, succ)) = self.obs_tree.get_out_succ(curr, *i) {
                    // We encountered a sink state, might as well exit.
                    if o == self.sink_output {
                        return None;
                    }
                    curr = succ;
                } else {
                    break;
                }
            }
        }

        if let Some(tree_prefix_out) = self.obs_tree.get_observation(None, prefix) {
            let hyp_out = fsm.trace(prefix).1.to_vec();
            for (idx, (tree_o, hyp_o)) in tree_prefix_out.iter().zip(hyp_out.iter()).enumerate() {
                if tree_o != hyp_o {
                    let prefix_in = prefix[..=idx].to_vec();
                    let hyp_out = hyp_out[..=idx].to_vec();
                    return Some((prefix_in, hyp_out));
                }
            }
            let ts = self
                .obs_tree
                .get_succ(T::S::default(), prefix)
                .expect("Safe");
            ads.next_input(None)
                .expect("ADS should have at least one input.");
            let ads_res = self.answer_ads_from_tree(ads, ts);
            match ads_res {
                Some((ads_inputs, ads_outputs)) => {
                    let mut input_seq = Vec::from(prefix);
                    input_seq.extend(ads_inputs.iter());
                    let mut output_seq = tree_prefix_out;
                    output_seq.extend(ads_outputs.iter());
                    // let hyp_out = fsm.trace(&input_seq).1;
                    unreachable!("Tree had a reply, duplicate query!");
                    // if output_seq != hyp_out.to_vec() {
                    // return Some((input_seq, output_seq));
                    // }
                }
                None => ads.reset_to_root(),
            }
        }

        self.sul.reset();
        let prefix_out = self.sul.step(prefix);
        let (mut hyp_curr, hyp_out) = fsm.trace(prefix);
        for (idx, (tree_o, hyp_o)) in prefix_out.iter().zip(hyp_out.iter()).enumerate() {
            if tree_o != hyp_o {
                let prefix_in = prefix[..=idx].to_vec();
                let prefix_out = prefix_out[..=idx].to_vec();
                return Some((prefix_in, prefix_out));
            }
        }
        let mut inputs_sent = prefix.to_vec();
        let mut outputs_received = prefix_out.to_vec();
        let len_diff = inputs_sent.len() - outputs_received.len();
        for _ in 0..len_diff {
            outputs_received.push(self.sink_output);
        }
        let mut hyp_out;
        let mut prev_output = None;
        loop {
            let next_input = ads.next_input(prev_output);
            match next_input {
                Ok(input) => {
                    log::debug!("Input sent: {:?}", input);
                    inputs_sent.push(input);
                    let o = self.sul.step(&[input])[0];
                    (hyp_curr, hyp_out) = fsm.step_from(hyp_curr, input);
                    log::debug!("Output Received: {:?}", o);
                    prev_output = Some(o);
                    outputs_received.push(o);
                    if o != hyp_out {
                        self.add_observation(&inputs_sent, &outputs_received);
                        return Some((inputs_sent, outputs_received));
                    }
                }
                Err(ads_err) => match ads_err {
                    AdsStatus::Done => {
                        self.add_observation(&inputs_sent, &outputs_received);
                        return None;
                    }
                    AdsStatus::Unexpected => {
                        // This should never occur during EQs:
                        // either the hypothesis and SUL disagree on an ouput,
                        // OR the ADS is `Done`.
                        println!("Prefix: {prefix:?}");
                        unreachable!("IADS root must always contain the hyp state to identify.");
                    }
                },
            }
        }
    }

    #[allow(clippy::type_complexity)]
    fn answer_ads_from_tree(
        &self,
        ads: &mut impl AdaptiveDistinguishingSequence,
        from_state: T::S,
    ) -> Option<(Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let mut prev_output = None;
        let mut inputs_sent = vec![];
        let mut outputs_received = vec![];
        let mut curr_state = from_state;
        while let Ok(next_input) = ads.next_input(prev_output) {
            inputs_sent.push(next_input);
            let (output, dest) = self.obs_tree.get_out_succ(curr_state, next_input)?;
            prev_output = Some(output);
            outputs_received.push(output);
            curr_state = dest;
        }
        ads.reset_to_root();
        Some((inputs_sent, outputs_received))
    }

    pub fn add_observation(
        &mut self,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> T::S {
        self.obs_tree
            .insert_observation(None, input_seq, output_seq)
    }
}

impl<'a, T> Oracle<'a, T> {
    #[allow(clippy::type_complexity)]
    pub fn pass_maps(&self) -> (Vec<(String, InputSymbol)>, Vec<(String, OutputSymbol)>) {
        (self.sul.input_map(), self.sul.output_map())
    }
}
