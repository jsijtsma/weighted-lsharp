//! Equivalence oracles to use during an equivalence query.
//!
//! We currently support the following equivalence oracles:
//! 1. BFS,
//! 2. Hybrid-ADS,
//! 3. Separating Sequence,
//! 4. W-Method, and
//! 5. Wp-Method.

use strum_macros::Display;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};

// Factory for generic EOs with logs.
pub mod logs_generic;
// Factory for using EOs -- see module for more information.
/// Chained Equivalence oracles.
pub mod chained;
pub mod generic;
/// Hybrid-ADS oracle.
pub mod hads;
/// Soucha IADS algorithm.
pub mod incomplete;
/// Soucha EQ methods.
pub mod soucha;
/// W-method for partial mealy machines.
pub mod statechum_w;

// pub use self::incomplete::iads::IadsEO;
// pub use self::sep_seq::SequenceOracle;

/// Counterexample is `Some(...)` if found else `None`.
pub type CounterExample = Option<(Vec<InputSymbol>, Vec<OutputSymbol>)>;

/// Infix style: infinite infixes can only be used with expected random length > 0.
#[derive(Debug, Clone, Copy, Display, clap::ValueEnum)]
pub enum InfixStyle {
    Finite,
    Infinite,
}

/// Defines what generator will be used (in infinite mode). 
#[derive(Debug, Clone, Copy, Display, clap::ValueEnum)]
pub enum InfixGenerator {
    Random,
    Weighted,
    Inverted,
}

/// Trait for equivalence oracles.
///
/// `Tree` is the type for the Observation Tree, i.e., if a different type of
/// data structure is used to construct the observation tree, that will work
/// as well.
/// `'sul` is the lifetime of the System Under Learning reference.
#[allow(clippy::module_name_repetitions)]
pub trait EquivalenceOracle<'sul, Tree> {
    /// Get the number of inputs and resets sent to the SUL, and **reset the counters**.
    #[must_use = "Internal counters are reset after this method is called."]
    fn get_counts(&mut self) -> (usize, usize);

    fn get_cost(&mut self) -> usize;

    /// Given a hypothesis, find a CE if possible.
    #[must_use = "Do not forget to send the CE for processing."]
    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample;

    /// Given a hypothesis, find a CE if possible.
    #[must_use = "Do not forget to send the CE for processing."]
    fn find_counterexample_with_lookahead(
        &mut self,
        hyp: &Mealy,
        lookahead: usize,
    ) -> CounterExample;
}
