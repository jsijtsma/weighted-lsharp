use fnv::{FnvBuildHasher, FnvHashMap};
use itertools::Itertools;

use std::fs::File;
use std::io::Write;
use std::process::{Child, Command, Stdio};
use std::{cell::RefCell, rc::Rc, thread, time::Duration};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::writers::overall as MealyWriter;

use super::{CounterExample, EquivalenceOracle};
/// Partial W statechum equivalence oracle.
///
/// This module is not the implementation of the equivalence oracle, it is
/// just a wrapper written to communicate with the actual oracle. During each EQ,
/// we write the hypothesis to the hybrid-ads program and use the tests generated
/// by the same. The implementation of the oracle is in the hybrid-ads submodule of
/// the repository.
pub struct ExtWOracle<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    location: String,
    input_map: FnvHashMap<InputSymbol, String>,
    output_map: FnvHashMap<OutputSymbol, String>,
    extra_states: usize,
}

impl<'a, T> EquivalenceOracle<'a, T> for ExtWOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        {
            let sleepy = Duration::from_millis(100);
            thread::sleep(sleepy);
        }
        self.find_counterexample_with_lookahead(hypothesis, self.extra_states)
    }
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        {
            let write_config = MealyWriter::WriteConfigBuilder::default()
                .file_name(None)
                .format(MealyWriter::MealyEncoding::Dot)
                .build()
                .unwrap();
            let byte_hyp = MealyWriter::write_machine::<FnvBuildHasher, _>(
                &write_config,
                hypothesis,
                &self.input_map,
                &self.output_map,
            )
            .expect("Writer did not return the byte-encoded mealy machine.");

            let mut spec_file = File::create("/home/bharat/rust/automata-lib/spec.dot")
                .expect("Could not write hyp file for statechum.");
            spec_file
                .write_all(&byte_hyp)
                .expect("Could not write hyp.");
        }

        let es_vec = (0..=lookahead).into_iter().rev().collect_vec();
        let test_suite = es_vec.into_iter().find_map(|es| {
            let child = self
                .hads_process(es)
                .expect("Statechum process did not initialise.");
            let proc_out = child
                .wait_with_output()
                .expect("Error when running statechum")
                .stdout;
            let strx = std::str::from_utf8(&proc_out).expect("Safe");
            let ts = self.parse_tests(strx);
            if ts.is_empty() {
                None
            } else {
                log::info!("Generated test suite with statechum for k = {es}");
                Some(ts)
            }
        });
        let test_suite = test_suite.expect("Could not compute test suite with statechum.");

        for input_vec in test_suite {
            let hyp_output = hypothesis.trace(&input_vec).1.to_vec();
            let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(&input_vec);
            if sut_output.len() < hyp_output.len() {
                let hyp_out_lim = &sut_output[..sut_output.len()];
                if hyp_out_lim != sut_output {
                    let ce_input = input_vec[..sut_output.len()].to_vec();
                    let ce_output = sut_output;
                    let ret = Some((ce_input, ce_output));
                    return ret;
                }
            } else if hyp_output != sut_output {
                let ce_input = input_vec;
                let ce_output = sut_output;
                let ret = Some((ce_input, ce_output));
                return ret;
            }
        }
        None
    }
}

impl<'a, T> ExtWOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        location: String,
        extra_states: usize,
        input_map: FnvHashMap<InputSymbol, String>,
        output_map: FnvHashMap<OutputSymbol, String>,
    ) -> Self {
        Self {
            oq_oracle,
            location,
            input_map,
            output_map,
            extra_states,
        }
    }

    fn hads_process(&self, lookahead: usize) -> Result<Child, Box<dyn std::error::Error>> {
        const STATECHUM_CP: &str = "bin:lib/modified_collections:lib/colt.jar:lib/commons-collections-3.1.jar:lib/jung-1.7.6.jar:lib/OtpErlang.jar ";

        let statechum = Command::new("java")
            .current_dir(self.location.as_str())
            .args([
                "-cp",
                STATECHUM_CP,
                "statechum.apps.WMethodApp",
                "/Users/bharat/rust/automata-lib/spec.dot",
                &lookahead.to_string(),
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;
        Ok(statechum)
    }

    fn parse_tests(&self, child_out: &str) -> Vec<Vec<InputSymbol>> {
        let rev_input: FnvHashMap<String, InputSymbol> = self
            .input_map
            .iter()
            .map(|(k, v)| (v.clone(), *k))
            .collect();

        let test_suite: Vec<_> = child_out
            .lines()
            .map(|s| parse_input_seq(s, &rev_input))
            .collect();
        test_suite
    }
}

fn parse_input_seq(str_seq: &str, rev_input: &FnvHashMap<String, InputSymbol>) -> Vec<InputSymbol> {
    let parse_i = |s: &str| *rev_input.get(s).expect("Safe");
    let seq = str_seq
        .trim()
        .split_ascii_whitespace()
        .map(parse_i)
        .collect();
    seq
}
