
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::learner::obs_tree::ObservationTree;

use super::{CounterExample, EquivalenceOracle};

/// Contains a chain of equivalence oracles to use.
#[allow(clippy::module_name_repetitions)]
pub struct ChainedEO<'sul, T> {
    /// These need to be boxed, as the equivlance oracles are generated in a local function, and not provided
    /// by the caller function.
    oracle_chains: Vec<Box<dyn EquivalenceOracle<'sul, T> + 'sul>>,
}

impl<'sul, T> Default for ChainedEO<'sul, T> {
    fn default() -> Self {
        Self {
            oracle_chains: vec![],
        }
    }
}

impl<'sul, T> ChainedEO<'sul, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn add_oracle(&mut self, oracle: Box<dyn EquivalenceOracle<'sul, T> + 'sul>) {
        self.oracle_chains.push(oracle);
    }
}
impl<'sul, T> EquivalenceOracle<'sul, T> for ChainedEO<'sul, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        let mut symbols = 0;
        let mut resets = 0;
        for oracle in &mut self.oracle_chains {
            let (s, r) = oracle.get_counts();
            symbols += s;
            resets += r;
        }
        (symbols, resets)
    }

    fn get_cost(&mut self) -> usize {
        0
    }

    fn find_counterexample(&mut self, fsm: &Mealy) -> CounterExample {
        self.oracle_chains
            .iter_mut()
            .find_map(|oracle| oracle.find_counterexample(fsm))
    }

    fn find_counterexample_with_lookahead(
        &mut self,
        fsm: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        self.oracle_chains
            .iter_mut()
            .find_map(|oracle| oracle.find_counterexample_with_lookahead(fsm, lookahead))
    }
}
