use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::data_structs::arena_tree::ArenaTree;

use super::best_node::BestR;
use super::index::Index;
use super::prio_queue::PrioQueue;
use super::scoring::{score_sep, score_xfer};
use super::sep_seq::SepSeq;
use super::separating_nodes::SeparatingNodes;
use super::splits::{analyse_input_symb, input_word_is_sep_inj, Type};

#[derive(Debug, Default)]
pub struct Node {
    /// Set of states to split.
    pub label: Vec<State>,
    /// Successor nodes map.
    children: FxHashMap<OutputSymbol, usize>,
    /// Successors of the label states for input sequences.
    successors: FxHashMap<InputSymbol, Vec<State>>,
    /// Separating Sequence.
    pub(super) sep_seq: SepSeq<InputSymbol>,

    // Utilities
    /// Map input to split-type
    split_map: FxHashMap<InputSymbol, Type>,
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        let labels_eq = self.label == other.label;
        if !labels_eq {
            return false;
        }
        let seq_eq = self.sep_seq == other.sep_seq;
        if !seq_eq {
            return false;
        }
        let outs_eq = {
            let self_outs = self.children.keys().collect::<FxHashSet<_>>();
            let other_outs = other.children.keys().collect::<FxHashSet<_>>();
            self_outs == other_outs
        };
        if !outs_eq {
            return false;
        }
        true
    }
}

impl Node {
    /// Get inputs of specific type.
    fn inputs_of_type(&self, s_type: Type) -> Vec<InputSymbol> {
        self.split_map
            .par_iter()
            .filter(|(_, t)| **t == s_type)
            .map(|(i, _)| *i)
            .collect()
    }

    /// Node contains state `s` in the label.
    fn has_state(&self, s: State) -> bool {
        self.label.contains(&s)
    }

    /// Given a block of states, construct a new node,
    /// with the rest of the fields being default values.
    fn from_block(block: &[State]) -> Self {
        let label = block.iter().copied().unique().collect();
        Self {
            label,
            ..Default::default()
        }
    }

    /// A node is separated if it has children assigned to it.
    fn is_separated(&self) -> bool {
        !self.children.is_empty()
    }

    /// Number of states in the label.
    pub(super) fn size(&self) -> usize {
        self.label.len()
    }

    pub(super) fn _injective_split(&self) -> bool {
        let seq = self.sep_seq.seq();
        if seq.is_empty() {
            return false;
        }
        let Some(input) = seq.first() else { return false;};
        let Some(input_type) = self.split_map.get(input) else {return false};
        matches!(*input_type, Type::SepInj | Type::XferInj)
    }

    /// Analyse a node of the tree: check partitions induced by each input and create successors.
    fn analyse(&mut self, fsm: &Mealy) {
        let r_block = &self.label;
        let infos: Vec<_> = fsm
            .input_alphabet()
            .into_par_iter()
            .map(|i| (i, analyse_input_symb(fsm, i, r_block)))
            .filter(|(_, split)| split.i_type() != Type::Useless)
            .collect();
        let inj_sep_input = infos
            .iter()
            .find(|(_, info)| Type::SepInj == info.i_type())
            .map(|(i, _)| *i);
        if let Some(i) = inj_sep_input {
            self.sep_seq = SepSeq::inj(vec![i]);
        }
        for (i, info) in infos {
            self.successors.insert(i, info.all_dests().collect());
            self.split_map.insert(i, info.i_type());
        }
    }
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Default)]
pub struct SplittingTree {
    pub tree: ArenaTree<Node, ()>,
    /// separating Nodes LCA
    sep_lca: SeparatingNodes<State, Index>,
    /// Analysed Indices,
    analysed: FxHashSet<Index>,
}

macro_rules! score_and_update {
    ($curr:ident, $input:ident, $next:ident, $fsm:ident, $tree:ident, $best:ident) => {{
        let score = score_xfer(($tree).ref_at($curr), $input, ($tree).ref_at($next), $fsm);
        if score < ($best).score() {
            ($best).update($input, $next, score);
        }
    }};
}

impl SplittingTree {
    pub(super) fn separating_sequence(&self, block: &[State]) -> SepSeq<InputSymbol> {
        let lca = self.get_lca(block);
        let ret = lca.map(|lca| self.tree.ref_at(lca).sep_seq.clone());
        ret.unwrap_or_default()
    }

    pub(super) fn get_lca(&self, block: &[State]) -> Option<Index> {
        let block = block.iter().copied().unique().collect_vec();
        let (&pivot, rem) = block.split_first().expect("Block contains no states.");
        let lca_found = rem
            .iter()
            .filter_map(|&x| self.sep_lca.check_pair(x, pivot))
            .max_by(|&x, &y| {
                let x_size = self.tree.apply_at(x.to_index(), Node::size);
                let y_size = self.tree.apply_at(y.to_index(), Node::size);
                x_size.cmp(&y_size)
            });
        lca_found
    }

    #[must_use]
    pub fn new(fsm: &Mealy, root_label: &[State]) -> Self {
        let mut helpers = Helpers::default();
        let mut ret = Self::default();
        ret.construct(fsm, root_label, &mut helpers);
        ret.analysed.extend(helpers.analysed_indices.into_iter());
        ret
    }

    pub fn construct(&mut self, fsm: &Mealy, initial_label: &[State], helpers: &mut Helpers) {
        helpers
            .analysed_indices
            .extend(self.analysed.iter().copied());
        let root_node = Node::from_block(initial_label);
        let root_idx = Index::from(self.tree.node(root_node));
        helpers.nodes_in_tree.insert(root_idx);
        let original_block_size = initial_label.len();
        helpers.partition.push(root_idx, original_block_size);

        while let Some(r_idx) = helpers.partition.pop() {
            let r_idx = r_idx;

            // No need to 'split' nodes which have singleton labels.
            if self.ref_at(r_idx).size() == 1 {
                continue;
            }
            if !helpers.analysed_indices.contains(&r_idx) {
                self.analyse(r_idx, fsm, &mut helpers.analysed_indices);
            }

            if self.ref_at(r_idx).sep_seq.is_set() {
                self.separate(r_idx, fsm, helpers);
            } else {
                helpers.dependent.insert(r_idx);
            }
            let curr_block_size = self.ref_at(r_idx).size();
            if !helpers.dependent.is_empty() && helpers.partition.maximal() <= curr_block_size {
                let mut nodes_seen = FxHashSet::default();
                let mut stable = false;
                while !stable {
                    stable = true;
                    for r in helpers.dependent.clone() {
                        if !nodes_seen.insert(r) {
                            continue;
                        }
                        stable &= self.init_trans_on_inj_inputs(fsm, r, helpers);
                    }
                    if !stable {
                        nodes_seen.clear();
                    }
                }
                self.process_dependent(fsm, helpers);
                if !helpers.dependent.is_empty() {
                    let mut nodes_seen = FxHashSet::default();
                    let mut stable = false;
                    while !stable {
                        stable = true;
                        for r in helpers.dependent.clone() {
                            if !nodes_seen.insert(r) {
                                continue;
                            }
                            stable &= self.init_trans_on_inj_inputs(fsm, r, helpers);
                            if !self.ref_at(r).sep_seq.is_inj() {
                                stable &= self.init_trans_on_non_inj_inputs(fsm, r, helpers);
                            }
                        }
                        if !stable {
                            nodes_seen.clear();
                        }
                    }
                    self.process_dependent(fsm, helpers);
                }
            } else if !helpers.dependent.is_empty() {
                unreachable!(
                    "How do we have a block in the partition bigger than the current block?"
                );
            }
            if !self.ref_at(r_idx).is_separated() {
                helpers.partition.push(r_idx, self.ref_at(r_idx).size());
            }
        }

        let block = initial_label.iter().copied().collect_vec();
        let mut sep_nodes = vec![];
        Itertools::cartesian_product(block.iter().copied(), block.iter().copied())
            .filter(|(x, y)| x < y)
            .map(|(s1, s2)| (s1, s2, self.lca_of_two(s1, s2).expect("Safe")))
            .for_each(|(s1, s2, lca)| {
                sep_nodes.push((s1, s2, lca));
            });
        for (s1, s2, lca) in sep_nodes {
            if self.sep_lca.check_pair(s1, s2).is_none() {
                self.sep_lca.insert_pair(s1, s2, lca);
            }
        }
    }

    /// Initialise transitions on injective inputs.
    fn init_trans_on_inj_inputs(&mut self, fsm: &Mealy, r: Index, helpers: &mut Helpers) -> bool {
        let mut stable = true;
        let mut best_r = helpers.best_r.get(&r).cloned().unwrap_or_default(); // BestR::default();
        let injective_xfer_inputs = self.ref_at(r).inputs_of_type(Type::XferInj);

        // For each valid transferring input,
        for input in injective_xfer_inputs {
            // get lca of the dest states.
            let dest_block = self
                .ref_at(r)
                .successors
                .get(&input)
                .expect("Injective xfer input should have successors.");
            if let Some(r_x) = self.maybe_lca(dest_block, &helpers.nodes_in_tree) {
                if helpers.dependent.contains(&r_x) {
                    helpers.add_transition_from_to_via(r, r_x, input);
                } else {
                    score_and_update!(r, input, r_x, fsm, self, best_r);
                }
            } else {
                let new_node = Node::from_block(dest_block);
                let r_x = self.find_node_exact(dest_block).unwrap_or_else(|| {
                    let uidx = self.tree.node(new_node);
                    Index::from(uidx)
                });

                if helpers.analysed_indices.contains(&r_x) && self.ref_at(r_x).sep_seq.is_inj() {
                    score_and_update!(r, input, r_x, fsm, self, best_r);
                }
                if !helpers.analysed_indices.contains(&r_x) || !self.ref_at(r_x).sep_seq.is_set() {
                    if !helpers.analysed_indices.contains(&r_x) {
                        self.analyse(r_x, fsm, &mut helpers.analysed_indices);
                    }
                    if self.ref_at(r_x).sep_seq.is_set() {
                        score_and_update!(r, input, r_x, fsm, self, best_r);
                    } else {
                        stable = !helpers.dependent.insert(r_x);
                    }
                    helpers.add_transition_from_to_via(r, r_x, input);
                }
            }
        }
        helpers.best_r.insert(r, best_r);

        // If the best_r.next node has a valid separating sequence, then add the current node to the
        // dependent priority queue.
        let best_r = helpers.best_r.get(&r).expect("Safe");
        let next_has_inj_seq = best_r
            .next()
            .map(|x| self.ref_at(x).sep_seq.is_inj())
            .unwrap_or_default();

        if next_has_inj_seq {
            helpers.dependent_prio_queue.push(r, best_r.score());
        }
        stable
    }

    fn process_dependent(&mut self, fsm: &Mealy, helpers: &mut Helpers) {
        while let Some(r) = helpers.dependent_prio_queue.pop() {
            helpers.dependent.remove(&r);
            if self.ref_at(r).is_separated() {
                continue;
            }
            let best_r = helpers.best_r.get(&r).expect("Safe");
            let sep_seq = {
                let xfer_input = best_r
                    .input()
                    .expect("Xfer input should be defined for a dependent node.");
                let seq = best_r
                    .next()
                    .map(|idx| self.ref_at(idx).sep_seq.seq())
                    .cloned()
                    .unwrap_or_default();
                let mut sep_seq = vec![xfer_input];
                sep_seq.extend(seq);
                sep_seq
            };

            let iw_is_sep_inj = input_word_is_sep_inj(fsm, &sep_seq, &self.ref_at(r).label);
            let sep_seq = if iw_is_sep_inj {
                SepSeq::inj(sep_seq)
            } else {
                SepSeq::non_inj(sep_seq)
            };
            self.tree.arena[r.to_index()].val.sep_seq = sep_seq;
            self.separate(r, fsm, helpers);
            if let Some(trans_to_r) = helpers.transitions_to.get(&r) {
                for &(p_idx, input) in trans_to_r {
                    if self.ref_at(p_idx).sep_seq.is_set() {
                        continue;
                    }
                    let score_p = score_xfer(self.ref_at(p_idx), input, self.ref_at(r), fsm);
                    let best_p_score = helpers.score_of(p_idx);
                    if score_p < best_p_score {
                        let best_p = BestR::construct(input, r, score_p);
                        helpers.best_r.insert(p_idx, best_p);
                        helpers.dependent_prio_queue.push(p_idx, score_p);
                    }
                }
            }
        }
    }

    fn init_trans_on_non_inj_inputs(
        &mut self,
        fsm: &Mealy,
        r: Index,
        helpers: &mut Helpers,
    ) -> bool {
        let mut stable = true;
        // let best_r_score = helpers.score_of(r);
        let best_r = helpers.best_r.entry(r).or_default();
        let input_non_inj_separating =
            |i, label| analyse_input_symb(fsm, i, label).non_inj_sep_input();
        let best_non_inj_sep_input = fsm
            .input_alphabet()
            .into_par_iter()
            .filter(|&x| input_non_inj_separating(x, &self.ref_at(r).label))
            .map(|x| (x, score_sep(self.ref_at(r), x, fsm)))
            .filter(|(_, score)| *score < best_r.score()) // score is better than best score.
            .min_by(|(_, a_score), (_, b_score)| a_score.cmp(b_score)); // choose the best score.

        // If the result is Some, then the invalid separating input has a lower score than
        // whatever score we had before, so we update best_r with that information.
        if let Some((i, score)) = best_non_inj_sep_input {
            best_r.update(i, None, score);
        }

        // Now, we must check all non-injective transferring inputs.
        let invalid_xfer_inputs = self.ref_at(r).inputs_of_type(Type::XferNonInj);
        // If one of them leads to a lower score than the non-injective separating
        // input that we might have selected earlier, we update the best node accordingly.
        for input in invalid_xfer_inputs {
            let succ = self
                .ref_at(r)
                .successors
                .get(&input)
                .expect("Analysis was incomplete?");
            let maybe_r_x = self.maybe_lca(succ, &helpers.nodes_in_tree);
            let next_r;
            if let Some(r_x) = maybe_r_x {
                next_r = r_x;
                if self.ref_at(r_x).sep_seq.is_set() {
                    let new_score = score_xfer(self.ref_at(r), input, self.ref_at(r_x), fsm);
                    if new_score < best_r.score() {
                        best_r.update(input, r_x, new_score);
                    }
                }
            } else {
                let new_node = Node::from_block(succ);
                let r_x = self.find_node_exact(succ).unwrap_or_else(|| {
                    let uidx = self.tree.node(new_node);
                    Index::from(uidx)
                });
                next_r = r_x;
                if !helpers.analysed_indices.contains(&r_x) || !self.ref_at(r_x).sep_seq.is_set() {
                    if !helpers.analysed_indices.contains(&r_x) {
                        self.analyse(r_x, fsm, &mut helpers.analysed_indices);
                    }
                    if self.ref_at(r_x).sep_seq.is_set() {
                        let score = score_xfer(self.ref_at(r), input, self.ref_at(r_x), fsm);
                        if score < best_r.score() {
                            best_r.update(input, r_x, score);
                        }
                    } else {
                        // stable = !helpers.dependent.insert(r_x);
                    }
                    {
                        helpers
                            .transitions_to
                            .entry(r_x)
                            .or_default()
                            .insert((r, input));
                    };
                }
                if !self.ref_at(r_x).sep_seq.is_set() {
                    stable = !helpers.dependent.insert(r_x);
                }
            }
            if self.ref_at(next_r).sep_seq.is_set() {
                let score = score_xfer(self.ref_at(r), input, self.ref_at(next_r), fsm);
                if score < best_r.score() {
                    best_r.update(input, next_r, score);
                }
            }
        }
        let r_priority = helpers.score_of(r);
        if r_priority != usize::MAX {
            helpers.dependent_prio_queue.push(r, r_priority);
        }
        stable
    }

    fn maybe_lca(&self, block: &[State], nodes_in_tree: &FxHashSet<Index>) -> Option<Index> {
        if nodes_in_tree.len() == 1 {
            return Some(Index::from(0));
        }
        Itertools::cartesian_product(block.iter().copied(), block.iter().copied())
            .filter(|(x, y)| x != y)
            .filter_map(|(s1, s2)| self.lca_of_two(s1, s2))
            .max_by(|x, y| self.ref_at(*x).size().cmp(&self.ref_at(*y).size()))
    }

    fn lca_of_two(&self, s1: State, s2: State) -> Option<Index> {
        let mut cand = Index::from(0);
        let find_child_with_state = |r: &Node, st: State| {
            r.children
                .values()
                .find(|&&s| self.ref_at(Index::from(s)).has_state(st))
                .copied()
        };
        let find_children_at_node = |r: &Node| {
            let s1_child = find_child_with_state(r, s1);
            let s2_child = find_child_with_state(r, s2);
            s1_child.zip(s2_child)
        };
        let node_splits =
            |node: &Node| find_children_at_node(node).map_or(false, |(c1, c2)| c1 != c2);

        let next_node = |node: &Node| {
            find_children_at_node(node)
                .filter(|(c1, c2)| c1 == c2)
                .map(|(x, _)| Index::from(x))
        };

        loop {
            if node_splits(self.ref_at(cand)) {
                return Some(cand);
            }
            cand = next_node(self.ref_at(cand))?;
        }
    }

    /// Find the node index containing the exact `block` of states, if it exists.
    fn find_node_exact(&self, block: &[State]) -> Option<Index> {
        (0..self.tree.size())
            .map(Index::from)
            .filter(|&s| block.len() == self.ref_at(s).size())
            .find(|&i| block.iter().all(|s| self.ref_at(i).has_state(*s)))
    }

    fn analyse(&mut self, r_idx: Index, fsm: &Mealy, analysed: &mut FxHashSet<Index>) {
        self.tree.arena[r_idx.to_index()].val.analyse(fsm);
        analysed.insert(r_idx);
    }

    fn separate(&mut self, r_idx: Index, fsm: &Mealy, helpers: &mut Helpers) {
        // We only want to separate the nodes in the tree.
        if !helpers.nodes_in_tree.contains(&r_idx) {
            return;
        }
        let r_node = self.ref_at(r_idx);
        let seq = r_node.sep_seq.seq();
        let mut output_src_map = FxHashMap::<Box<[OutputSymbol]>, Vec<State>>::default();
        for s in &r_node.label {
            let outs = fsm.trace_from(*s, seq).1;
            output_src_map.entry(outs).or_default().push(*s);
        }
        // Create and add successors to the splitting tree using the output_src_map.
        let child_indxs = output_src_map
            .into_iter()
            .map(|(out_seq, src_label)| {
                let os = *out_seq.last().expect("Safe");
                let child_node = Node::from_block(&src_label);
                (os, child_node)
            })
            .map(|(out_symbol, child_node)| {
                let child_node_idx = self.tree.node(child_node);
                let r_node = &mut self.tree.arena[r_idx.to_index()].val;
                r_node.children.insert(out_symbol, child_node_idx);
                Index::from(child_node_idx)
            })
            .collect_vec();
        child_indxs
            .into_iter()
            // Singleton children don't need to be split.
            .filter(|c| self.ref_at(*c).size() > 1)
            .for_each(|c| {
                let prio = self.ref_at(c).size();
                helpers.nodes_in_tree.insert(c);
                helpers.partition.push(c, prio);
            });
    }

    fn ref_at(&self, idx: Index) -> &Node {
        self.tree.ref_at(idx)
    }
}

#[derive(Debug, Default)]
pub struct Helpers {
    /// Set of nodes in the splitting tree.
    /// We do not want the auxilary nodes to be present in the tree.
    nodes_in_tree: FxHashSet<Index>,
    /// Queue of indices indicating leaf nodes of the splitting tree.
    partition: PrioQueue<Index, usize>,
    /// Nodes that do not have a sep. input. We need a xfering or invalid input.
    dependent: FxHashSet<Index>,
    /// Helper queue for processing nodes from ``dependent``.
    dependent_prio_queue: PrioQueue<Index, usize>,
    /// Links from a node to others.
    /// Key is the dest idx and value is a set of (src,input) tuples.
    transitions_to: FxHashMap<Index, FxHashSet<(Index, InputSymbol)>>,

    // Utilities: Yes, really.
    analysed_indices: FxHashSet<Index>,
    /// Map a node to the current "best" possible node to split the former.
    best_r: FxHashMap<Index, BestR>,
}

impl Helpers {
    /// Add a transition from index `src` to index `dest` using input `via`.
    #[inline]
    fn add_transition_from_to_via(&mut self, src: Index, dest: Index, via: InputSymbol) {
        self.transitions_to
            .entry(dest)
            .or_default()
            .insert((src, via));
    }

    /// Get score of the node `r`.
    #[inline]
    fn score_of(&self, r: Index) -> usize {
        self.best_r.get(&r).map(BestR::score).expect("Safe")
    }
}

#[cfg(test)]
mod tests {

    use super::{Node, SplittingTree, Type};
    use crate::{definitions::FiniteStateMachine, util::parsers::machine::read_mealy_from_file};
    use itertools::Itertools;
    use rstest::rstest;
    use rustc_hash::FxHashMap;

    #[rstest]
    #[case::soucha_example_input_analysis("tests/src_models/iads_example.dot")]
    /// Check if the block of nodes is analysed correctly.
    fn analyse_input(#[case] file_name: &str) {
        let (fsm, input_map, _) = read_mealy_from_file(file_name);
        let states = fsm.states().into_iter().collect_vec();
        let mut r_node = Node::from_block(&states);
        r_node.analyse(&fsm);
        let split_map = r_node.split_map;

        let exp_splits = [Type::SepNonInj, Type::SepNonInj, Type::XferNonInj];
        let exp_map: FxHashMap<_, _> = ["a", "b", "c"]
            .into_iter()
            .filter_map(|x| input_map.get(x))
            .copied()
            .zip(exp_splits.into_iter())
            .collect();

        assert!(
            exp_map == split_map,
            "{}",
            format!("Actual split map: {split_map:?}")
        );
    }

    #[rstest]
    #[case("tests/src_models/esm_hypothesis_534.dot")]
    #[case("tests/src_models/hypothesis_6.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/TCP_FreeBSD_Server.dot")]
    #[case("tests/src_models/bestr_TCP_Win_serv_hyp10.dot")]
    #[case::win8_server_loop("tests/src_models/tcp_win8_serv_hyp10.dot")]
    #[case::linux_server_hyp24("tests/src_models/stree_tcp_linux_srv_hyp24.dot")]
    #[case("tests/src_models/iads_example.dot")]
    fn all_states_split(#[case] file_name: &str) {
        let (fsm, _, _) = read_mealy_from_file(file_name);
        let s_tree = SplittingTree::new(&fsm, &fsm.states());
        let fsm_states = fsm.states();
        let states = fsm_states.iter().copied();
        for (x, y) in
            Itertools::cartesian_product(states.clone(), states.clone()).filter(|(x, y)| x < y)
        {
            s_tree.get_lca(&[x, y]).unwrap();
        }
    }
}
