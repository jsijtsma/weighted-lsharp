use std::{cell::RefCell, collections::HashMap, rc::Rc};

use fnv::FnvHashMap;
use itertools::Itertools;
use rand::prelude::StdRng;
use rand::{Rng, SeedableRng};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use rustc_hash::FxHashMap;

use crate::ads::AdaptiveDistinguishingSequence;
use crate::definitions::mealy::{
    shortest_separating_sequence, InputSymbol, Mealy, OutputSymbol, State,
};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::equivalence::InfixStyle;
use crate::oracles::equivalence::{generic::EOParams, CounterExample, EquivalenceOracle};
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::access_seqs::{access_sequences, InputSelection, SearchStrategy};
use crate::util::sequences::{
    FixedInfixGenerator, Order, RandomAccessSequences, RandomInfixGenerator,
};
use crate::util::toolbox;

use super::{impl_ads::AdsTree, tree::SplittingTree};

#[allow(clippy::module_name_repetitions)]
pub struct IadsEO<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    lookahead: usize,
    params: EOParams,
    rng: StdRng,
    revisit: usize,
    ads_map: FnvHashMap<Vec<State>, AdsTree>,
    cost_map: &'a FxHashMap<InputSymbol, usize>,
}

impl<'a, T> IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        lookahead: usize,
        params: EOParams,
        cost_map: &'a FxHashMap<InputSymbol, usize>,
    ) -> Self {
        let rng = StdRng::seed_from_u64(params.seed);
        Self {
            oq_oracle,
            params,
            revisit: 0,
            lookahead,
            rng,
            ads_map: HashMap::default(),
            cost_map,
        }
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        self.find_counterexample_with_lookahead(hypothesis, self.params.extra_states)
    }

    fn find_counterexample_with_lookahead(
        &mut self,
        hyp: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        self.ads_map.clear();
        self.revisit = 0;
        if hyp.states().len() == 1 {
            return self.find_counterexample_initial(hyp);
        }

        match self.params.infix_style {
            InfixStyle::Finite => self.run_finite_suite(hyp, lookahead),

            InfixStyle::Infinite => self.run_infinite_suite(hyp, lookahead),
        }
    }
}

impl<'a, T> IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
{
    fn run_infinite_suite(&mut self, hyp: &Mealy, lookahead: usize) -> CounterExample {
        let hyp_states = hyp.states();

        //println!("weight map in iads: {:?}", weights);
        let access_map = access_sequences(hyp, InputSelection::Normal, SearchStrategy::Dfs, self.cost_map);
        let access_map = access_map.into_iter().collect();
        let acc_selector = RandomAccessSequences::new(&access_map, self.params.seed);
        let rnd_infix_gen = RandomInfixGenerator::new(
            self.rng.gen(),
            hyp.input_alphabet().len(),
            lookahead,
            self.params.expected_random_length,
        )
        .into_iter();
        log::info!("Running AIC sequences.");
        let mut splitting_tree = SplittingTree::new(hyp, &hyp_states.iter().copied().collect_vec());
        let sink_out = RefCell::borrow(&self.oq_oracle).sink_output;
        log::info!("Finished making ST-IADS.");
        let ce = Iterator::zip(acc_selector, rnd_infix_gen)
            .map(|(access, infix)| toolbox::concat_slices(&[access, &infix]))
            .find_map(|prefix| self.run_ads_test(&prefix, hyp, &mut splitting_tree, sink_out));
        log::info!("Num revisits: {}", self.revisit);
        ce
    }

    #[must_use]
    fn run_ads_test(
        &mut self,
        prefix: &[InputSymbol],
        hyp: &Mealy,
        splitting_tree: &mut SplittingTree,
        sink_out: OutputSymbol,
    ) -> CounterExample {
        let hyp_dest = hyp.trace(prefix).0;
        // State hyp_dest must be separated from the following states.
        let mut states_to_sep = self.must_sep_from(prefix, hyp, sink_out);
        log::debug!("States to separate: {:?}", &states_to_sep);
        match &states_to_sep[..] {
            // [] => {
            // unreachable!("Cannot distinguish a state from itself!");
            // }
            // Already separated from everything, no need to run any test.
            [] => return None,
            [_q_p] => return None,
            [q, q_p] => {
                // We need to split "q" from "q'", so we just run an output query.
                let ss = shortest_separating_sequence(hyp, hyp, *q, *q_p);
                let wit = ss.expect("Two states in the hypothesis are not apart!").0;
                let test_input = toolbox::concat_slices(&[prefix, &wit]);
                return self.run_test(hyp, &test_input);
            }
            _ => {}
        };
        // let mut states_to_sep = hyp.states();
        // There are at least three states (in total) we need to split,
        // so it makes sense to use an IADS now.
        states_to_sep.sort_unstable();
        let st_ads = self
            .ads_map
            .entry(states_to_sep.clone())
            .or_insert_with(|| {
                AdsTree::construct(hyp, splitting_tree, &states_to_sep, self.rng.gen())
            });
        let other_states = states_to_sep
            .iter()
            .copied()
            .filter(|&x| x != hyp_dest)
            .collect_vec();

        st_ads.randomise_root(hyp_dest, &other_states);
        log::debug!("IADS:{:?}", &st_ads);
        let ce = RefCell::borrow_mut(&self.oq_oracle).ads_equiv_test(st_ads, prefix, hyp);
        st_ads.reset_to_root();
        ce
    }

    /// Returns the states which `tree_state` is not separated from in the observation tree.
    #[must_use]
    fn must_sep_from(
        &mut self,
        prefix: &[InputSymbol],
        hyp: &Mealy,
        sink_out: OutputSymbol,
    ) -> Vec<State> {
        let temp = RefCell::borrow(&self.oq_oracle);
        let tree = temp.borrow_tree();
        let mut other_states = hyp.states().into_iter().collect();
        // The tree state reached by the prefix.
        // If we end up in a sink state, we return early, since we
        // do not need to identify the sink state.
        let ts = {
            let mut c_t = T::S::default();
            for i in prefix {
                let x = tree.get_out_succ(c_t, *i);
                if let Some((x, y)) = x {
                    c_t = y;
                    if x == sink_out {
                        return vec![];
                    }
                } else {
                    // If all of the prefix seq. is not defined,
                    // we need to split from everything.
                    return other_states;
                }
            }
            c_t
        };

        let state_is_not_apart =
            |s: &State| !tree.tree_and_hyp_states_apart_sink(ts.clone(), *s, hyp, sink_out, 10000);
        let org_num = other_states.len();
        other_states = other_states
            .into_par_iter()
            .filter(state_is_not_apart)
            .collect();
        if other_states.len() < org_num {
            self.revisit += 1;
        }
        other_states
    }

    /// Specialised case for when the hypothesis is a single-state FSM.
    #[must_use]
    fn find_counterexample_initial(&mut self, hyp: &Mealy) -> CounterExample {
        // When the hypothesis has size one, there's no point in testing A.C,
        // as we will have no characterising set.
        // A similar argument works for A.I.C, as C is empty.
        // A will contain only the empty sequence,
        // since we are already in the initial state;
        // Therefore, the only thing left is I<=(n+1)
        let input_alphabet = hyp.input_alphabet();
        let fig = FixedInfixGenerator::new(input_alphabet, self.lookahead + 1, Order::ASC);
        fig.generate().find_map(|seq| self.run_test(hyp, &seq))
    }

    /// Run a single test sequence and return whether it is a CE or not.
    #[inline]
    #[must_use]
    fn run_test(&mut self, hyp: &Mealy, input_seq: &[InputSymbol]) -> CounterExample {
        let hyp_output = hyp.trace(input_seq).1.to_vec();
        let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(input_seq);
        (hyp_output != sut_output).then_some((input_seq.to_vec(), sut_output))
    }

    fn run_finite_suite(&mut self, hyp: &Mealy, lookahead: usize) -> CounterExample {
        let access_map = access_sequences(hyp, InputSelection::Normal, SearchStrategy::Dfs, self.cost_map);
        let acc_vec = access_map.values().cloned().collect_vec();
        let acc_iter = acc_vec.clone().into_iter().map(|acc| (acc, vec![]));
        let fig = FixedInfixGenerator::new(hyp.input_alphabet(), lookahead, Order::ASC).generate();
        let prefix_iter = acc_vec.into_iter().cartesian_product(fig);
        log::info!("Running AIC sequences.");
        let hyp_states = hyp.states();
        let mut splitting_tree = SplittingTree::new(hyp, &hyp_states.iter().copied().collect_vec());
        let sink_out = RefCell::borrow(&self.oq_oracle).sink_output;
        log::info!("Finished making ST-IADS.");
        let ce = acc_iter
            .chain(prefix_iter)
            .map(|(access, infix)| toolbox::concat_slices(&[&access, &infix]))
            .find_map(|prefix| self.run_ads_test(&prefix, hyp, &mut splitting_tree, sink_out));
        log::info!("Num revisits: {}", self.revisit);
        ce
    }
}
