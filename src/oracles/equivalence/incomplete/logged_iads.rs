use super::{impl_ads::AdsTree, tree::SplittingTree};
use crate::{
    ads::AdaptiveDistinguishingSequence,
    definitions::{
        mealy::{shortest_separating_sequence, InputSymbol, Mealy, OutputSymbol, State},
        FiniteStateMachine,
    },
    learner::obs_tree::ObservationTree,
    oracles::{
        equivalence::{generic::EOParams, CounterExample, EquivalenceOracle},
        membership::Oracle as OQOracle,
    },
    util::{
        access_seqs::{access_sequences, InputSelection, SearchStrategy},
        sequences::{FixedInfixGenerator, LogExpSequences, Order, RandomInfixGenerator},
        toolbox,
    },
};
use fnv::FnvHashMap;
use itertools::Itertools;
use rand::{
    prelude::{SliceRandom, StdRng},
    Rng, SeedableRng,
};
use rand_distr::{Bernoulli, Distribution};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use rustc_hash::FxHashMap;
use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[allow(clippy::module_name_repetitions)]
pub struct IadsEO<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    lookahead: usize,
    params: EOParams,
    rng: StdRng,
    ads_map: FnvHashMap<Vec<State>, AdsTree>,
    logs: Vec<Vec<InputSymbol>>,
    cost_map: &'a FxHashMap<InputSymbol, usize>,
}

impl<'a, T> IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        lookahead: usize,
        params: EOParams,
        logs: Vec<Vec<InputSymbol>>,
        cost_map: &'a FxHashMap<InputSymbol, usize>,
    ) -> Self {
        let rng = StdRng::seed_from_u64(params.seed);
        Self {
            oq_oracle,
            params,
            lookahead,
            rng,
            ads_map: HashMap::default(),
            logs,
            cost_map,
        }
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        self.find_counterexample_with_lookahead(hypothesis, self.params.extra_states)
    }

    fn find_counterexample_with_lookahead(
        &mut self,
        hyp: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        self.ads_map.clear();
        let hyp_states = hyp.states();
        let num_states = hyp_states.len();
        if num_states == 1 {
            return self.find_counterexample_initial(hyp);
        }

        let infix_picker = Bernoulli::new(0.5).expect("Safe");
        // let _access_map = random_access_map(hyp, &mut self.rng);

        //println!("weight map in logged_iads: {:?}", weights);
        let access_map = access_sequences(
            hyp,
            InputSelection::rand_with_seed(self.rng.gen()),
            SearchStrategy::Dfs,
            self.cost_map,
        );
        let access_vec = access_map.into_values().collect_vec();
        let log_rig = LogExpSequences::from_traces(
            &self.logs,
            self.params.seed,
            self.params.extra_states,
            self.params.expected_random_length,
        );
        let mut log_infix_gen = log_rig.into_iter();
        let _sink_out = {
            let temp = RefCell::borrow(&self.oq_oracle);
            temp.pass_maps()
                .1
                .into_iter()
                .find(|(st, _)| st == "error")
                .map_or(OutputSymbol::new(u16::MAX), |x| x.1)
        };

        let rnd_infix_gen = RandomInfixGenerator::new(
            self.rng.gen(),
            hyp.input_alphabet().len(),
            lookahead,
            self.params.expected_random_length,
        );
        let mut infix_gen = rnd_infix_gen.into_iter();

        log::info!("Running AIC sequences.");
        let mut splitting_tree = SplittingTree::new(hyp, &hyp_states.iter().copied().collect_vec());
        let sink_out = RefCell::borrow(&self.oq_oracle).sink_output;
        log::info!("Finished making ST-IADS.");

        loop {
            let infix = if infix_picker.sample(&mut self.rng) {
                infix_gen.next().expect("Safe")
            } else {
                log_infix_gen.next().expect("Safe")
            };
            let access = access_vec.choose(&mut self.rng).expect("Safe");
            let prefix = toolbox::concat_slices(&[access, &infix]);
            let ce = self.run_ads_test(&prefix, hyp, &mut splitting_tree, sink_out);
            if ce.is_some() {
                return ce;
            }
        }
        // for infix in rnd_infix_gen {}
        // unreachable!("This process never ends until a CE is found.");
    }
}

impl<'a, T> IadsEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
{
    #[must_use]
    fn run_ads_test(
        &mut self,
        prefix: &[InputSymbol],
        hyp: &Mealy,
        splitting_tree: &mut SplittingTree,
        sink_out: OutputSymbol,
    ) -> CounterExample {
        let hyp_dest = hyp.trace(prefix).0;
        // State hyp_dest must be separated from the following states.
        let mut states_to_sep = self.must_sep_from(prefix, hyp, sink_out);
        log::debug!("States to separate: {:?}", &states_to_sep);
        match &states_to_sep[..] {
            // [] => {
            // unreachable!("Cannot distinguish a state from itself!");
            // }
            // Already separated from everything, no need to run any test.
            [] => return None,
            [_q_p] => return None,
            [q, q_p] => {
                // We need to split "q" from "q'", so we just run an output query.
                let wit = shortest_separating_sequence(hyp, hyp, *q, *q_p)
                    .map(|(x, _)| x)
                    .expect("Two states in the hypothesis are not apart!");
                let test_input = toolbox::concat_slices(&[prefix, &wit]);
                return self.run_test(hyp, &test_input);
            }
            _ => {}
        };
        // let mut states_to_sep = hyp.states();
        // There are at least three states (in total) we need to split,
        // so it makes sense to use an IADS now.
        states_to_sep.sort_unstable();
        let st_ads = self
            .ads_map
            .entry(states_to_sep.clone())
            .or_insert_with(|| {
                AdsTree::construct(hyp, splitting_tree, &states_to_sep, self.rng.gen())
            });
        let other_states = states_to_sep
            .iter()
            .copied()
            .filter(|&x| x != hyp_dest)
            .collect_vec();

        st_ads.randomise_root(hyp_dest, &other_states);
        log::debug!("IADS:{:?}", &st_ads);
        let ce = RefCell::borrow_mut(&self.oq_oracle).ads_equiv_test(st_ads, prefix, hyp);
        st_ads.reset_to_root();
        ce
    }

    /// Returns the states which `tree_state` is not separated from in the observation tree.
    #[must_use]
    fn must_sep_from(
        &mut self,
        prefix: &[InputSymbol],
        hyp: &Mealy,
        sink_out: OutputSymbol,
    ) -> Vec<State> {
        let temp = RefCell::borrow(&self.oq_oracle);
        let tree = temp.borrow_tree();
        let mut other_states = hyp.states().into_iter().collect();
        let ts = {
            let mut c_t = T::S::default();
            for i in prefix {
                let x = tree.get_out_succ(c_t, *i);
                if let Some((x, y)) = x {
                    c_t = y;
                    if x == sink_out {
                        return vec![];
                    }
                } else {
                    return other_states;
                }
            }
            c_t
        };
        // let tree_dest = tree.get_succ(T::S::default(), prefix);
        // let ts = match tree_dest {
        //     Some(ts) => ts,
        //     None => return other_states,
        // };
        let _org_num = other_states.len();
        // TODO: Do something about the repeated apartness checks.
        other_states = other_states
            .into_par_iter()
            .filter(|s| {
                !tree.tree_and_hyp_states_apart_sink(ts.clone(), *s, hyp, sink_out, 10000)
                // !tree_and_hyp_states_apart_sunk_bounded(tree, ts.clone(), *s, hyp, sink_out, 100)
            })
            .collect();
        // if other_states.len() < org_num {
        //     self.revisit += 1;
        // }
        other_states
    }

    /// Specialised case for when the hypothesis is a single-state FSM.
    #[must_use]
    fn find_counterexample_initial(&mut self, hyp: &Mealy) -> CounterExample {
        // When the hypothesis has size one, there's no point in testing A.C,
        // as we will have no characterising set.
        // A similar argument works for A.I.C, as C is empty.
        // A will contain only the empty sequence,
        // since we are already in the initial state;
        // Therefore, the only thing left is I<=(n+1)
        let input_alphabet = hyp.input_alphabet();
        let fig = FixedInfixGenerator::new(input_alphabet, self.lookahead + 1, Order::ASC);
        fig.generate().find_map(|seq| self.run_test(hyp, &seq))
    }

    /// Run a single test sequence and return whether it is a CE or not.
    #[inline]
    #[must_use]
    fn run_test(&mut self, hyp: &Mealy, input_seq: &[InputSymbol]) -> CounterExample {
        let hyp_output = hyp.trace(input_seq).1.to_vec();
        let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(input_seq);
        if hyp_output == sut_output {
            return None;
        }
        Some((input_seq.to_vec(), sut_output))
    }
}
