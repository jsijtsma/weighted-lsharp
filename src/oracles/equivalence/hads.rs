use std::fmt::Debug;
use std::{
    cell::RefCell,
    collections::HashMap,
    io::{BufRead, BufReader, Write},
    process::{Child, Command, Stdio},
    rc::Rc,
    thread,
    time::{Duration, Instant},
};

use derive_builder::Builder;
use fnv::FnvBuildHasher;
use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::writers::overall as MealyWriter;

use super::{CounterExample, EquivalenceOracle};

#[derive(Default, Builder, Debug)]
// #[builder(build_fn(skip))]
pub struct Config {
    mode: Mode,
    prefix: Prefix,
    suffix: Suffix,
    #[builder(default = "3")]
    extra_states: usize,
    #[builder(default = "5")]
    expected_rnd_length: usize,
    seed: u64,
    #[builder(default = "Duration::new(20*60,0)")]
    max_duration: Duration,
}

impl Config {
    /// # Errors
    /// If the path is not valid UTF-8 or cannot be canonicalised.
    pub fn oracle<'a, T>(
        self,
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        location: &str,
    ) -> Result<Oracle<'a, T>, Box<dyn std::error::Error>> {
        let hads_path = std::fs::canonicalize(location)?;
        let location = hads_path
            .to_str()
            .ok_or("Path is not valid UTF-8")?
            .to_string();
        let config = self;
        let ret = Oracle {
            oq_oracle,
            config,
            location,
        };
        Ok(ret)
    }
}

/// Hybrid-ADS equivalence oracle.
///
/// This module is not the implementation of the equivalence oracle, it is
/// just a wrapper written to communicate with the actual oracle. During each EQ,
/// we write the hypothesis to the hybrid-ads program and use the tests generated
/// by the same. The implementation of the oracle is in the hybrid-ads submodule of
/// the repository.
pub struct Oracle<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    config: Config,
    location: String,
}
impl<'a, T> EquivalenceOracle<'a, T> for Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }


    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        {
            let sleepy = Duration::from_millis(100);
            thread::sleep(sleepy);
        }
        self.find_counterexample_with_lookahead(hypothesis, self.config.extra_states)
    }
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        let mut child = self
            .hads_process(Some(lookahead))
            .expect("H-ADS process did not initialise.");
        {
            let write_config = MealyWriter::WriteConfigBuilder::default()
                .file_name(None)
                .format(MealyWriter::MealyEncoding::Dot)
                .build()
                .unwrap();
            let byte_hyp = MealyWriter::write_machine::<FnvBuildHasher, _>(
                &write_config,
                hypothesis,
                &HashMap::default(),
                &HashMap::default(),
            )
            .expect("Writer did not return the byte-encoded mealy machine.");
            let child_stdin = child.stdin.as_mut().unwrap();
            child_stdin
                .write_all(&byte_hyp)
                .expect("Failed to write hypothesis model to h-ads program!");
            child_stdin
                .flush()
                .expect("Failed to write hypothesis model to h-ads program!");
        }
        // Get a Pipestream, which implements the Reader trait.
        let mut child_out = BufReader::new(child.stdout.as_mut().unwrap());
        let start = Instant::now();
        let mut oracle_mut_borrow = RefCell::borrow_mut(&self.oq_oracle);
        let mut buffer = Vec::new();
        loop {
            Self::read_into_buffer(&mut child_out, 1000, &mut buffer);
            for input_vec in &buffer {
                let hyp_output = hypothesis.trace(input_vec).1.to_vec();
                let sut_output = oracle_mut_borrow.output_query(input_vec);
                if sut_output.len() < hyp_output.len() {
                    let hyp_out_lim = &sut_output[..sut_output.len()];
                    if hyp_out_lim != sut_output {
                        child
                            .kill()
                            .expect("Could not manage to kill H-ADS process.");
                        let ce_input = input_vec[..sut_output.len()].to_vec();
                        let ce_output = sut_output;
                        let ret = Some((ce_input, ce_output));
                        return ret;
                    }
                } else if hyp_output != sut_output {
                    // We need to kill the child, as we cannot signal the
                    // H-ADS process to terminate.
                    child
                        .kill()
                        .expect("Could not manage to kill H-ADS process.");
                    let ce_input = input_vec.clone();
                    let ce_output = sut_output;
                    let ret = Some((ce_input, ce_output));
                    return ret;
                }
            }
            buffer.clear();
            if start.elapsed() >= self.config.max_duration {
                return None;
            }
        }
    }
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync,
{
    fn hads_process(
        &self,
        lookahead_custom: Option<usize>,
    ) -> Result<Child, Box<dyn std::error::Error>> {
        let h_ads_path = std::fs::canonicalize(self.location.clone())?;
        let es = lookahead_custom.unwrap_or(self.config.extra_states);
        let c = Command::new(h_ads_path)
            .args([
                "-m",
                self.config.mode.to_string().to_ascii_lowercase().as_str(),
                "-p",
                self.config.prefix.to_string().to_ascii_lowercase().as_str(),
                "-k",
                &es.to_string(),
                "-s",
                self.config.suffix.to_string().to_ascii_lowercase().as_str(),
                "-r",
                &self.config.expected_rnd_length.to_string(),
                "-x",
                &self.config.seed.to_string(),
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;
        Ok(c)
    }

    fn read_into_buffer(
        child_out: &mut BufReader<&mut std::process::ChildStdout>,
        buffer_size: usize,
        ret: &mut Vec<Vec<InputSymbol>>,
    ) {
        let mut string_vec = Vec::with_capacity(buffer_size);
        let mut i = 0;
        while i <= buffer_size {
            let mut strline = String::new();
            child_out.read_line(&mut strline).unwrap();
            if strline.is_empty() {
                continue;
            }
            string_vec.push(strline);
            i += 1;
        }
        string_vec
            .into_iter()
            .map(|strline| {
                strline
                    .trim()
                    .split_ascii_whitespace()
                    .map(|str_input| InputSymbol::from(str_input.parse::<usize>().expect("Safe")))
                    .collect_vec()
            })
            .for_each(|x| ret.push(x));
    }
}

use std::string::ToString;
#[derive(PartialEq, Eq, Clone, Copy, strum_macros::Display, Debug, Default)]
pub enum Mode {
    All,
    Fixed,
    #[default]
    Random,
}

#[derive(PartialEq, Eq, Clone, Copy, strum_macros::Display, Debug, Default)]
pub enum Prefix {
    Minimal,
    Lexmin,
    #[default]
    Buggy,
    Longest,
}

#[derive(PartialEq, Eq, Clone, Copy, strum_macros::Display, Debug, Default)]
pub enum Suffix {
    #[default]
    Hads,
    Hsi,
    None,
}
