use std::{
    cell::RefCell,
    fmt::{Debug, Display},
    io::Write,
    process::{Child, Command, Stdio},
    rc::Rc,
};

use fnv::FnvHashMap;
use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::equivalence::{CounterExample, EquivalenceOracle};
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::sequences::{FixedInfixGenerator, Order};
use crate::util::writers::overall as MealyWriter;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Default)]
pub enum Method {
    H,
    #[default]
    HSI,
    SPY,
    SPYH,
    Unused,
}

#[derive(
    Debug, PartialEq, Eq, PartialOrd, Ord, derive_builder::Builder, derive_getters::Getters,
)]
pub struct Config {
    /// Which FSMLib CTT should we use?
    method: Method,
    lookahead: usize,
    #[builder(setter(into))]
    exec_loc: String,
}

impl Display for Method {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Method::HSI => write!(f, "hsi"),
            Method::SPY => write!(f, "spy"),
            Method::SPYH => write!(f, "spyh"),
            Method::H => write!(f, "h"),
            Method::Unused => write!(f, "unused"),
        }
    }
}

pub struct Oracle<'a, Tree> {
    oq_oracle: Rc<RefCell<OQOracle<'a, Tree>>>,
    rev_input_map: FnvHashMap<InputSymbol, String>,
    rev_output_map: FnvHashMap<OutputSymbol, String>,
    config: Config,
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    /// Creates a new [`Oracle<T>`] using `FSMLib`.
    pub fn new(
        output_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        config: Config,
        rev_input_map: FnvHashMap<InputSymbol, String>,
        rev_output_map: FnvHashMap<OutputSymbol, String>,
    ) -> Self {
        Self {
            oq_oracle: output_oracle,
            rev_input_map,
            rev_output_map,
            config,
        }
    }

    fn parse_tests(strx: &str) -> Vec<Vec<InputSymbol>> {
        // Format: tc_{id}: i1,i2,i3,...,iN\n
        let test_str: for<'b> fn(&'b str) -> &'b str = |line: &str| {
            line.split_ascii_whitespace()
                .nth(1)
                .expect("Test case did not have an input sequence?")
        };
        let parse_input_symbols = |line: &str| {
            line.split(',')
                .map(|x| x.parse::<u16>().expect("Input symbol could not be parsed."))
                .map(InputSymbol::from)
                .collect_vec()
        };
        let test_suite = strx
            .lines()
            .map(test_str)
            .map(parse_input_symbols)
            .collect();

        test_suite
    }
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync,
{
    fn soucha_process(&self, lookahead: usize) -> Child {
        let fsmlib_path = std::fs::canonicalize(self.config.exec_loc().clone()).expect("Safe");
        log::info!(
            "Calling fsm-lib with ctt {} and es {}",
            self.config.method().to_string(),
            lookahead
        );
        Command::new(fsmlib_path)
            .args([
                "-m",
                self.config.method().to_string().as_str(),
                "-es",
                &lookahead.to_string(),
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .unwrap()
    }

    /// Specialised case for when the hypothesis is a single-state FSM.
    #[must_use]
    fn find_counterexample_initial(&mut self, hyp: &Mealy) -> CounterExample {
        // When the hypothesis has size one, there's no point in testing A.C,
        // as we will have no characterising set.
        // A similar argument works for A.I.C, as C is empty.
        // A will contain only the empty sequence,
        // since we are already in the initial state;
        // Therefore, the only thing left is I<=(n+1)
        let fig =
            FixedInfixGenerator::new(hyp.input_alphabet(), self.config.lookahead + 1, Order::ASC);
        fig.generate().find_map(|seq| self.run_test(hyp, &seq))
    }

    /// Run a single test sequence and return whether it is a CE or not.
    #[inline]
    #[must_use]
    fn run_test(&mut self, hyp: &Mealy, input_seq: &[InputSymbol]) -> CounterExample {
        let hyp_output = hyp.trace(input_seq).1.to_vec();
        let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(input_seq);
        (hyp_output != sut_output).then_some((input_seq.to_vec(), sut_output))
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        self.find_counterexample_with_lookahead(hypothesis, self.config.lookahead)
    }

    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        if hypothesis.states().len() == 1 {
            return Self::find_counterexample_initial(self, hypothesis);
        }
        let write_config = MealyWriter::WriteConfigBuilder::default()
            .format(MealyWriter::MealyEncoding::Soucha)
            .file_name(None)
            .build()
            .expect("Safe");
        let byte_hyp = MealyWriter::write_machine(
            &write_config,
            hypothesis,
            &self.rev_input_map,
            &self.rev_output_map,
        )
        .expect("Safe");
        let mut fsmlib_proc = self.soucha_process(lookahead);
        let child_stdin = fsmlib_proc.stdin.as_mut().unwrap();
        child_stdin
            .write_all(&byte_hyp)
            .expect("Failed to write hypothesis model to fsmlib program!");
        child_stdin
            .flush()
            .expect("Failed to write hypothesis model to fsmlib program!");

        let test_suite = {
            let proc_out = fsmlib_proc
                .wait_with_output()
                .expect("FSMLib did not exit!")
                .stdout;
            let strx = std::str::from_utf8(&proc_out)
                .expect("Parse error when reading tests from FSMLib.");
            Self::parse_tests(strx)
        };
        log::info!("Size of the test suite : {}", test_suite.len());

        test_suite
            .into_iter()
            .find_map(|seq| self.run_test(hypothesis, &seq))
    }
}
