use std::cell::RefCell;
use std::collections::BTreeMap;
use std::rc::Rc;

use chrono::{Duration, Utc};
use itertools::Itertools;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};
use rand_distr::{Bernoulli, Distribution, WeightedAliasIndex};
use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::access_seqs::{access_sequences, InputSelection, SearchStrategy};
use crate::util::sequences::{
    FixedInfixGenerator, LogExpSequences, Order, RandomAccessSequences, RandomInfixGenerator,
};
use crate::util::toolbox;

use super::generic::EOParams;
use super::EquivalenceOracle;
use super::{CounterExample, InfixStyle};

/// Generic factory Equivalence oracle.
///
/// This EO supports the W, Wp, HSI, and HADS oracles.
#[allow(clippy::module_name_repetitions)]
pub struct GenericEO<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    params: EOParams,
    rng: StdRng,
    logs: Vec<Vec<InputSymbol>>,
    cost_map: &'a FxHashMap<InputSymbol, usize>,
}

impl<'a, T> GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        params: EOParams,
        logs: Vec<Vec<InputSymbol>>,
        cost_map: &'a FxHashMap<InputSymbol, usize>,
    ) -> Self {
        let seed = params.seed;
        Self {
            oq_oracle,
            params,
            rng: StdRng::seed_from_u64(seed),
            logs,
            cost_map
        }
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn get_cost(&mut self) -> usize {
        RefCell::borrow_mut(&self.oq_oracle).get_cost()
    }

    #[must_use]
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        if hypothesis.states().len() == 1 {
            return self.find_counterexample_initial(hypothesis);
        }

        match self.params.infix_style {
            InfixStyle::Finite => self.run_finite_suite(hypothesis, lookahead),

            InfixStyle::Infinite => self.run_infinite_suite(hypothesis, lookahead),
        }
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        self.find_counterexample_with_lookahead(hypothesis, self.params.extra_states)
    }
}

impl<'a, T> GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn run_infinite_suite(&mut self, fsm: &Mealy, lookahead: usize) -> CounterExample {
        // let access_map = Self::random_access_map(fsm, self.params.seed);
        let access_map = access_sequences(
            fsm,
            InputSelection::rand_with_seed(self.rng.gen()),
            if self.params.use_dijkstra {SearchStrategy::Dijkstra} else {SearchStrategy::Bfs},  
            self.cost_map
        );
        let access_map = access_map.into_iter().collect();
        let mut acc_seq_gen = RandomAccessSequences::new(&access_map, self.params.seed);
        let mut rng: StdRng = SeedableRng::seed_from_u64(self.params.seed);
        let infix_picker = Bernoulli::new(0.5).expect("Safe");
        let log_rig = LogExpSequences::from_traces(
            &self.logs,
            self.params.seed,
            self.params.extra_states,
            self.params.expected_random_length,
        );
        let mut log_infix_gen = log_rig.into_iter();
        let sink_out = {
            let temp = RefCell::borrow(&self.oq_oracle);
            temp.pass_maps()
                .1
                .into_iter()
                .find(|(st, _)| st == "error")
                .map_or(OutputSymbol::new(u16::MAX), |x| x.1)
        };
        let rnd_infix_gen = RandomInfixGenerator::new(
            self.params.seed,
            fsm.input_alphabet().len(),
            lookahead,
            self.params.expected_random_length,
        );
        let mut infix_gen = rnd_infix_gen.into_iter();

        let char_map: BTreeMap<State, Vec<Vec<_>>> = self
            .params
            .char_style
            .characterisation_map(fsm)
            .into_iter()
            .map(|(k, v)| (k, v.into_iter().collect_vec()))
            .collect();
        let weight_map = sort_according_to_num_apart(fsm, &char_map);
        let start_time = Utc::now();
        let max_time = Duration::minutes(20);
        loop {
            let acc = acc_seq_gen.next().expect("Safe");
            let infix = if infix_picker.sample(&mut rng) {
                infix_gen.next().expect("Safe")
            } else {
                log_infix_gen.next().expect("Safe")
            };
            let prefix = toolbox::concat_slices(&[acc, &infix]);
            let useful_prefix = {
                let prefix_ce = self.run_test(fsm, &prefix);
                if prefix_ce.is_some() {
                    return prefix_ce;
                }
                let temp = RefCell::borrow(&self.oq_oracle);
                let o_tree = temp.borrow_tree();
                let mut useful_prefix = vec![];
                let mut curr = T::S::default();
                for i in &prefix {
                    let (out, dest) = o_tree.get_out_succ(curr, *i).expect("Safe");
                    if out == sink_out {
                        break;
                    }
                    curr = dest;
                    useful_prefix.push(*i);
                }
                useful_prefix
            };
            let state = fsm.trace(&useful_prefix).0;
            let dist_seq = {
                let idx = weight_map.get(&state).expect("Safe").sample(&mut self.rng);
                char_map.get(&state).expect("Safe").get(idx).expect("Safe")
            };
            let test = toolbox::concat_slices(&[&useful_prefix, dist_seq]);
            let ce = self.run_test(fsm, &test);
            if ce.is_some() {
                return ce;
            }
            let curr_time = Utc::now();
            let elapsed_time = curr_time - start_time;
            if elapsed_time > max_time {
                return None;
            }
        }
    }

    fn run_finite_suite(&mut self, fsm: &Mealy, lookahead: usize) -> CounterExample {
        let input_alphabet = fsm.input_alphabet();
        let hyp_states = fsm.states();
        let fig = FixedInfixGenerator::new(input_alphabet, lookahead, Order::ASC);
        let iter_0 = itertools::repeat_n(vec![], hyp_states.len());
        let infix_gen = fig.generate().chain(iter_0);
        // let access_map = Self::random_access_map(fsm, 0); // S -> Access Seq.
        let access_map = access_sequences(
            fsm,
            InputSelection::rand_with_seed(self.rng.gen()),
            if self.params.use_dijkstra {SearchStrategy::Dijkstra} else {SearchStrategy::Bfs},  
            self.cost_map
        );
        // let access_map = access_map.into_iter().collect();
        let access_seqs = access_map.values().cloned().collect_vec();
        let char_map = self.params.char_style.characterisation_map(fsm);
        for i in infix_gen {
            for a in access_seqs.clone() {
                let seq = toolbox::concat_slices(&[&a, &i]);
                let s = fsm.trace(&seq).0;
                let idents = char_map.get(&s).expect("Safe");
                for chars in idents {
                    let seq = toolbox::concat_slices(&[&a, &i, chars]);
                    let ce = self.run_test(fsm, &seq);
                    if ce.is_some() {
                        return ce;
                    }
                }
            }
        }
        None
    }

    /// Specialised case for when the hypothesis is a single-state FSM.
    #[must_use]
    fn find_counterexample_initial(&mut self, hyp: &Mealy) -> CounterExample {
        // When the hypothesis has size one, there's no point in testing A.C,
        // as we will have no characterising set.
        // A similar argument works for A.I.C, as C is empty.
        // A will contain only the empty sequence,
        // since we are already in the initial state;
        // Therefore, the only thing left is I<=(n+1)
        let input_size = hyp.input_alphabet().len();
        let input_alphabet = toolbox::inputs_iterator(input_size).collect_vec();
        match self.params.infix_style {
            InfixStyle::Finite => {
                let fig =
                    FixedInfixGenerator::new(input_alphabet, self.params.extra_states, Order::ASC);
                fig.generate().find_map(|seq| self.run_test(hyp, &seq))
            }
            InfixStyle::Infinite => {
                let rig = RandomInfixGenerator::new(
                    self.params.seed,
                    input_size,
                    self.params.extra_states,
                    self.params.expected_random_length,
                );
                rig.into_iter().find_map(|seq| self.run_test(hyp, &seq))
            }
        }
    }

    /// Run a single test sequence and return whether it is a CE or not.
    #[inline]
    #[must_use]
    fn run_test(&mut self, hyp: &Mealy, input_seq: &[InputSymbol]) -> CounterExample {
        let hyp_output = hyp.trace(input_seq).1.to_vec();
        let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(input_seq);
        if hyp_output == sut_output {
            return None;
        }
        Some((input_seq.to_vec(), sut_output))
    }
}

fn sort_according_to_num_apart(
    fsm: &Mealy,
    char_map: &BTreeMap<State, Vec<Vec<InputSymbol>>>,
) -> FxHashMap<State, WeightedAliasIndex<usize>> {
    let mut weight_state_map = FxHashMap::default();
    for s in fsm.states() {
        let other_states: Vec<_> = fsm.states().into_iter().filter(|x| *x != s).collect();
        let char_set_s = char_map.get(&s).expect("Safe");
        let mut seq_score_vec = char_set_s
            .iter()
            .map(|seq| {
                // For each sequence, how many states are apart?
                let s_resp = fsm.trace_from(s, seq).1;
                let apart_cnt = other_states
                    .iter()
                    .filter(|x| {
                        let x_resp = fsm.trace_from(**x, seq).1;
                        x_resp != s_resp
                    })
                    .count();
                (seq, apart_cnt)
            })
            .collect_vec();
        seq_score_vec.sort_unstable_by(|(_, a_apart), (_, b_apart)| b_apart.cmp(a_apart));
        let weights = seq_score_vec.iter().map(|x| x.1).collect();
        let weights = WeightedAliasIndex::new(weights).expect("Safe");
        weight_state_map.insert(s, weights);
    }
    weight_state_map
}
