//! Module containing functionality to generate characterisation sets.
//!
//! Different types of characterisation sets can be generated using the
//! functions in this module. Please check the sub-modules for more information.

mod basic;
mod w;
mod wp;

use std::collections::{BTreeMap, BTreeSet};

pub use self::basic::SeparatingSeqs;
pub use self::w::WMethod;
pub use self::wp::WpMethod;

use super::mealy::{InputSymbol, Mealy, State};

pub trait Characterisation {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>>;
}

#[cfg(test)]
mod tests {
    use std::collections::{BTreeMap, BTreeSet};
    use std::path::Path;

    use rstest::rstest;
    use rustc_hash::FxHashSet;

    use crate::definitions::characterisation::{Characterisation, WMethod, WpMethod};
    use crate::definitions::mealy::{InputSymbol, Mealy, State};
    use crate::definitions::FiniteStateMachine;
    use crate::oracles::equivalence::incomplete::hads::HADSMethod;
    use crate::oracles::equivalence::incomplete::hsi::HSIMethod;
    use crate::util::parsers::machine::read_mealy_from_file;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    /// Local state identifiers should be redundancy-free.
    #[rstest]
    #[case("tests/src_models/trial.dot", HSIMethod::characterisation_map)]
    #[case("tests/src_models/w_test.dot", HSIMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_23.dot", HSIMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_14.dot", HSIMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_21.dot", HSIMethod::characterisation_map)]
    #[case("tests/src_models/trial.dot", HADSMethod::characterisation_map)]
    #[case("tests/src_models/w_test.dot", HADSMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_23.dot", HADSMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_14.dot", HADSMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_21.dot", HADSMethod::characterisation_map)]
    #[case("tests/src_models/trial.dot", WMethod::characterisation_map)]
    #[case("tests/src_models/w_test.dot", WMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_23.dot", WMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_14.dot", WMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_21.dot", WMethod::characterisation_map)]
    #[case("tests/src_models/trial.dot", WpMethod::characterisation_map)]
    #[case("tests/src_models/w_test.dot", WpMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_23.dot", WpMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_14.dot", WpMethod::characterisation_map)]
    #[case("tests/src_models/hypothesis_21.dot", WpMethod::characterisation_map)]
    fn no_overlap(
        #[case] file_name: &str,
        #[case] style: fn(&Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>>,
    ) {
        let fsm = load_basic_fsm(file_name);
        let identifiers = style(&fsm);
        for s in fsm.states() {
            let mut other_states: FxHashSet<_> =
                fsm.states().into_iter().filter(|t| *t != s).collect();
            let local_identifiers = identifiers.get(&s).expect("Safe");
            for char_seq in local_identifiers {
                assert!(!other_states.is_empty());
                let s_resp = fsm.trace_from(s, char_seq).1;
                other_states.retain(|t| fsm.trace_from(*t, char_seq).1 == s_resp);
                if other_states.is_empty() {
                    break;
                }
            }
        }
    }
}
