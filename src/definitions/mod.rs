use rustc_hash::FxHashSet;

use crate::definitions::mealy::{InputSymbol, OutputSymbol, State};

pub mod characterisation;
pub mod mealy;
// pub mod splitting_tree;

/// Trait for interacting with an FSM during learning.
///
/// Modification of the FSM is illegal during the learning process.
pub trait FiniteStateMachine {
    /// Get states of the FSM
    fn states(&self) -> Vec<State>;
    /// Check if the FSM is complete.
    fn is_complete(&self) -> bool;
    /// Return a copy of the initial state of the FSM.
    fn initial_state(&self) -> State;
    /// Stateless single step from given state.
    fn step_from(&self, src: State, i: InputSymbol) -> (State, OutputSymbol);
    /// Stateless multi-step from given state.
    fn trace_from(&self, src: State, i_word: &[InputSymbol]) -> (State, Box<[OutputSymbol]>);
    /// Get destination state for input word.
    fn get_dest<T: IntoIterator<Item = InputSymbol>>(&self, src: State, input_word: T) -> State;
    /// Stateless multi-step from initial state.
    fn trace(&self, i_word: &[InputSymbol]) -> (State, Box<[OutputSymbol]>);
    /// Return a copy of the input alphabet of the FSM.
    fn input_alphabet(&self) -> Vec<InputSymbol>;
    /// Return a copy of the output alphabet of the FSM.
    fn output_alphabet(&self) -> FxHashSet<OutputSymbol>;
}
