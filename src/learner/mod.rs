//! Learner Module
//!
//! This module contains the implementations for the L# library,
//!  specifically the implementations for an observation tree,
//!  computing witnesses, and the learner itself.

/// Provide functions for computing witnesses.
pub mod apartness;
/// Main algorithm implementation.
pub mod l_sharp;
/// Vec-arena based implementation of an Observation Tree.
pub mod obs_tree;
