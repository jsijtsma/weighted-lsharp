//! Module containing implementation(s) for a System Under Learning (SUL).
//!

use std::collections::HashMap;
use std::hash::BuildHasher;
use std::io::{BufRead, BufReader, ErrorKind, Write};
use std::path::Path;
use std::process::{Child, ChildStderr, ChildStdin, ChildStdout, Command};

use bimap::BiHashMap;
use itertools::Itertools;
use rustc_hash::FxHashMap;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

/// System Under Learning, as a trait.
/// An SUL needs to implement this trait. You can see [`Simulator`](self::Simulator)
/// or [`Stdio`](self::Stdio) for an example of the same.
pub trait SystemUnderLearning {
    /// Return the output sequence of the provided input sequence.
    #[must_use]
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]>;

    /// Reset the SUL to its initial state.
    fn reset(&mut self);

    /// A [`reset`](SystemUnderLearning::reset) and a [`step`](SystemUnderLearning::step).
    #[must_use]
    fn trace(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        self.reset();
        self.step(input_seq)
    }

    /// Get the number of inputs and resets sent to the SUL thus far, **and reset the counters**.
    #[must_use]
    fn get_counts(&mut self) -> (usize, usize);

    fn get_cost(&mut self) -> usize;

    /// Get a ref to the input bimap of the SUL.
    #[must_use]
    fn input_map(&self) -> Vec<(String, InputSymbol)>;

    /// Get a ref to the input bimap of the SUL.
    #[must_use]
    fn output_map(&self) -> Vec<(String, OutputSymbol)>;
}

/// Simulate an SUL.
///
/// This struct uses an FSM to "simulate" the behaviour of a real SUL.
#[derive(Debug)]
pub struct Simulator<'a, S> {
    fsm: &'a Mealy,
    initial_state: State,
    curr_state: State,
    input_map: BiHashMap<String, InputSymbol, S, S>,
    output_map: BiHashMap<String, OutputSymbol, S, S>,
    cost_map: FxHashMap<InputSymbol, usize>,
    reset_cost: usize,
    cnt_inputs: usize,
    cnt_resets: usize,
    cnt_cost: usize,
}

impl<'a, S: BuildHasher + Default> Simulator<'a, S> {
    /// Constructor.
    #[must_use]
    pub fn new(
        fsm: &'a Mealy,
        input_map: &HashMap<String, InputSymbol, S>,
        output_map: &HashMap<String, OutputSymbol, S>,
        cost_map: &FxHashMap<InputSymbol, usize>,
        reset_cost: usize
    ) -> Self {
        let i_m = input_map.iter().map(|(s, i)| (s.clone(), *i)).collect();
        let o_m = output_map.iter().map(|(s, o)| (s.clone(), *o)).collect();
        let c_m = cost_map.iter().map(|(s, c)| (s.clone(), *c)).collect();
        Self {
            initial_state: fsm.initial_state(),
            curr_state: fsm.initial_state(),
            fsm,
            input_map: i_m,
            output_map: o_m,
            cost_map: c_m,
            reset_cost,
            cnt_inputs: 0,
            cnt_resets: 0,
            cnt_cost: 0,

        }
    }
}

impl<'a, S: BuildHasher> SystemUnderLearning for Simulator<'a, S> {


    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let (dest, out_seq) = self.fsm.trace_from(self.curr_state, input_seq);

        //let i_s = input_seq
        //    .iter()
        //    .map(|i| self.input_map.get_by_right(i).expect("Safe"))
        //    .collect_vec();
        for i in input_seq {
            self.cnt_cost += self.cost_map[i];
            self.cnt_inputs += 1;
        }

        // okay normal programming resumes    
        //self.cnt_inputs += input_seq.len(); 
        self.curr_state = dest;
        log::trace!(" {:?} / {:?}", input_seq, out_seq);
        out_seq
    }

    fn reset(&mut self) {
        // added for testing purposes pls dont arrest me
        
        self.cnt_cost += self.reset_cost;
        self.cnt_resets += 1;
        // okay normal programming resumes
        self.curr_state = self.initial_state;
        //println!("[Resetted]");
        log::trace!("\tRESET");
    }

    fn get_counts(&mut self) -> (usize, usize) {
        let ret = (self.cnt_inputs, self.cnt_resets);
        self.cnt_inputs = 0;
        self.cnt_resets = 0;
        ret
    }

    fn get_cost(&mut self) -> usize {
        let ret = self.cnt_cost;
        self.cnt_cost = 0;
        ret
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.input_map
            .iter()
            .map(|(s, i)| (s.clone(), *i))
            .collect()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.output_map
            .iter()
            .map(|(s, o)| (s.clone(), *o))
            .collect()
    }
}

/// An external SUL struct interacting with the actual SUL over stdio.
///
/// ### NOTE
/// We initialise the child process during the construction of the struct
/// itself.
pub struct Stdio<S> {
    cnt_inputs: usize,
    cnt_resets: usize,
    input_map: BiHashMap<String, InputSymbol, S, S>,
    output_map: BiHashMap<String, OutputSymbol, S, S>,
    #[allow(dead_code)]
    child: Child,
    sul_stdin: ChildStdin,
    sul_stdout: BufReader<ChildStdout>,
    reset_code: String,
    #[allow(dead_code)]
    sul_stderr: ChildStderr,
}

impl<S: BuildHasher + Default> Stdio<S> {
    /// Initialise the struct.
    ///
    /// ### Errors
    /// If the SUL could not be initialised: the path is incorrect; or the input, output, or error streams
    /// could not be obtained.
    pub fn new(
        loc: &dyn AsRef<Path>,
        input_map: &HashMap<String, InputSymbol, S>,
        reset_code: String,
    ) -> std::io::Result<Self> {
        let i_m = input_map.iter().map(|(s, i)| (s.clone(), *i)).collect();
        let canon_loc = std::fs::canonicalize(loc)?;
        let mut child = Command::new(canon_loc)
            .stdin(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;
        let stderr_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stderr.");
        let stdout_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stdout.");
        let stdin_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stdin.");
        let sul_stdin = child.stdin.take().ok_or(stdin_err)?;
        let sul_stdout = BufReader::new(child.stdout.take().ok_or(stdout_err)?);
        let sul_stderr = child.stderr.take().ok_or(stderr_err)?;

        Ok(Self {
            cnt_inputs: 0,
            cnt_resets: 0,
            input_map: i_m,
            output_map: BiHashMap::default(),
            child,
            sul_stdin,
            sul_stdout,
            sul_stderr,
            reset_code,
        })
    }
}

impl<S: BuildHasher> SystemUnderLearning for Stdio<S> {
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let mut o_buf = String::new();
        let mut o_s = Vec::with_capacity(input_seq.len());
        let i_s = input_seq
            .iter()
            .map(|i| self.input_map.get_by_right(i).expect("Safe"))
            .collect_vec();
        for i in i_s {
            o_buf.clear();
            self.sul_stdin
                .write_all(i.as_bytes())
                .expect("Writing input to SUL failed.");
            self.sul_stdin
                .write_all("\n".as_bytes())
                .expect("Writing input newline to SUL failed.");
            self.sul_stdout
                .read_line(&mut o_buf)
                .expect("Could not read from SUL.");
            o_buf = o_buf.trim().to_string();
            let mut osymb = self.output_map.get_by_left(&o_buf).copied();
            if osymb.is_none() {
                #[allow(clippy::cast_possible_truncation)]
                self.output_map.insert(
                    o_buf.clone(),
                    OutputSymbol::new(self.output_map.len() as u16),
                );
                osymb = self.output_map.get_by_left(&o_buf).copied();
            }
            o_s.push(osymb.expect("Safe"));
        }
        self.cnt_inputs += input_seq.len();
        log::trace!(" {:?} / {:?}", input_seq, o_s);
        o_s.into_boxed_slice()
    }

    fn reset(&mut self) {
        self.cnt_resets += 1;
        self.sul_stdin
            .write_all(self.reset_code.as_bytes())
            .expect("Writing reset to SUL failed.");
        self.sul_stdin
            .write_all("\n".as_bytes())
            .expect("Writing reset newline to SUL failed.");
    }

    fn get_counts(&mut self) -> (usize, usize) {
        let ret = (self.cnt_inputs, self.cnt_resets);
        self.cnt_inputs = 0;
        self.cnt_resets = 0;
        ret
    }

    fn get_cost(&mut self) -> usize {
        0
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.input_map
            .iter()
            .map(|(s, i)| (s.clone(), *i))
            .collect()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.output_map
            .iter()
            .map(|(s, o)| (s.clone(), *o))
            .collect()
    }
}

/// A wrapper over an SUL for Sink-State Optimisation.
///
/// Any struct implementing [`SystemUnderLearning`] can be supplied to
/// this struct.
pub struct SinkWrapper<S> {
    sul: S,
    sink_output: OutputSymbol,
    in_error: bool,
}

impl<S: SystemUnderLearning> SinkWrapper<S> {
    pub fn new(sul: S, sink_output: OutputSymbol) -> Self {
        Self {
            sul,
            sink_output,
            in_error: false,
        }
    }
}

impl<S: SystemUnderLearning> SystemUnderLearning for SinkWrapper<S> {
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let mut ret = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            if self.in_error {
                ret.push(self.sink_output);
                continue;
            }
            let o = self.sul.step(&[i])[0];
            ret.push(o);

            if o == self.sink_output {
                self.in_error = true;
            }
        }
        ret.into_boxed_slice()
    }

    fn reset(&mut self) {
        self.in_error = false;
        self.sul.reset();
    }

    fn get_counts(&mut self) -> (usize, usize) {
        self.sul.get_counts()
    }

    fn get_cost(&mut self) -> usize {
        self.sul.get_cost()
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.sul.input_map()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.sul.output_map()
    }
}
