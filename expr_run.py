from concurrent.futures import ProcessPoolExecutor
import os
from os.path import exists
import sys
import datetime
import re
import subprocess
from time import sleep
import random
import json

experiments_total = 0
experiments_skipped = 0

seeds = [
    81,
    100,
    158,
    216,
    245,
    359,
    366,
    470,
    560,
    578,
    580,
    597,
    661,
    689,
    692,
    783,
    818,
    879,
    930,
    968,
    995,
    1004,
    1005,
    1190,
    1205,
    1257,
    1320,
    1534,
    1541,
    1596,
    1607,
    1665,
    1836,
    1989,
    2015,
    2143,
    2147,
    2199,
    2221,
    2263,
    2283,
    2365,
    2370,
    2408,
    2495,
    2528,
    2554,
    2558,
    2561,
    2588,
    2610,
    # 2619,
    # 2679,
    # 2773,
    # 2816,
    # 2950,
    # 2966,
    # 2969,
    # 2983,
    # 3044,
    # 3101,
    # 3131,
    # 3147,
    # 3169,
    # 3209,
    # 3211,
    # 3213,
    # 3235,
    # 3265,
    # 3350,
    # 3383,
    # 3415,
    # 3444,
    # 3496,
    # 3528,
    # 3588,
    # 3658,
    # 3743,
    # 3769,
    # 3806,
    # 3809,
    # 3900,
    # 3980,
    # 4094,
    # 4179,
    # 4358,
    # 4370,
    # 4447,
    # 4467,
    # 4535,
    # 4550,
    # 4588,
    # 4632,
    # 4646,
    # 4689,
    # 4782,
    # 4845,
    # 4948,
    # 4950,
    # 4973,
]


def get_lsharp_command(name, oracle, seed, es, rnd_len, dijkstra, infix_gen, piw_listsize, piw_sortstyle, budget, ratio, pre, cw, weight_file, extra, result_filename, hypothesis_only):
    # when adding new parameters to test, remember to not only add them to the generated command here, but also to add the parameter to the output result file name in some form so you dont overwrite what you actually want to test. 
    # additionaly, add it to the header string in analysis.py 's combine_resultfiles()
    org_dir = "experiment_models/"
    model = org_dir + name
    # log = org_dir + name + ".traces"
    returnlist = [
        "target/release/lsharp",
        "-e",
        oracle,
        "--rule2",
        "ads",
        "--rule3",
        "ads",
        "-k",
        str(es),
        "-l",
        str(rnd_len),
        "-x",
        str(seed),
        "--eq-mode",
        "infinite",
        "-i",
        infix_gen,
        # "--traces",
        # log,
        "-m",
        model,
        "-v",
        "1",
        "--out",
        result_filename, #"csvs/results" + name + str(seed) + oracle + dijkstra + "_" + infix_gen + "_n" + str(piw_listsize) + piw_sortstyle + "pt" + str(pre) + cw + "ratio_" + str(ratio) + "budg" + str(budget) + extra + ".csv",
        #"--compress-tree",
        "-q",
        "-w",
        weight_file,
        "-n",
        str(piw_listsize),
        "-s",
        piw_sortstyle,
        "-b",
        str(pre),
        "-r",
        str(ratio),
        "-u",
        str(budget)
    ]
    if len(dijkstra)>0 :
        returnlist.append("-d")
    if len(cw)>0:
        returnlist.append("-c")
    if hypothesis_only:
        returnlist.append("-h")
        returnlist.append("hypotheses/" + name[:-4] + "_hypo.dot")
    return returnlist


def result_filename(model, oracle, seed, es, rnd_len, dijkstra, infix_gen, piw_listsize, piw_sortstyle, budget, ratio, pre, cw, w, extra, experiment_name, hypothesis_only, pre_defined_weights):
    filename = "expr_out/" + str(experiment_name) + "/results"
    if hypothesis_only:
        filename = filename + "_hypothesis"
    if pre_defined_weights:
        filename = filename + "_predefweights"
    filename = filename + "/" + model + str(seed) + "_w" + str(w) + oracle + dijkstra + "_" + infix_gen + "_n" + str(piw_listsize) + piw_sortstyle + "pt" + str(pre) + cw + "ratio_" + str(ratio) + "budg" + str(budget) + extra + ".csv"
    return filename

def weightfile_filename(model, experiment_name, exp_cost, n):
    weightfile = "expr_out/" + str(experiment_name) + "/weights/" + model + "_cost" + str(exp_cost) + "_weightfile" + str(n) + ".txt"
    return weightfile

def run_expr(cmd):
    print("Starting experiment")
    pr = subprocess.Popen(cmd)
    if pr.wait() != 0:
        Exception("Experiment failed!")
        exit(-1)
    return

def input_list(file):
    with open(file, 'r') as f:
        actions = []
        for line in f:
            if "->" in line:
                regex = r".*\"(.*)/.*"
                action = re.findall(regex, line)
                if action:
                    if action[0] not in actions:
                        actions.append(action[0])
        return actions
        
def output_list(file):
    with open(file, 'r') as f:
        outputs = []
        for line in f:
            if "->" in line:
                regex = r".*/(.*)\".*"
                output = re.findall(regex, line)
                if output:
                    if output[0] not in outputs:
                        outputs.append(output[0])
        return outputs
        
    
def generate_random_weight_file(actions, filename, expensive_cost):
    leng = (len(actions)//2)+1
    ub = leng
    lb = 2 if leng>2 else 1
    expensive_actions = random.sample(actions, random.randrange(lb, ub))

    #print(len(actions), len(expensive_actions))
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w' ) as f:
        for a in expensive_actions:
            f.write(a + ":" + str(expensive_cost) + "\n")

def current_moment_string():
    return datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S_%f')


def result_exists(file_name):
    if not exists(file_name):
        return False
    else:
        print("file found.")
        with open(file_name, 'r') as f:
            lines_count = len(f.readlines())
            return lines_count==2    


def generate_cmds(models_file, seeds_n, weightfiles_n, experiment_name, expensive_cost, hypothesis_only, pre_defined_weights):
    global experiments_total, experiments_skipped
    exp_models = ["example.dot"]

    with open(models_file) as mf:
        exp_models = mf.read().splitlines()
    
    weight_files = ["example.txt"]

    if pre_defined_weights:
        with open(models_file[:-4] + "_weightfiles.txt") as mf:
            weight_files = mf.read().splitlines()



    # oracles = ["hads-int", "iads"]
    oracles = ["hads-int"]#, "hsi", "w", "wp"]
    # oracles = ["iads"]
    # oracles = ["soucha-spy", "soucha-spyh"]
    # oracles = ["soucha-spy"]

    sds = seeds[:seeds_n]
    all_results = []
    cmds = []
    es = [3]#[1, 2]
    rnd_lens = [2]#[2, 3]
    dijk = [""]#["", "-d"]#,"-d"]#["", "-d"]
    infixers = ['random']#['random', 'weighted']#, 'weighted']#["random", "weighted"]#, "inverted"]
    cheap_ws = [""]#["", "-c"]
    ratios = [0]#[2, 5, 100]
    budgets = [0]
    piw_listsizes = [1]#[1, 2, 4, 8, 12, 16, 20, 24, 32, 64]

    #ws = ["weights/custom/weightsNeutral.txt"]#[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]#[ "weights/custom/IdealOpenSSH.txt"]#,"weights/custom/weightsneutral.txt"
    ws = range(weightfiles_n)
    if pre_defined_weights:
        ws = [0]
    pre_input = [0]#[0, 10, 50, 100]
    pre = 0
    sort_styles = ["cost"]#, "cost-reversed", "length", "cost-per-length", "length-per-cost"]#, "length"]
    expensive_costs = [expensive_cost]
    extra = ""

    #the order of these fors does actually matter in some cases. For example, it might be needed to create a single random weight file that is used with different parameters.

    print("Generating commands: " +  str(len(exp_models)) + " models, " + str(seeds_n) + " seeds, " + str(weightfiles_n) + " weightfiles...")

    for k in es:
        for rnd_len in rnd_lens:
            for i, model in enumerate(exp_models):
                modelname = model[:-4] #remove ".dot"
                for exp_cost in expensive_costs:
                    for w in ws:
                        model_file = "experiment_models/" + model
                        weightfile = weightfile_filename(modelname, experiment_name, exp_cost, w)

                        if pre_defined_weights:
                            weightfile = weight_files[i]
                        
                        if not exists(weightfile):
                            if pre_defined_weights:
                                print("Could not find weightfile: " + weightfile)
                            else:
                                actions = input_list(model_file)
                                generate_random_weight_file(actions, weightfile, exp_cost)

                        with open("expr_configs/" + str(experiment_name) + ".json", 'r') as config_file:
                            configs = json.load(config_file)

                            for config in configs['configs']:
                                oracle = config['oracle']
                                d = config['p']
                                i = config['i']
                                cw = config['w']
                                ratio = config['ratio']
                                budget = config['budget']
                                n = config['buffersize']
                                s = config['sort_style']

                                for seed in sds:
                                    result_file = result_filename(modelname, oracle, seed, k, rnd_len, d, i, n, s, budget, ratio, pre, cw, w, extra, experiment_name, hypothesis_only, pre_defined_weights)
                                    experiments_total += 1                        
                                    if not result_exists(result_file):
                                        os.makedirs(os.path.dirname(result_file), exist_ok=True)
                                        
                                        cmd = get_lsharp_command(model, oracle, seed, k, rnd_len, d, i, n, s, budget, ratio, pre, cw, weightfile, extra, result_file, hypothesis_only)
                                        cmds.append(cmd)
                                    else:
                                        print("Skipping experiment, " + result_file + " already exists.")
                                        experiments_skipped += 1

    return cmds






if __name__ == "__main__":
    experiment_name = sys.argv[1].strip()
    models_file = sys.argv[2].strip()
    seeds_n = int(sys.argv[3])
    weightfiles_n = int(sys.argv[4])
    expensive_cost = sys.argv[5].strip()
    hypothesis_only = sys.argv[6].strip() == 'hypothesis'
    pre_defined_weights = weightfiles_n==0

    cmds = generate_cmds(models_file, seeds_n, weightfiles_n, experiment_name, expensive_cost, hypothesis_only, pre_defined_weights)

    commands_created = len(cmds)

    with ProcessPoolExecutor(max_workers=1) as executor:
        executor.map(run_expr, cmds)

    print("\nOf a total of " + str(experiments_total) + " experiments, " + str(experiments_skipped) + " were skipped and " + str(commands_created) + " were generated.")
        