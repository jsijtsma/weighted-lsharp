#!/bin/bash
python3 expr_run.py BFSvsDIJKSTRA medium_models.txt $1 $2 10 non
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP medium_models.txt $1 $2 10 non
python3 expr_run.py CHEAPPIWvsNORMAL medium_models.txt $1 $2 10 non
python3 expr_run.py RANDOMvsWEIGHTED medium_models.txt $1 $2 10 non
python3 expr_run.py SORTvsNORMAL medium_models.txt $1 $2 10 non 
python3 expr_run.py BFSvsDIJKSTRA medium_models.txt $1 $2 10 hypothesis
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP medium_models.txt $1 $2 10 hypothesis
python3 expr_run.py CHEAPPIWvsNORMAL medium_models.txt $1 $2 10 hypothesis
python3 expr_run.py RANDOMvsWEIGHTED medium_models.txt $1 $2 10 hypothesis
python3 expr_run.py SORTvsNORMAL medium_models.txt $1 $2 10 hypothesis 
python3 combine_results.py BFSvsDIJKSTRA
python3 combine_results.py CARTHESIANDIJKSTRAvsHADSINTvsWP
python3 combine_results.py CHEAPPIWvsNORMAL
python3 combine_results.py RANDOMvsWEIGHTED
python3 combine_results.py SORTvsNORMAL