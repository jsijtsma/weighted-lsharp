#!/bin/bash
python3 expr_run.py test small_models.txt $1 $2 10 non
python3 expr_run.py BFSvsDIJKSTRA small_models.txt $1 $2 10 non
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP small_models.txt $1 $2 10 non
python3 expr_run.py CHEAPPIWvsNORMAL small_models.txt $1 $2 10 non
python3 expr_run.py RANDOMvsWEIGHTED small_models.txt $1 $2 10 non
python3 expr_run.py SORTvsNORMAL small_models.txt $1 $2 10 non
python3 expr_run.py test small_models.txt $1 $2 10 hypothesis
python3 expr_run.py BFSvsDIJKSTRA small_models.txt $1 $2 10 hypothesis
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP small_models.txt $1 $2 10 hypothesis
python3 expr_run.py CHEAPPIWvsNORMAL small_models.txt $1 $2 10 hypothesis
python3 expr_run.py RANDOMvsWEIGHTED small_models.txt $1 $2 10 hypothesis
python3 expr_run.py SORTvsNORMAL small_models.txt $1 $2 10 hypothesis
python3 expr_run.py BFSvsDIJKSTRA weighted_models.txt $1 0 10 non
python3 expr_run.py CARTHESIANDIJKSTRAvsHADSINTvsWP weighted_models.txt $1 0 10 non
python3 expr_run.py CHEAPPIWvsNORMAL weighted_models.txt $1 0 10 non
python3 expr_run.py RANDOMvsWEIGHTED weighted_models.txt $1 0 10 non
python3 expr_run.py SORTvsNORMAL weighted_models.txt $1 0 10 non 
python3 combine_results.py test
python3 combine_results.py BFSvsDIJKSTRA
python3 combine_results.py CARTHESIANDIJKSTRAvsHADSINTvsWP
python3 combine_results.py CHEAPPIWvsNORMAL
python3 combine_results.py RANDOMvsWEIGHTED
python3 combine_results.py SORTvsNORMAL