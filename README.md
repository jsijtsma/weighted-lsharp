This is an (outdated) fork of the [LSharp Learning Library.](https://gitlab.science.ru.nl/sws/lsharp)
For my master thesis I implemented the possibility to use weights for input symbols, as well as measures that could reduce the cost of learning a model.

### Using Weightfiles
With the additional argument `-w [WeightFileName.txt]` you can assign a weightfile that conatins how expensive each input symbol will be.
In this file, each line contains one inputsymbol-cost pair, split by the `:` character.
For example `i2:2` means that input `i2` will have a weight of `2`. 
Inputs not in the file will default to a value of one.
The reset could be given a value too, with `RESET:[cost]`.
Examples can be found [here.](expr_configs/weights)

### Using Other Measures
The following is a list of the measures and optional arguments to use them.
In general, when the argument is not used, then the code defaults to base L# behaviour. 
These measures are only implemented for "infinite mode". 
Additionally the `-r` argument only works when the `-n` argument is above 1.

 - `-w [filename]`, to assign a weightfile.
 - `-n [integer]`, to define how large the list of pre-sorted test sequences will be. A value of `1` is base L# behaviour. Defaults to `1`.
 - `-s [sortstyle]`, to define how that list is sorted. Defaults to `cost`. Other valid values are `costreversed`, `length`, `lengthreversed`, `costperlength`, `occurence`, `occurencereversed`, `costperoccurence`, `occurencepercost`, `presence`.
 - `-b [integer]`, **unfinished** implementation of the 'pre input' measure. This determines the maximum cost allowed for inputting sequences before the normal learning algorithm starts (minus the cost of one last reset). Defaults to `0`. **mostly untested**.
 - `-u [integer]`, determines the maximum budget for one test learning loop. Exits the counterexample finding loop immediatly after this budget is exceeded. `0` is infinite. Defaults to `999999`.
 - `-d`, defines how the p is generated in the piw, when supplied it uses Dijkstra, otherwise BFS.
 - `-i [style]`,  defines the way the i is generated in the piw. `style` is either `weighted` or `random`.
 - `-c`, when supplied the w is generated with Cartesian-Dijkstra.
 - `-r [0-100]`, **unfinished** implementation for a 'ratio' measure. With `0` only new sequences are sent to the SUL, with `100` only base L# sequences, with `50` being an even split. Only implemented for `-n > 1`.  
 - `-h [HypothesisFileName.dot]`, when supplied does not use the normal L# algorithm, but only tries to find a single counterexample. This counterexample is not outputted, but a `results.csv` is still generated.

### Other Scripts
The `process_log.py` script was used to make log files easier to read and compare, by stripping time information adn replacing 'InputSymbol(1)' with the real input symbol as defined in the models' `.dot` file.

The `expr_run.py` script can be used to run the experiments. 
Settings for each experiment are in `.json` files in the [](expr_configs) directory. 
The experiments were split in three based on model size. 
The experiments where started with the `small_expr.sh`, `medium_expr.sh` and `large_expr.sh` shell scripts in a docker container created with the `Dockerfile`.

Results of these experiments can then be combined in single files with the `combine_results.py` script.
The `combine.sh` script was used to combine all experiments in one go (one file per experiment).

The graphs of the thesis were generated with the `graph_making.py` script. 
The output of this script can be found in the [](graphs) directory, as well as the raw data for the graphs with the averages. 

 
## Code Changes

Added `weights.rs`
 - Mostly contains utility function for use in the rest of the project. This includes:
 - Functions for reading weights from a weightfile
 - Functions to calculate cost of a sequence
 - The Dijkstra functions that finds the cheapest access sequence
 - Function for the unused 'pre_input' measure. 
 
`main.rs`
 - Added variables for the new commandline arguments, the values for these arguments is added to the output file as well. 
 - Added a if-branche for the single counterexample experiment.

 
`l_sharp.rs` and `sul.rs`
 - Added a get_cost function, SUL also keeps track of the incurred cost

 
`learning.rs`
 - Changes here are mostly for the 'pre_input' cost saving measure, that did not make it the final thesis, and for the single counterexample experiments. (learn_fsm() and find_counterexample() respectively)
 
`cli.rs`
 - Added new commandline arguments as described above.
 
`learning_config.rs`
 - Added new commandline arguments, to input and output.
 
`generic.rs`
 - Used to have one function run_infinite_suite, this one was split into run_infinite_suite_single, with minimal changes and run_infinite_suit_presorted with major changes for the sorting methods.
 - Added enum for sorting methods
 - Added functions for sorting the test sequence list.

`sequences.rs`
 - added WeightedInfixGenerator






Original Text Below:

## L<sup>#</sup> Learning Library


### Note
[LearnLib](https://learnlib.de) is **not** a part of our tool,
it is included as a sub-module purely in order to make it easier
to run benchmarks against it.

## Requirements

This tool is meant to be run **only on 64-bit** systems:
running it on 32-bit systems is **undefined behaviour**.

## Building

Clone the repository; you will also need the rust toolchain installed.
If you wish to use [FSMLib's](https://github.com/Soucha/FSMlib) equivalence oracles (SPY and SPYH), you'll also need to download and build that.

You can build our learning library using ```cargo build``` to build the dev version of the same. 
Add the ```--release``` flag for the optimised release build, although this takes much longer to compile.


<!-- 
After cloning, you must build hybrid-ads (in the hybrid-ads directory), see [here](https://gitlab.science.ru.nl/moerman/hybrid-ads) for build instructions.
Next, you can build learnlib (in the learnlib directory) using the command ```maven compile assembly:single``` to get the jar file.
Finally, 
-->

## Running Experiments

In order to get help text, you can run ```cargo run -- -h```.
Please compile the program in release mode to get best (reasonable) performance, 
debug mode is quite slow.
In order to use FSMLib's [^1] equivalence oracles, you'll need to compile and provide the path to them yourself! [^2].

To learn the "example.dot" model, you can run the following command from the base directory.
Note: all arguments are mandatory. We do no use any default arguments.
```bash
cargo run -r --bin lsharp -- -e hads-int --rule2 ads --rule3 ads -k 3 -l 1 -m example.dot \
--eq-mode infinite --out results.csv -x 0 -v 0 
```

Results will be written to a file called "results.csv" in the same directory.

### Equivalence oracles 

The following equivalence oracles are valid:
1. Hybrid-ADS `hads-int`
2. W-method `w`
3. Wp-method `wp`
4. HSI `hsi`
5. IADS `iads`
6. FSMLib SPY and SPYH `soucha-spy` (`soucha-spyh`) 

When running equivalence oracles in infinite mode (`eq-mode` is set to `infinite`), the expected random length (argument `l`) must be greater than 0.

### Models
The models from AutomataWiki can be found in the `experiment_models` directory,
 while ASML models can be found in the `asml_rers_models` directory.
 ESM models are in the `esm_models` directory.
 Note, as yet, we can only learn the AutomataWiki models easily, the other two are difficult to learn, 
 due to equivalence checking.

You can provide logs (with the argument `traces`), and if those are provided, you can also use the `logged-hads` equivalence oracle.
Currently, we only have logs for the ASML models.

[^1]: Called Soucha in the code.
[^2]: The path can be provided using variable `soucha_config` in file `learning.rs`.