import os
import sys


def combine_resultfiles(directory, final_filename):
    topline = "name,learned,rounds,num_states,num_inputs,pre_inputs,pre_resets,pre_cost,learn_inputs,learn_resets,learn_cost,test_inputs,test_resets,test_cost,ads_score,cost,used_dijkstra,infix_gen,piw_listsize,sort_style,cheap_w,budget,ratio,pre_input,weightfile,seed,learning_algorithm,oracle,filename\n"
    if os.path.exists(directory):
        filelist = os.listdir(directory)
        if len(filelist)>0:
            o = open(final_filename, "w")
            o.write(topline)
                
            
            for filename in filelist:
                f = open(directory+"/"+filename,"r")
                f.readline() # read the first line (column names)
                line=f.readline().strip() # save results
                if "wp" in filename:
                    line = line + ",wp"
                else:
                    line = line + ",hads-int"
                line = line + "," + str(filename)
                o.write(line+"\n") # write the results to combined file
                f.close()
            o.close()
            print("The results from \"" + directory + "\" are now combined in \"" + final_filename + "\"")
        else:
            print(directory + "  is empty. Skipping...")
    else:
        print(directory + "  does not exist. Skipping...")


if __name__ == "__main__":
    
    experiment_name = sys.argv[1].strip()
    directory = "expr_out/" + experiment_name + "/results"
    hypo_directory = "expr_out/" + experiment_name + "/results_hypothesis"
    predef_directory = "expr_out/" + experiment_name + "/results_predefweights"
    predefhypo_directory = "expr_out/" + experiment_name + "/results_hypothesis_predefweights"
    combine_resultfiles(directory, "expr_out/completed/" + experiment_name + "_results.csv")
    combine_resultfiles(hypo_directory, "expr_out/completed/" + experiment_name + "_hypothesis_results.csv")
    combine_resultfiles(predef_directory, "expr_out/completed/" + experiment_name + "_predefweights_results.csv")
    combine_resultfiles(predefhypo_directory, "expr_out/completed/" + experiment_name + "_hypothesis_predefweights_results.csv")