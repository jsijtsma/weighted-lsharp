import sys
import os
import re

def input_list(file):
    with open(file) as f:
        actions = []
        for line in f:
            if "->" in line:
                regex = r".*\"(.*)/.*"      #should capture the input from a 
                action = re.findall(regex, line)
                if action:
                    if action[0] not in actions:
                        actions.append(action[0])
        return actions
        
def output_list(file):
    with open(file) as f:
        outputs = []
        for line in f:
            if "->" in line:
                regex = r".*/(.*)\".*"
                output = re.findall(regex, line)
                if output:
                    if output[0] not in outputs:
                        outputs.append(output[0])
        return outputs
    

def rewrite_log(logpath, model, outputfile):
    modelpath = "experiment_models/" + model + ".dot"
    inputs = input_list(modelpath)
    outputs = output_list(modelpath)
    outputstr = ""
    infostr = ""
    with open(logpath, 'r') as log:
        with open(outputfile, 'w') as newlog:
            for entry in log:
                l = re.findall(r".*(Applying rule [1234])", entry)
                if l:
                    outputstr += l[0] + "\n"
                l = re.findall(r".*(RESET)", entry)
                if l:
                    outputstr += l[0] + "\n"
                l = re.findall(r".*\[InputSymbol\(([0-9]*)\)\] / \[OutputSymbol\(([0-9]*)\)\].*", entry)
                if l:
                    inputindex = int(l[0][0])
                    outputindex = int(l[0][1])
                    outputstr += "[ " + inputs[inputindex] + " / " + outputs[outputindex] + " ]\n"
                l = re.findall(r".*([ME]Q .*)", entry)
                if l:
                    infostr += l[0] + "\n"
                l = re.findall(r".*(Total Cost.*)", entry)
                if l:
                    infostr = l[0] + "\n" + infostr
                l = re.findall(r".*(Rounds.*)", entry)
                if l:
                    infostr += l[0] + "\n"
            infostr = "Learning " + modelpath + "\n" + infostr
            newlog.write(infostr)
            newlog.write(outputstr)

def replace_inputsymbol(line, inputs):
    newline = line
    for i, symbol in enumerate(inputs):
        newline = newline.replace("InputSymbol("+str(i)+")", symbol)
    return newline

def replace_outputsymbol(line, outputs):
    newline = line
    for o, symbol in enumerate(outputs):
        newline = newline.replace("OutputSymbol("+str(o)+")", symbol)
    return newline


def undate_log(logpath, model, outputfile):
    modelpath = "experiment_models/" + model + ".dot"
    inputs = input_list(modelpath)
    outputs = output_list(modelpath)
    outputstr = ""
    infostr = ""
    with open(logpath, 'r') as log:
        with open(outputfile, 'w') as newlog:
            for entry in log:
                str = entry[20:]
                str = replace_inputsymbol(str, inputs)
                str = replace_outputsymbol(str, outputs)
                newlog.write(str)



def undate_directory(directory, model):
    for filename in os.listdir(directory):
        print("Processing " + filename + " ...")
        file = directory+"/"+filename
        undate_log(file, model, directory+"/processed"+filename)


def test_replace():
    modelpath = "experiment_models/" + "DropBear" + ".dot"
    line = r"""INFO lsharp | Input Map{"KEXINIT_PROCEED": InputSymbol(9), "CH_CLOSE": InputSymbol(10), "CH_OPEN": InputSymbol(6), "KEXINIT": InputSymbol(11), "UA_PK_NOK": InputSymbol(0), "CH_DATA": InputSymbol(7), "NEWKEYS": InputSymbol(4), "CH_EOF": InputSymbol(12), "SERVICE_REQUEST_AUTH": InputSymbol(2), "KEX30": InputSymbol(1), "SERVICE_REQUEST_CONN": InputSymbol(3), "CH_REQUEST_PTY": InputSymbol(5), "UA_PK_OK": InputSymbol(8)}"""
    print(line)
    inputs = input_list(modelpath)
    outputs = output_list(modelpath)
    print(replace_inputsymbol(line, inputs))






#rewrite_log("2023-06-22_09_22_58_615196200_UTC.log", "exampleweight", "simplelogs/newlogtest.ezlog")

if __name__ == "__main__":

    #test_replace()
    undate_directory("debug_logs", "DropBear")
    
    #log = sys.argv[1]
    #model = sys.argv[2]
    #output = sys.argv[3]
    #rewrite_log(log, model, output)
    #undate_log(log, model, output)

        