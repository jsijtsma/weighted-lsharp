#!/bin/bash
python3 combine_results.py test
python3 combine_results.py BFSvsDIJKSTRA
python3 combine_results.py CARTHESIANDIJKSTRAvsHADSINTvsWP
python3 combine_results.py CHEAPPIWvsNORMAL
python3 combine_results.py RANDOMvsWEIGHTED
python3 combine_results.py SORTvsNORMAL