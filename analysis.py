from concurrent.futures import ProcessPoolExecutor
import os
import subprocess
import sys
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import collections


def combine_resultfiles(directory, final_filename):
    topline = "name,learned,rounds,num_states,num_inputs,pre_inputs,pre_resets,pre_cost,learn_inputs,learn_resets,learn_cost,test_inputs,test_resets,test_cost,ads_score,cost,used_dijkstra,infix_gen,piw_listsize,sort_style,cheap_w,budget,ratio,pre_input,weightfile,seed,learning_algorithm\n"
    o = open(final_filename, "w")
    o.write(topline)
        
    for filename in os.listdir(directory):
        f = open(directory+"/"+filename,"r")
        f.readline()
        o.write(f.readline())
        f.close()
    o.close()
    print("results in" + directory + "combined in" + final_filename)


def find_logs():
    search_dir = "log"
    files = os.listdir(search_dir)
    files = [os.path.join(search_dir, f) for f in files] # add path to each file
    files.sort(key=lambda x: -os.path.getmtime(x))
    return files

def split_results_dijkstra(df, model_name):
    # this function returns the results of the specified model, split between ones where dijkstra was used, and where it was not (generally bfs)
    dijk = df[df["used_dijkstra"]==True]
    dijkcost= dijk[dijk["name"]==model_name]

    nodijk = df[df["used_dijkstra"]==False]
    nodijkcost = nodijk[nodijk["name"]==model_name]

    c = (dijkcost, nodijkcost)
    return c

def split_results_infix(df, model_name):
    # this function returns the results of the specified model, split between ones where dijkstra was used, and where it was not (generally bfs)
    pres1= df[df["infix_gen"]=="Random"]
    res1= pres1[pres1["name"]==model_name]

    pres2 = df[df["infix_gen"]=="Weighted"]
    res2 = pres2[pres2["name"]==model_name]

    return (res1, res2)

def check_df(results):
    df = pd.read_csv(results, sep=",", error_bad_lines=False)
    print(df)


def compare_cost_vs_presortedlistsize(results):
    df = pd.read_csv(results, sep=",", error_bad_lines=False)
    model_names = [
    #"exampleweight.dot",
    "example.dot",
    #"BitVise.dot",
    #"DropBear.dot",
    #"GnuTLS_3.3.8_client_full.dot",
    "NSS_3.17.4_client_full.dot",
    ]
    #newdf = df[(df.name == "NSS_3.17.4_client_full.dot")]

    #print(df)
    #print("new")
    #print(newdf)

    xplot = []
    yplot = []
    ms = []

    xplot = df['piw_listsize']
    yplot = df['cost']
    #ms = newdf['name']

    fig, ax = plt.subplots()
    ax.scatter(xplot, yplot)
    plt.show()



def scatter_compare(results):
    df = pd.read_csv(results, sep=",", error_bad_lines=False)
    model_names = [
    #"exampleweight.dot",
    #"example.dot",
    #"BitVise.dot",
    "DropBear.dot",
    #"GnuTLS_3.3.8_client_full.dot",
    ]

    xplot = []
    yplot = []
    plotnames = []
    for m in model_names:
        (dijkcost, nodijkcost) = split_results_dijkstra(df, m)
        xplot.append(dijkcost['cost'])
        plotnames.append(m + " Dijkstra")
        yplot.append(nodijkcost['cost'])
        #print(nodijkcost['cost'])
        plotnames.append(m + " BFS")

    fig, ax = plt.subplots()
    ax.scatter(xplot, yplot)
    ax.plot([0, 10000000000], [0, 10000000000], label="Line", color='orangered' )

    plt.show()

def boxplot_infix_compare(results):
    df = pd.read_csv(results, sep=",", error_bad_lines=False)
    model_names = [
    #"exampleweight.dot",
    "example.dot",
    #"BitVise.dot",
    #"DropBear.dot",
    #"GnuTLS_3.3.8_client_full.dot",
    ]

    plot = []
    plotnames = []
    for m in model_names:
        (dijkcost, nodijkcost) = split_results_infix(df, m)
        plot.append(dijkcost['cost'])
        plotnames.append(m + " Random")
        plot.append(nodijkcost['cost'])
        #print(nodijkcost['cost'])
        plotnames.append(m + " Weighted")

    fig, ax = plt.subplots()
    #ax.boxplot(dijkcost['cost'])
    #print(plot)
    ax.boxplot(plot)
    plt.xticks(range(1, len(plot)+1), plotnames, rotation=45)

    plt.show()


def boxplot_compare(results):
    df = pd.read_csv(results, sep=",", error_bad_lines=False)
    model_names = [
    #"exampleweight.dot",
    "example.dot",
    #"BitVise.dot",
    #"DropBear.dot",
    #"GnuTLS_3.3.8_client_full.dot",
    ]

    #processed = df.groupby(['name','used_dijkstra'])['cost'].aggregate(["mean","min"])

    #name = "GnuTLS_3.3.8_client_full.dot"

    
    plot = []
    plotnames = []
    for m in model_names:
        (dijkcost, nodijkcost) = split_results_dijkstra(df, m)
        plot.append(dijkcost['cost'])
        plotnames.append(m + " Dijkstra")
        plot.append(nodijkcost['cost'])
        #print(nodijkcost['cost'])
        plotnames.append(m + " BFS")



    #x = dijk.groupby(["name"])
    #bp = dijkcost.boxplot(column=['cost'])


    fig, ax = plt.subplots()
    #ax.boxplot(dijkcost['cost'])
    #print(plot)
    ax.boxplot(plot)
    plt.xticks(range(1, len(plot)+1), plotnames, rotation=45)

    plt.show()
    #df.groupby('name', 'used_dijkstra', )['cost'].boxplot()

    #print(processed)
    #dijk = df[df["used_dijkstra"]==True]
    #nodijk = df[df["used_dijkstra"]==False]
    #dijkwex = dijk[dijk["name"]=="exampleweight.dot"]
    #dijkex = dijk[dijk["name"]=="example.dot"]
    #nodijkwex = nodijk[nodijk["name"]=="exampleweight.dot"]
    #nodijkex = nodijk[nodijk["name"]=="example.dot"]
    #print(df["cost"].mean())
    #print(dijkwex["cost"].mean())
    #print(nodijkwex["cost"].mean())
    #print(dijkex["cost"].mean())
    #print(nodijkex["cost"].mean())

def diff_command(log1, log2, output_file):
    	return ["fc.exe",  log1, log2]

def run_cmd(cmd, output_file):
    subprocess.run(cmd, stdout=output_file)
    #if pr.wait() != 0:
    #    Exception("Experiment failed!")
    #    exit(-1)
    #return

def find_diffs_in_logs(amount_of_tests, output_file):
    n = amount_of_tests
    logs = find_logs() # sorted by date
    i = 0
    while n > i:
        log1 = logs[i*2]
        log2 = logs[i*2+1]
        cmd = diff_command(log1, log2, output_file)
        with open(output_file, "a") as f:
            run_cmd(cmd, f)
        i+=1
    

#find_diffs_in_logs(10, "RatioTestSortByCost.txt")

combine_resultfiles("csvs", "expr_results/PIWSuitableModelHunt3s.csv")
#check_df("expr_results/ASMLweighted.csv")

#compare_cost("expr_results/dijkstra_VS_bfs_NoSeedInAccSeqEdition.csv")

#scatter_compare("expr_results/dijkstra_VS_bfs_randomweights_UnexplainedRandomnessEdition.csv")
#scatter_compare("expr_results/bfs_VS_dijkstra_releaseVersion.csv")

#boxplot_compare("expr_results/bfs_VS_dijkstra_releaseVersion_randomweights.csv")

#boxplot_infix_compare("expr_results/single_vs_presorted_BADLABELING_randomweights.csv")
#boxplot_infix_compare("expr_results/random_vs_weighted_infix_randomweights.csv")

#compare_cost_vs_presortedlistsize("expr_results/presorted_parameters_thirdtest.csv")


