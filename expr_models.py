#from progress.bar import Bar
from concurrent.futures import ProcessPoolExecutor
import subprocess
from time import sleep


model_names = [
    "10_learnresult_MasterCard_fix",
    "GnuTLS_3.3.8_server_regular",
    "Rabo_learnresult_MAESTRO_fix",
    "1_learnresult_MasterCard_fix",
    "NSS_3.17.4_client_full",
    "Rabo_learnresult_SecureCode_Aut_fix",
    "4_learnresult_MAESTRO_fix",
    "NSS_3.17.4_client_regular",
    "TCP_FreeBSD_Client",
    "4_learnresult_PIN_fix",
    "NSS_3.17.4_server_regular",
    "TCP_FreeBSD_Server",
    r"4_learnresult_SecureCode Aut_fix",
    "OpenSSH",
    "TCP_Linux_Client",
    "ASN_learnresult_MAESTRO_fix",
    "OpenSSL_1.0.1g_client_regular",
    "TCP_Linux_Server",
    r"ASN_learnresult_SecureCode Aut_fix",
    "OpenSSL_1.0.1g_server_regular",
    "TCP_Windows8_Client",
    "BitVise",
    "OpenSSL_1.0.1j_client_regular",
    "TCP_Windows8_Server",
    "DropBear",
    "OpenSSL_1.0.1j_server_regular",
    "Volksbank_learnresult_MAESTRO_fix",
    "GnuTLS_3.3.12_client_full",
    "OpenSSL_1.0.1l_client_regular",
    "learnresult_fix",
    "GnuTLS_3.3.12_client_regular",
    "OpenSSL_1.0.1l_server_regular",
    "miTLS_0.1.3_server_regular",
    "GnuTLS_3.3.12_server_full",
    "OpenSSL_1.0.2_client_full",
    "model1",
    "GnuTLS_3.3.12_server_regular",
    "OpenSSL_1.0.2_client_regular",
    "model3",
    "GnuTLS_3.3.8_client_full",
    "OpenSSL_1.0.2_server_regular",
    "model4",
    "GnuTLS_3.3.8_client_regular",
    "RSA_BSAFE_C_4.0.4_server_regular",
    "GnuTLS_3.3.8_server_full",
    "RSA_BSAFE_Java_6.1.1_server_regular",
]

# model_names = ["hyp.0.obf"]

seeds = [
    81,
    100,
    158,
    216,
    245,
    359,
    366,
    470,
    560,
    578,
    580,
    597,
    661,
    689,
    692,
    783,
    818,
    879,
    930,
    968,
    995,
    1004,
    1005,
    1190,
    1205,
    1257,
    1320,
    1534,
    1541,
    1596,
    1607,
    1665,
    1836,
    1989,
    2015,
    2143,
    2147,
    2199,
    2221,
    2263,
    2283,
    2365,
    2370,
    2408,
    2495,
    2528,
    2554,
    2558,
    2561,
    2588,
    2610,
    # 2619,
    # 2679,
    # 2773,
    # 2816,
    # 2950,
    # 2966,
    # 2969,
    # 2983,
    # 3044,
    # 3101,
    # 3131,
    # 3147,
    # 3169,
    # 3209,
    # 3211,
    # 3213,
    # 3235,
    # 3265,
    # 3350,
    # 3383,
    # 3415,
    # 3444,
    # 3496,
    # 3528,
    # 3588,
    # 3658,
    # 3743,
    # 3769,
    # 3806,
    # 3809,
    # 3900,
    # 3980,
    # 4094,
    # 4179,
    # 4358,
    # 4370,
    # 4447,
    # 4467,
    # 4535,
    # 4550,
    # 4588,
    # 4632,
    # 4646,
    # 4689,
    # 4782,
    # 4845,
    # 4948,
    # 4950,
    # 4973,
]


def get_lsharp_command(name, oracle, seed, es, rnd_len):
    org_dir = "./experiment_models/"
    model = org_dir + name + ".dot"
    # log = org_dir + name + ".traces"
    return [
        "./target/release/lsharp",
        "-e",
        oracle,
        "--rule2",
        "sep-seq",
        "--rule3",
        "sep-seq",
        "-k",
        str(es),
        "-l",
        str(rnd_len),
        "-x",
        str(seed),
        "--eq-mode",
        "infinite",
        # "--traces",
        # log,
        "-m",
        model,
        "-v",
        "1",
        "--out",
        "./csvs/results.csv" + name + str(seed) + oracle,
        "-q",
        "--compress-tree",
    ]


def run_expr(cmd):
    pr = subprocess.Popen(cmd)
    if pr.wait() != 0:
        Exception("Experiment failed!")
        exit(-1)
    return


# oracles = ["hads-int", "iads"]
oracles = ["hads-int", "hsi", "w", "wp"]
# oracles = ["iads"]
# oracles = ["soucha-spy", "soucha-spyh"]
# oracles = ["soucha-spy"]
all_results = []
cmds = []
es = [1, 2]
rnd_lens = [2, 3]
for k in es:
    for oracle in oracles:
        for model in model_names:
            for seed in seeds:
                for rnd_len in rnd_lens:
                    cmd = get_lsharp_command(model, oracle, seed, k, rnd_len)
                    cmds.append(cmd)


# if __name__ == "__main__":
#     with Bar("Running experiments", max=len(cmds), suffix="%(percent)d%%") as bar:
#         for cmd in cmds:
#             run_expr(cmd)
#             bar.next()


if __name__ == "__main__":
    with ProcessPoolExecutor(max_workers=1) as executor:
        executor.map(run_expr, cmds)
