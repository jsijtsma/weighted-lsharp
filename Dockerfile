FROM rust:1.67


RUN apt-get update
RUN apt-get install -y python3

WORKDIR /home/user

COPY lsharp.zip /home/user/lsharp.zip

RUN unzip lsharp.zip -d .
RUN cargo build -r --bin lsharp


